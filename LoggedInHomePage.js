/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
// import { useDrawerStatus } from '@react-navigation/drawer';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image, Button, TouchableHighlight, Dimensions, TouchableOpacity
} from 'react-native';

import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"

const LoggedInHomePage = ({ navigation }) => {

    const screenHeight = Dimensions.get('window').height;
    // const isDrawerOpen = useDrawerStatus() === 'open';

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: '#060606', height: screenHeight}}>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                {/* //paddingBottom:450 */}


                <View style={{ flex:1, backgroundColor: '#060606', justifyContent: 'flex-start', flexDirection: 'column', alignItems: 'center' , alignContent:'space-between'}}>

                    <View style={{
                        flex: 0.1, justifyContent: 'space-between', flexDirection: 'row',
                        alignItems: 'center', backgroundColor: 'transparent', width: '100%'
                    }}>
                        {/* <Image source={require('./assets/top-drawer-icon.png')} width={22} height={20} style={{ marginLeft: 10, marginRight: 0 }} /> */}

                        <TouchableHighlight
                                style={{ width: '10%', height: '100%' , backgroundColor:'transparent', justifyContent:'center', alignItems:'center'}}

                                onPress={() => navigation.openDrawer()}
                                underlayColor='grey'>
                                 
                                    <Image source={require('./assets/top-drawer-icon.png')} resizeMode='contain' style={{left:wp('7%')}} />

                                
                            </TouchableHighlight>


                        <Image source={require('./assets/Frizpay.png')} style={styles.image} />
                        <TouchableOpacity onPress={()=>navigation.navigate('NotificationList')}>
                        <Image source={require('./assets/top-bell-icon.png')} width={20} height={22} style={{right:wp('8%')}} resizeMode='contain'></Image>
                        </TouchableOpacity>
                    </View>
                    {/* <ScrollView style={{ flex: 1 , height:'10%', width:'100%' }} contentContainerStyle={{ flexGrow: 1, height:'100%', width:'100%', alignContent:'center', flex:1}}> */}
                    <View style={{
                        flex: 0.04, justifyContent: 'space-evenly', flexDirection: 'row',
                        alignItems: 'center', height: 50,
                        backgroundColor: '#E8EB59', borderRadius: 15, width: '85%', marginTop: 20
                    }}>
                        {/* <TouchableHighlight onPress={()=>navigation.navigate('TaskList')}> */}
                     <TouchableOpacity onPress={()=>navigation.navigate('TaskList')}>
                      <View style={{width:wp('35%'),height:50,alignItems:'center',justifyContent:'center'}}>
                      <Text style={styles.submitTitle}>Assign Task</Text>
                      </View>
                      </TouchableOpacity>
                      {/* </TouchableHighlight> */}

                        <View style={{
                            flex: 0.023, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent', width: 1, height: '90%',
                        }}>
                            <View style={{ backgroundColor: '#000000', width: '100%', height: '100%' }}>

                            </View>

                        </View>

                        <TouchableOpacity onPress={()=>navigation.navigate('SendMoney')}>
                      <View style={{width:wp('35%'),height:50,alignItems:'center',justifyContent:'center'}}>
                      <Text style={styles.submitTitle}>Send Money</Text>
                      </View>
                      </TouchableOpacity>
                    </View>


                    <View style={{
                        flex: 0.1, justifyContent: 'space-around', flexDirection: 'column',
                        alignItems: 'flex-start', backgroundColor: 'transparent', width: '90%',alignSelf:'center'
                    }}>
                        <Text style={{
                            color: '#E0E0E0',

                            // width: '85%',
width:wp('90%'),
alignSelf:'center',
                            textAlign: 'center',

                            fontFamily: 'SFPRODISPLAYBOLD',
                            fontSize: 22,
                            marginTop:hp('1%')
                        }}>My Family</Text>

                      
                            <TouchableHighlight
                                // style={{ width: '100%', height: '100%' }}

                                onPress={() => { 
                                    console.log('single cliked----------------------');
                                    navigation.navigate("FamilyDetail");
                                }}
                                underlayColor='grey'>
                                <View style={{ backgroundColor: '#484848',  borderRadius: 15, justifyContent: 'space-evenly', alignItems: 'center',height:hp('25%') ,width: wp('35%')}}>
                                    <Image source={require('./assets/plus-icon.png')} style={{height:40,width:40}} resizeMode={'contain'} />

                                </View>
                            </TouchableHighlight>

                        
                    </View>

                    <View style={{
                        flex: 0.07, justifyContent: 'flex-start', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent', width: '90%', height: '90%',marginTop:10
                    }}>
                        <TouchableHighlight
                            style={{ width: '100%', height: '100%' }}

                            onPress={() => { }}
                            underlayColor='grey'>
                            <View style={{ backgroundColor: '#484848', width: '100%', height: '100%', borderRadius: 15, justifyContent: 'flex-start', alignContent: 'space-between', flexDirection: 'column' }}>

                                <Text style={{
                                    color: '#E0E0E0',

                                    // width: '85%',

                                    textAlign: 'left',
                                    marginLeft: 10,
                                    fontWeight:'900',

                                    // fontFamily: 'SFPRODISPLAYMEDIUM',
                                    fontSize: 22,
                                    padding:hp('0.5%')
                                }}>Fee Portal</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                                    <Image source={require('./assets/fee-icon.png')} resizeMode='contain' width={30} height={30} style={{ marginLeft: 30 }} />

                                    <Text style={{
                                        color: '#C0C0C0',

                                        width: '85%',

                                        textAlign: 'left',
                                        marginLeft: 10,

                                        fontFamily: 'SFPRODISPLAYMEDIUM',
                                        fontSize: 15
                                    }}>Enter details for fee</Text>
                                </View>

                            </View>
                        </TouchableHighlight>

                    </View>

                    <View style={{
                        flex:0.8, justifyContent: 'flex-start', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent', width: '90%', height: '100%', marginTop:10
                    }}>
                        <TouchableHighlight
                            style={{ width: '100%', height: '100%' }}

                            onPress={() => { }}
                            underlayColor='grey'>
                            <View style={{ backgroundColor: '#484848', width: '100%', height: '100%', borderRadius: 15, justifyContent: 'flex-start', alignContent: 'space-between', flexDirection: 'column' }}>

                                <Text style={{
                                    color: '#E0E0E0',

                                    // width: '85%',

                                    textAlign: 'left',
                                    marginLeft: 10,

                                    fontFamily: 'SFPRODISPLAYMEDIUM',
                                    fontSize: 20,
                                    padding:hp('1%')
                                }}>{'What more can your child \ncan learn'}</Text>


                                <View style={{
                                    flex: 0.9, justifyContent: 'space-around', flexDirection: 'row',
                                    alignItems: 'center', backgroundColor: 'transparent', width: '100%', height: '100%',
                                }}>
                                    <TouchableHighlight
                                        style={{ width: '45%', height: '100%' }}

                                        onPress={() => { }}
                                        underlayColor='grey'>
                                        <View style={{ backgroundColor: '#000000', width: '100%', height: '100%', borderRadius: 15, justifyContent: 'flex-start', alignContent: 'space-between', flexDirection: 'column' }}>



                                        </View>
                                    </TouchableHighlight>

                                    <TouchableHighlight
                                        style={{ width: '45%', height: '100%' }}

                                        onPress={() => { }}
                                        underlayColor='grey'>
                                        <View style={{ backgroundColor: '#000000', width: '100%', height: '100%', borderRadius: 15, justifyContent: 'flex-start', alignContent: 'space-between', flexDirection: 'column' }}>



                                        </View>
                                    </TouchableHighlight>

                                </View>



                            </View>
                        </TouchableHighlight>

                    </View>
                    {/* </ScrollView> */}

                </View>
               
            </ScrollView>

            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 80,
        height: 20,
        resizeMode:'contain',
        top:hp('0.8%')
        // marginVertical: 32,

    },
    imagelogo: {
        width: 46,
        height: 50,

    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '40%',
        // backgroundColor:'pink'
        // height: 51,
        // backgroundColor: 'red',
        // justifyContent: 'center',
        // alignItems: 'center',
        // borderRadius: 15,
        backgroundColor:'pink'
    },
    submitTitle: {
        // textAlign: 'center',

        // fontFamily: 'SFPRODISPLAYMEDIUM',
        fontSize: 16,
        fontWeight:'900',
        color: '#000000',
        // backgroundColor:'red',
        // height:hp('5%'),
        // alignSelf:'center',justifyContent:'center',
        // alignItems:'center',


    }
});

export default LoggedInHomePage;
