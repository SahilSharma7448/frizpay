/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TextInput,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/Ionicons';

 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
 
 const SendMoney = ({ navigation }) => {
    const [activeTab, setActiveTab] = useState('0')

 
     return (
         <SafeAreaView style={{ flex: 1 }}>
             <View style={{ flex: 1, backgroundColor: '#060606', }}>
                 {/* <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}> */}
                 {/* //paddingBottom:450 */}
                 <ScrollView>
 
 
                
                             
 <View style={{flexDirection:'row',width:wp('90%'),justifyContent:'space-between',marginTop:hp('5%'),alignSelf:'center'}}>
 <TouchableHighlight
                             style={{ }}
 
                             onPress={() => {
                                 // navigation.navigate("LoggedInHomePage")
                                 // console.log(navigation.getParent());
                                 // navigation.toggleDrawer();
                                 // navigationContext.navigate('Notifications');
                                 // navigationContext.navigate('HomeTabPage',
                                 //     {
                                 //         screen: 'Home',
                                 //         // initial: false,
                                 //         params: {
                                 //             screen: 'LoggedInHomePage'
                                 //         }
                                 //     }
                                 // );
 
                                 // navigation.goBack();
 
                                 // navigation.popToTop();
                                 // navigation.goBack(navigation.getState().routes[0].key);
                                 // navigation.navigate('SettingsScreen2');
 navigation.goBack()
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{top:hp('1%')}} />
 
 
                         </TouchableHighlight>
 
                    <View style={{width:wp('60%'),alignSelf:'flex-start',right:wp('3%')}}>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             fontFamily: 'SFPRODISPLAYBOLD',
                             fontSize: 25,
                         }}>Send money</Text>
                        
</View>

<Image source={require('./assets/ridhima.png')} style={{height:60,width:60,borderRadius:50,top:hp('1%')}} resizeMode="contain" />

</View>
<Text style={{
                             color: '#E0E0E0',
 
                             width: wp('80%'),
 
                             alignSelf:'center',
                             fontFamily:'SF-Pro-Rounded-Semibold' ,
                             fontSize: 12,
                             right:wp('2.5%')
                         }}>Ridhima’s Available Balance:   ₹1500
                         </Text>
<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             fontFamily:'SF-Pro-Rounded-Semibold' ,

                             fontSize: 15,
                             alignSelf:'center',
                             width:wp('85%'),
                             marginTop:hp('6%')
                         }}>Send money</Text>
<TextInput style={{  color: 'grey', alignSelf: 'center', width: wp('85%'), right: 5,fontSize:25, }} placeholder={'₹ Enter amount'} placeholderTextColor={'grey'} keyboardType={'number-pad'}></TextInput>
<View style={{ borderBottomColor: 'grey', borderBottomWidth: 1, width: wp('85%'), alignSelf: 'center', }}></View>



<TouchableOpacity onPress={()=>navigation.navigate('Payment')}>
<View style={{ height: hp('7%'), width: wp('85%'),alignSelf:'center', marginTop: hp('50%'), backgroundColor: '#E8EB59', borderRadius: 13, alignItems: 'center', justifyContent: 'center',marginBottom:hp('10%') }}>
            <Text style={{fontFamily:'SF-Pro-Rounded-Semibold', color:'black',left:wp('1%')}}>Send Money</Text>
          </View>
          </TouchableOpacity>
 
             </ScrollView>
              
 
             </View>
         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default SendMoney;
 