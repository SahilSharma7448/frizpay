/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image, Button, TouchableHighlight, TextInput, TouchableWithoutFeedback,
    Keyboard
} from 'react-native';

import SelectDropdown from 'react-native-select-dropdown'
import Entypo from 'react-native-vector-icons/Entypo';
import { widthPercentageToDP } from './App/utility';


const countries = ["1L", "2L", "3L", "4L"];

const PanDeclarationPage = ({ navigation }) => {

    const [politicalyes, setpoliticalyes] = useState('#E8EB59');
    const [politicalno, setpoliticalno] = useState('#C4C4C4');

    const [countryyes, setcountryyes] = useState('#E8EB59');
    const [counntryno, setcountryno] = useState('#C4C4C4');


    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-evenly' }}>
                    <View style={{ flex: 0.7, backgroundColor: '#060606', justifyContent: 'flex-start', alignItems: 'center' }}>

                        <View style={{
                            flex: 0.25, justifyContent: 'space-evenly', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent'
                        }}>
                            <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                            <Image source={require('./assets/Frizpay.png')} style={styles.image} />
                        </View>

                        <View style={{
                            flex: 0.04, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent',
                        }}>
                            <Text style={{
                                color: '#E0E0E0',

                                width: '90%',


                                textAlign: 'center',

                                fontFamily: 'SFPRODISPLAYMEDIUM',
                                fontSize: 32
                            }}>Declarations</Text>






                        </View>


                        <View style={{ flex: 0.1, backgroundColor: 'transparent', justifyContent: 'space-evenly', width: '90%', marginTop: 10 }}>

                            <Text style={{
                                color: '#E0E0E0',

                                width: '90%',

                                textAlign: 'left',

                                fontFamily: 'SFProText-Regular',
                                fontSize: 12
                            }}>Are you a member of political party?</Text>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TouchableHighlight
                                    style={{
                                        width: '45%', height: 45, backgroundColor: 'transparent', borderRadius: 15, borderColor: politicalyes,
                                        borderWidth: 0,
                                        flexDirection: 'row', justifyContent: 'center', alignContent: 'center'
                                    }}
                                    onPress={() => {
                                        setpoliticalyes('#E8EB59');
                                        setpoliticalno('#C4C4C4');
                                    }}
                                    underlayColor='lightgrey'>
                                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                                        <View style={{
                                            color: '#E0E0E0',

                                            width: 10,
                                            height: 10,
                                            borderRadius: 5,
                                            marginTop: 3,
                                            marginRight: 5,
                                            backgroundColor: politicalyes
                                            // marginLeft:10
                                        }}></View>
                                        <Text style={{
                                            color: '#E0E0E0',

                                            // width: '100%',

                                            textAlign: 'center',

                                            fontFamily: 'SFProText-Regular',
                                            fontSize: 14,
                                            // marginLeft:10,
                                            // marginTop:20
                                        }}>Yes</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight
                                    style={{
                                        width: '45%', height: 45, backgroundColor: 'transparent', borderRadius: 15, borderColor: politicalno,
                                        borderWidth: 0,
                                        flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignContent: 'center'
                                    }}
                                    onPress={() => {
                                        setpoliticalno('#E8EB59');
                                        setpoliticalyes('#C4C4C4');
                                    }}
                                    underlayColor='lightgrey'>
                                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                                        <View style={{
                                            color: '#E0E0E0',

                                            width: 10,
                                            height: 10,
                                            borderRadius: 5,
                                            marginTop: 3,
                                            marginRight: 5,
                                            backgroundColor: politicalno
                                            // marginLeft:10
                                        }}></View>
                                        <Text style={{
                                            color: '#E0E0E0',

                                            // width: '100%',

                                            textAlign: 'center',

                                            fontFamily: 'SFProText-Regular',
                                            fontSize: 14,
                                            // marginLeft:10
                                        }}>No</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>



                        </View>



                        <View style={{ flex: 0.1, backgroundColor: 'transparent', justifyContent: 'space-evenly', width: '90%', marginTop: 10 }}>

                            <Text style={{
                                color: '#E0E0E0',

                                width: '90%',

                                textAlign: 'left',

                                fontFamily: 'SFProText-Regular',
                                fontSize: 12
                            }}>Are you a tax payer in ANY other country than india?</Text>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TouchableHighlight
                                    style={{
                                        width: '45%', height: 45, backgroundColor: 'transparent', borderRadius: 15, borderColor: countryyes,
                                        borderWidth: 0,
                                        flexDirection: 'row', justifyContent: 'center', alignContent: 'center'
                                    }}
                                    onPress={() => {
                                        setcountryyes('#E8EB59');
                                        setcountryno('#C4C4C4');
                                    }}
                                    underlayColor='lightgrey'>
                                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                                        <View style={{
                                            color: '#E0E0E0',

                                            width: 10,
                                            height: 10,
                                            borderRadius: 5,
                                            marginTop: 3,
                                            marginRight: 5,
                                            backgroundColor: countryyes
                                            // marginLeft:10
                                        }}></View>
                                        <Text style={{
                                            color: '#E0E0E0',

                                            // width: '100%',

                                            textAlign: 'center',

                                            fontFamily: 'SFProText-Regular',
                                            fontSize: 14,
                                            // marginLeft:10,
                                            // marginTop:20
                                        }}>Yes</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight
                                    style={{
                                        width: '45%', height: 45, backgroundColor: 'transparent', borderRadius: 15, borderColor: counntryno,
                                        borderWidth: 0,
                                        flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignContent: 'center'
                                    }}
                                    onPress={() => {
                                        setcountryno('#E8EB59');
                                        setcountryyes('#C4C4C4');
                                    }}
                                    underlayColor='lightgrey'>
                                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                                        <View style={{
                                            color: '#E0E0E0',

                                            width: 10,
                                            height: 10,
                                            borderRadius: 5,
                                            marginTop: 3,
                                            marginRight: 5,
                                            backgroundColor: counntryno
                                            // marginLeft:10
                                        }}></View>
                                        <Text style={{
                                            color: '#E0E0E0',

                                            // width: '100%',

                                            textAlign: 'center',

                                            fontFamily: 'SFProText-Regular',
                                            fontSize: 14,
                                            // marginLeft:10
                                        }}>No</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>



                        </View>



                        <View style={{
                            flex: 0.3, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'flex-start', backgroundColor: 'transparent',width:'90%'
                        }}>


                            <Text style={{
                                color: '#E0E0E0',

                                width: '100%',

                                textAlign: 'left',

                                fontFamily: 'SFProText-Regular',
                                fontSize: 12,
                                marginBottom:10
                            }}>Whats your annual income?</Text>

<SelectDropdown
	data={countries}
    dropdownIconPosition='right'
    defaultButtonText={"Select"}
	onSelect={(selectedItem, index) => {
		console.log(selectedItem, index)
	}}
	buttonTextAfterSelection={(selectedItem, index) => {
		// text represented after item is selected
		// if data array is an array of objects then return selectedItem.property to render after item is selected
		return selectedItem
	}}
	rowTextForSelection={(item, index) => {
		// text represented for each item in dropdown
		// if data array is an array of objects then return item.property to represent item in dropdown
		return item
	}}
    renderDropdownIcon={() => {
        return <Entypo name='triangle-down' size={20} color={'white'} />;

      }}
      buttonStyle={{backgroundColor:'#484848', borderRadius:15}}
      buttonTextStyle={{color:'white'}}
/>

                        </View>






                        <View style={{ width: '100%', height: 25, backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                            <Text style={{
                                color: '#E0E0E0',

                                width: '100%',

                                textAlign: 'center',

                                fontFamily: 'SFProText-Regular',
                                fontSize: 12
                            }}>3/3</Text>
                        </View>



                    </View>
                    <View style={{
                        flex: 0.2, justifyContent: 'space-around', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent'
                    }}>
                        <Text style={{
                            color: '#E0E0E0',

                            width: '85%',

                            textAlign: 'left',

                            fontFamily: 'SFProText-Regular',
                            fontSize: 12
                        }}>By clicking continue you agree to share your information with our partner bank.

                        </Text>

                        <TouchableHighlight
                            style={styles.doneButton}
                            onPress={() => 
                                {
                                    // navigation.goBack();
                                navigation.navigate('DrawerPage')
                                }
                                }
                            underlayColor='#E8EB59'>
                            <Text style={styles.submitTitle}>Continue</Text>
                        </TouchableHighlight>

                    </View>

                </View>
            </ScrollView>
        </SafeAreaView>

    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 90,
        height: 25,
        resizeMode:'contain'


    },
    imagelogo: {
        width: 36,
        height: 40,
        resizeMode:'contain'
        // marginTop: 32,
        // marginBottom: 20
    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '85%',
        height: 51,
        backgroundColor: '#E8EB59',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 15,
        color: '#000000',
left:widthPercentageToDP('1%')
    }
});

export default PanDeclarationPage;
