/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image, Button, TouchableHighlight, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard
} from 'react-native';

import OTPInputView from '@twotalltotems/react-native-otp-input'

const AadharOTPPage = ({ navigation }) => {


    return (
        <SafeAreaView style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
        <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-evenly' }}>
            <View style={{ flex: 0.8, backgroundColor: '#060606', justifyContent: 'flex-start' }}>

                <View style={{
                    flex: 0.25, justifyContent: 'space-evenly', flexDirection: 'column',
                    alignItems: 'center', backgroundColor: 'transparent'
                }}>
                    <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                    <Image source={require('./assets/Fri-zpay-banner.png')} style={styles.image} />
                </View>

                <View style={{
                    flex: 0.03, justifyContent: 'flex-start', flexDirection: 'column',
                    alignItems: 'center', backgroundColor: 'transparent', 
                }}>
                    <Text style={{
                        color: '#E0E0E0',

                        width: '90%',

                        textAlign: 'left',

                        fontFamily: 'SFPRODISPLAYMEDIUM',
                        fontSize: 25
                    }}>Enter OTP Received on your Registered number</Text>
<Text style={{
                        color: '#E0E0E0',

                        width: '90%',

                        textAlign: 'left',

                        fontFamily: 'SFProText-Regular',
                        fontSize: 12
                    }}>Enter the OTP which you have ecieved on the phone number associated with your aadhaar.</Text>

                </View>

               


                <View style={{
                    flex: 0.2, justifyContent: 'flex-start', flexDirection: 'column',
                    alignItems: 'center', backgroundColor: 'transparent'
                }}>
                    <OTPInputView
                        style={{ width: '80%', height: 50, backgroundColor: 'transparent' }}
                        pinCount={4}
                        // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                        // onCodeChanged = {code => { this.setState({code})}}
                        autoFocusOnLoad={false}
                        codeInputFieldStyle={styles.underlineStyleBase}
                        codeInputHighlightStyle={styles.underlineStyleHighLighted}
                        onCodeFilled={(code) => {
                            console.log(`Code is ${code}, you are good to go!`)
                        }}
                    />
                </View>

            </View>

            <View style={{
                flex: 0.2, justifyContent: 'space-around', flexDirection: 'column',
                alignItems: 'center', backgroundColor: 'transparent'
            }}>
                <Text style={{
                    color: '#E0E0E0',

                    width: '85%',

                    textAlign: 'left',

                    fontFamily: 'SFProText-Regular',
                    fontSize: 12
                }}>By clicking continue you agree to share your information with our partner bank.</Text>
                <TouchableHighlight
                    style={styles.doneButton}
                    onPress={() => navigation.navigate('HomeTabPage')}
                    underlayColor='#E8EB59'>
                    <Text style={styles.submitTitle}>Continue</Text>
                </TouchableHighlight>

            </View>
        </View>
        </ScrollView>
        </SafeAreaView>

    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 113,
        height: 30,


    },
    imagelogo: {
        width: 46,
        height: 50,
        // marginTop: 32,
        // marginBottom: 20
    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '85%',
        height: 51,
        backgroundColor: '#E8EB59',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 17,
        color: '#000000',


    },
    resendotp: {
        textAlign: 'center',

        fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 17,
        color: '#E0E0E0',


    },
    borderStyleBase: {
        width: 30,
        height: 45
    },

    borderStyleHighLighted: {
        borderColor: "#03DAC6",
    },

    underlineStyleBase: {
        width: 66,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
    },

    underlineStyleHighLighted: {
        borderColor: "#03DAC6",
    },
});

export default AadharOTPPage;
