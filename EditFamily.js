/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/Ionicons';
 import { selectContactPhone } from 'react-native-select-contact';
 import { useNavigation } from '@react-navigation/native';
import { heightPercentageToDP as hp,widthPercentageToDP as wp } from './App/utility';
import Icon from "react-native-vector-icons/FontAwesome"
import {Input} from "react-native-elements"
 const screenHeight = Dimensions.get('window').height
 const screenWidth = Dimensions.get('window').width
 
 const EditFamily = ({ navigation }) => {
 
     const navigationContext = useNavigation();
 
     return (
         <SafeAreaView style={{ flex: 1,backgroundColor:'#060606' }}>
            
<ScrollView>
<View style={{width:wp('95%'),alignSelf:'center',marginTop:hp('5%'),flexDirection:'row'}}>
<TouchableOpacity
 
                             onPress={() => {
                                
 
                                 navigation.goBack();
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{top:hp('1%')}} />
 
 
                         </TouchableOpacity>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
 
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontSize: 32
                         }}>Edit Family</Text>
</View>







                 <View style={{ marginTop: hp('2%'), alignSelf: 'center' }}>
          <Image source={require('./assets/avtar.png')} style={{height:50,width:50,alignSelf:'center',marginTop:hp('2%'),borderRadius:60}} resizeMode="contain" />
          <Text style={{ marginTop: hp('1%'),color:'white',textAlign:"center"  }}>Satnam</Text>
              <Text style={{ marginTop: hp('0.5%'), fontSize: 10 ,color:'grey',textAlign:"center" }}>(90563434331)</Text>

          <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: wp('80%') }}>
            <View style={{}}>
            {/* <Image source={require('./assets/avtar.png')} style={{height:60,width:60}} resizeMode="contain" /> */}
            <Image source={require('./assets/ridhima.png')} style={{height:50,width:50,alignSelf:'center',marginTop:hp('2%'),borderRadius:60}} resizeMode="contain" />

              <Text style={{ marginTop: hp('1%'),color:'white',textAlign:"center"  }}>Ridhima</Text>
              <Text style={{ marginTop: hp('0.5%'), fontSize: 10 ,color:'grey',textAlign:"center" }}>(90563434331)</Text>

            </View>
            <View style={{}}>
            {/* <Image source={require('./assets/avtar.png')} style={{height:60,width:60}} resizeMode="contain" /> */}
            <Image source={require('./assets/shubham.png')} style={{height:50,width:50,alignSelf:'center',marginTop:hp('2%'),borderRadius:60}}  />

              <Text style={{ marginTop: hp('1%'),color:'white',textAlign:"center" }}>Chakashu</Text>
              <Text style={{ marginTop: hp('0.5%'), fontSize: 10,color:'grey',textAlign:"center"  }}>(90563434331)</Text>

            </View>
          </View>
        </View>

        <View style={{ flexDirection: 'row', width: wp('90%'), alignSelf: 'center' }}>
          <Icon
            name={'arrow-left'}
            size={17}
            color={'black'}
            style={{ alignSelf: 'center', alignItems: 'center', justifyContent: "center", marginRight: 20 }}
          />



          {/* <Input
            placeholder={'Enter phone number or selct a contact to invite in family'}
            leftIcon={
              <Icon
                name={'search'}
                size={17}
                color={'#C9C9C9'}
                style={{ alignSelf: 'baseline', marginRight: 5 }}
              />
            }
            inputContainerStyle={{
              borderColor:'#484848',
              borderWidth: 0,

              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              top: 10,
              
            }}
            labelStyle={{fontSize:10}}
            containerStyle={{
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              height: hp('7%'),
              backgroundColor: '#484848',
              width: wp('80%'),
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              borderRadius: 10,
              marginTop:hp('5%')
            }}
            rightIcon={
              <TouchableOpacity>

              </TouchableOpacity>
            }
          /> */}
 
        </View>


        <View style={{ backgroundColor: '#484848',  borderRadius: 10, justifyContent: 'space-around', alignItems: 'center', flexDirection: 'row',marginTop:hp('3%'),width:wp('90%'),alignSelf:'center' }}>
                                <Ionicons name="search" color="#C9C9C9" size={25} />
                                <Text style={{
                                    color: '#848484',

                                    width: '85%',

                                    textAlign: 'left',

                                    fontFamily: 'SFPRODISPLAYMEDIUM',
                                    fontSize: 14,
                                    padding:hp('1.5%')
                                }}>Enter phone number or selct a contact to invite in family</Text>
                            </View>

        

<Text style={{fontSize:20 ,     fontFamily: 'SFPRODISPLAYBOLD', color:'#E0E0E0',marginTop:hp('3%'),width:wp('85%'),alignSelf:'center'}}>Frizday users</Text>









 
<View style={{ width: wp('90%'), alignSelf: "center",  alignItems: "center", flexDirection: "row",marginTop:hp('1%') }}>
         <Image source={require('./assets/teenager_logo.png')} style={{height:40,width:40,top:hp('1%')}} resizeMode="contain" />
         <View style={{ width: wp('60%'),  marginLeft:wp('5%'),marginTop:hp('1%') }}>
           <Text style={{color:'white',fontSize:15}}>Shubham sharma</Text>
           <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}>9056343431</Text>

         </View>
         {/* <Switch value={false} color="orange" /> */}

       </View>
       <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>

       <View style={{ width: wp('90%'), alignSelf: "center",  alignItems: "center", flexDirection: "row",marginTop:hp('1%') }}>
         <Image source={require('./assets/teenager_logo.png')} style={{height:40,width:40,alignSelf:'center',justifyContent:'center',alignItems:'center',top:hp('1%')}} resizeMode="contain" />
         <View style={{ width: wp('60%'),  marginLeft:wp('5%'),marginTop:hp('1%') }}>
           <Text style={{color:'white',fontSize:15}}>Shubham sharma</Text>
           <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}>9056343431</Text>

         </View>
         {/* <Switch value={false} color="orange" /> */}

       </View>
       <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>


















       <Text style={{fontSize:20 , fontFamily: 'SFPRODISPLAYBOLD',color:'#E0E0E0',marginTop:hp('3%'),width:wp('85%'),alignSelf:'center'}}> Non-Frizpay users</Text>










<View style={{ width: wp('90%'), alignSelf: "center",  alignItems: "center", flexDirection: "row",marginTop:hp('2%') }}>
        <Image source={require('./assets/teenager_logo.png')} style={{height:40,width:40,top:hp('1%')}} resizeMode="contain" />
        <View style={{ width: wp('60%'),  marginLeft:wp('5%'),marginTop:hp('1%') }}>
          <Text style={{color:'white',fontSize:15}}>Shubham sharma</Text>
          <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}>9056343431</Text>

        </View>
        {/* <Switch value={false} color="orange" /> */}

      </View>
      <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>

      <View style={{ width: wp('90%'), alignSelf: "center",  alignItems: "center", flexDirection: "row",marginTop:hp('1%') }}>
        <Image source={require('./assets/teenager_logo.png')} style={{height:40,width:40,top:hp('1%')}} resizeMode="contain" />
        <View style={{ width: wp('60%'),  marginLeft:wp('5%'),marginTop:hp('1%') }}>
          <Text style={{color:'white',fontSize:15}}>Shubham sharma</Text>
          <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}>9056343431</Text>

        </View>
        {/* <Switch value={false} color="orange" /> */}

      </View>
      <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%'),marginBottom:hp('5%') }}></View>






      </ScrollView>
         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default EditFamily;
 