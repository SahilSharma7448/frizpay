/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image, Button, TouchableHighlight, TextInput, TouchableWithoutFeedback,
    Keyboard,
    Alert
} from 'react-native';
import moment from 'moment'
import { TextField, FilledTextField, OutlinedTextField } from 'rn-material-ui-textfield';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DateTimePicker from '@react-native-community/datetimepicker';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from './App/utility'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch } from 'react-redux'
import { Set_parent_Data } from './App/Action/Auth'
const PanCardDetailsPage = ({ route, navigation }) => {
    const { name, pin } = route.params
    const dispatch = useDispatch()
    const firstNameRef = React.createRef()
    const lastNameRef = React.createRef()
    const mobileNumRef = React.createRef()

    const [firstName, setfirstName] = useState('');
    const [lastName, setlastName] = useState('');
    const [mobileNum, setmobileNum] = useState('');
    const [malegender, setmalegender] = useState('#E8EB59');
    const [femalegender, setfemalegender] = useState('transparent');
    const [gender, setGender] = useState('male')
    const [date, setDate] = useState('');
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    // const showDate=async()=>{
    //    await setDatePickerVisibility(true)
    //    console.log("checkkkkkkkas",isDatePickerVisible)

    // }


    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');

        setDate(moment(currentDate).format('YYYY-MM-DD'));

    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);

    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };

    const goToRoute = async () => {
        // navigation.navigate('PanCardNumberPage')
        console.log('first', firstName, "last", lastName, "mobile", mobileNum, "selected", date, "gender", gender)
        if (await firstName == '' || lastName == '' || mobileNum == '' || date == '' || gender == '' || mobileNum < 10) {
            return Alert.alert('Please fill valid detail')
        }
        else {
            let data = {
                name: name,
                pin: pin,
                firstName: firstName,
                lastName: lastName,
                mobileNum: mobileNum,
                date: date,
                gender: gender
            }
            let res = await dispatch(Set_parent_Data(data))
            navigation.navigate('PanCardNumberPage')
        }

    }
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1  }}>
                <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-evenly' }}>
                    <View style={{ flex: 0.7, backgroundColor: '#060606', justifyContent: 'flex-start' }}>

                        <View style={{
                            flex: 0.25, justifyContent: 'space-evenly', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent'
                        }}>
                            <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                            <Image source={require('./assets/Frizpay.png')} style={styles.image} />
                        </View>

                        <View style={{
                            flex: 0.04, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent',
                        }}>
                            <Text style={{
                                color: '#E0E0E0',

                                width: '90%',


                                textAlign: 'left',

                                fontFamily: 'SF-Pro-Rounded-Semibold',
                                fontSize: 32
                            }}>{'Enter your \nBasic Pancard Details'}</Text>

                            <Text style={{
                                color: '#E0E0E0',

                                width: '90%',

                                textAlign: 'left',

                                fontFamily: 'SFProText-Regular',
                                fontSize: 12
                            }}>Enter the personal details as per mentioned in your pancard</Text>



                        </View>




                        <View style={{
                            flex: 0.1, flexDirection: 'column',
                            backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center'
                        }}>


<View style={{flexDirection:'row',alignSelf:'center',justifyContent:'space-between',width:wp('90%'),marginTop:hp('2%')}}>
                           
                           <View>
                            <FilledTextField
                                label='First Name'
                                labelFontSize={10}
                                placeholder='First Name'
                                placeholderTextColor='#626262'
                                baseColor='#E0E0E0'
                                textColor='#626262'
                                lineType='none'
                                fontFamily='SFProText-Regular'
                                fontSize={18}
                                inputContainerStyle={{ backgroundColor: 'transparent', borderBottomWidth: 1, borderBottomColor: '#E0E0E0', width: wp('35%') }}
                                // keyboardType='phone-pad'
                                // formatText={ (text) =>{     return text.replace(/[^+\d]/g, '')}}
                                onSubmitEditing={() => {
                                    let { current: field } = firstNameRef

                                    console.log(field.value())
                                }
                                }
                                onChangeText={(text) => {
                                    setfirstName(text);
                                }}
                                value={firstName}
                                ref={firstNameRef}
                                style={{ backgroundColor: 'transparent', width: wp('35%') }}
                            />
                            <View style={{}}></View>
                                </View>

                            <FilledTextField
                                label='Last Name'
                                labelFontSize={10}
                                placeholder='Last Name'
                                placeholderTextColor='#626262'
                                baseColor='#E0E0E0'
                                textColor='#626262'
                                lineType='none'
                                fontFamily='SFProText-Regular'
                                fontSize={18}
                                inputContainerStyle={{ backgroundColor: 'transparent', borderBottomWidth: 1, borderBottomColor: '#E0E0E0', width: wp('35%'), }}
                                // keyboardType='phone-pad'
                                // formatText={ (text) =>{     return text.replace(/[^+\d]/g, '')}}
                                onSubmitEditing={() => {
                                    let { current: field } = lastNameRef

                                    console.log(field.value())
                                }
                                }
                                onChangeText={(text) => {
                                    setlastName(text);
                                }}
                                value={lastName}
                                ref={lastNameRef}
                                style={{ backgroundColor: 'transparent', width: wp('35%') }}
                            />
</View>
                            <FilledTextField
                                label='Mobile Number'
                                labelFontSize={10}
                                placeholder='+910000000000'
                                placeholderTextColor='#626262'
                                baseColor='#E0E0E0'
                                textColor='#626262'
                                lineType='none'
                                fontFamily='SFProText-Regular'
                                fontSize={18}
                                maxLength={10}
                                inputContainerStyle={{ backgroundColor: 'transparent', width: '90%' ,}}
                                keyboardType='phone-pad'
                                // formatText={ (text) =>{     return text.replace(/[^+\d]/g, '')}}
                                onSubmitEditing={() => {
                                    let { current: field } = mobileNumRef

                                    console.log(field.value())
                                }
                                }
                                onChangeText={(text) => {
                                    setmobileNum(text);
                                }}
                                value={mobileNum}
                                ref={mobileNumRef}
                                style={{ backgroundColor: 'transparent', width: '85%' }}
                            />

                            <View style={{flex:0.4, backgroundColor:'transparent', justifyContent:'space-between', width:'90%', marginTop: 10}}>
                                {/* <Button title="Show Date Picker" onPress={() => { setDatePickerVisibility(true) }} /> */}
                                <Text style={{
                                    color: '#E0E0E0',

                                    width: '90%',

                                    textAlign: 'left',

                                    fontFamily: 'SFProText-Regular',
                                    fontSize: 12
                                }}>Date of Birth</Text>

                                {/* <TouchableHighlight
                                    style={ { marginTop: 10}}
                                    onPress={showDatepicker}
                                    underlayColor='lightgrey'> */}
                                <TouchableOpacity onPress={showDatepicker}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems:'center' }}>
                                        <Image source={require('./assets/date_icon.png')} style={{ width: 20, height: 20 }} />
                                        <Text style={{
                                            color: '#E0E0E0',

                                            width: '90%',

                                            textAlign: 'left',

                                            fontFamily: 'SFProText-Regular',
                                            fontSize: 14,
                                            marginLeft:10
                                        }}>{date}</Text>
                                    </View>
                                    {/* </TouchableHighlight> */}
                                </TouchableOpacity>



                               
                            </View>

                            <View style={{flex:0.5, backgroundColor:'transparent', justifyContent:'space-evenly', width:'90%', marginTop:10}}>
                                {/* <Button title="Show Date Picker" onPress={() => { setDatePickerVisibility(true) }} /> */}
                                <Text style={{
                                    color: '#E0E0E0',

                                    width: '90%',

                                    textAlign: 'left',

                                    fontFamily: 'SFProText-Regular',
                                    fontSize: 12
                                }}>Gender</Text>

                                <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:20}}>
                                <TouchableHighlight
                                   style={{width:'40%', height:45, backgroundColor:'transparent', borderRadius:10, borderColor:malegender,
                                    borderWidth:2,
                                    flexDirection: 'row', justifyContent: 'center', alignItems:'center' , alignContent:'center'
                                }}
                                    onPress={() =>  {
                                        setmalegender('#E8EB59');
                                        setfemalegender('transparent');
                                        setGender('male')
                                    }}
                                    underlayColor='lightgrey'>
                                    <View style={{  }}>
                                         
                                        <Text style={{
                                            color: '#E0E0E0',

                                            width: '100%',

                                            textAlign: 'center',

                                            fontFamily: 'SFProText-Regular',
                                            fontSize: 16,
                                            // marginLeft:10
                                        }}>Male</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight
                                   style={{width:'40%', height:45, backgroundColor:'transparent', borderRadius:10, borderColor:femalegender,
                                    borderWidth:2,
                                    flexDirection: 'row', justifyContent: 'center', alignItems:'center' , alignContent:'center'
                                }}
                                    onPress={() =>  {
                                        setfemalegender('#E8EB59');
                                        setmalegender('transparent');
                                        setGender('female')

                                    }}
                                    underlayColor='lightgrey'>
                                    <View style={{ }}>
                                         
                                        <Text style={{
                                            color: '#E0E0E0',

                                            width: '100%',

                                            textAlign: 'center',

                                            fontFamily: 'SFProText-Regular',
                                            fontSize: 16,
                                            // marginLeft:10
                                        }}>Female</Text>
                                    </View>
                                </TouchableHighlight>
                                </View>


                              
                            </View>

                        </View>



                        <View style={{ width: '100%', height: 25, backgroundColor: 'transparent', justifyContent:'center', alignItems:'center', marginTop:20 }}>
                        <Text style={{
                                color: '#E0E0E0',

                                width: '100%',

                                textAlign: 'center',

                                fontFamily: 'SFProText-Regular',
                                fontSize: 12
                            }}>1/2</Text>
                        </View>


                    </View>
                    <View style={{
                        flex: 0.2, justifyContent: 'space-evenly', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent', height:150
                    }}>
                        <Text style={{
                            color: '#E0E0E0',

                            width: '85%',

                            textAlign: 'left',

                            fontFamily: 'SFProText-Regular',
                            fontSize: 12
                        }}>By clicking continue you agree to share your information with our partner bank.

                        </Text>

                        <TouchableHighlight
                            style={styles.doneButton}
                            onPress={() => goToRoute()}
                            underlayColor='#E8EB59'>
                            <Text style={styles.submitTitle}>Continue</Text>
                        </TouchableHighlight>

                    </View>

                </View>
            </ScrollView>
            {/* <DateTimePickerModal
                                    isVisible={isDatePickerVisible}
                                    mode="date"
                                    onConfirm={(date) => { console.log(date) }}
                                    onCancel={hideDatePicker}
                                /> */}
            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={new Date()}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
        </SafeAreaView>







    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 90,
        height: 25,
resizeMode:'contain'

    },
    imagelogo: {
        width: 36,
        height: 40,
        // marginTop: 32,
        // marginBottom: 20
    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '85%',
        height: 51,
        backgroundColor: '#E8EB59',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        fontSize: 15,
        color: 'black',
fontFamily:'SF-Pro-Rounded-Semibold',
left:wp('1%')
    }
});

export default PanCardDetailsPage;
