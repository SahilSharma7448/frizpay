/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TextInput,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/FontAwesome';

 import { Shadow } from 'react-native-shadow-2';
 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
import { ImageBackground } from 'react-native';
import { CheckBox } from 'react-native-elements'
 const Payment = ({ navigation }) => {
    const [activeTab, setActiveTab] = useState('0')
const [showCardForm,setCardForm]=useState(false)
 const [saveUPI,setUPI]=useState()
 const [saveCard,setCard]=useState(false)
 const [showUpi,setUpi]=useState(false)
     return (
         <SafeAreaView style={{ flex: 1 }}>
             <View style={{ flex: 1, backgroundColor: '#060606', }}>
                 {/* <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}> */}
                 {/* //paddingBottom:450 */}
                 <ScrollView>
 
 
                 <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'flex-start', flexDirection: 'column', alignItems: 'center', alignContent: 'space-between' }}>
 
                     <View style={{
                         flex: 0.1, justifyContent: 'space-around', flexDirection: 'row',
                         alignItems: 'center', backgroundColor: 'transparent', width: '100%'
                     }}>
 
                         <TouchableHighlight
                             style={{ width: '10%', height: '100%', backgroundColor: 'transparent', justifyContent: 'center', marginLeft: -120, marginRight: 0, position: 'absolute' }}
 
                             onPress={() => {
                                 // navigation.navigate("LoggedInHomePage")
                                 // console.log(navigation.getParent());
                                 // navigation.toggleDrawer();
                                 // navigationContext.navigate('Notifications');
                                 // navigationContext.navigate('HomeTabPage',
                                 //     {
                                 //         screen: 'Home',
                                 //         // initial: false,
                                 //         params: {
                                 //             screen: 'LoggedInHomePage'
                                 //         }
                                 //     }
                                 // );
 
                                 // navigation.goBack();
 
                                 // navigation.popToTop();
                                 // navigation.goBack(navigation.getState().routes[0].key);
                                 // navigation.navigate('SettingsScreen2');
 navigation.goBack()
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{}} />
 
 
                         </TouchableHighlight>
 
                         <View style={{ width: 113, height: 30, }} >
                             <Image style={{ flex: 1, width: undefined, height: undefined, backgroundColor: 'transparent' }}
                                 source={require('./assets/Frizpay.png')}
                                 resizeMode='center'
                             />
                         </View>
                         {/* <Image source={require('./assets/Fri-zpay-banner.png')}  width={113} height={30} style={{ marginLeft: 0, marginRight: 0 }} resizeMode='stretch' /> */}
                         {/* <Image source={require('./assets/top-bell-icon.png')} width={0} height={0} /> */}
 
                     </View>
                     {/* <ScrollView style={{ flex: 1 , height:'10%', width:'100%' }} contentContainerStyle={{ flexGrow: 1, height:'100%', width:'100%', alignContent:'center', flex:1}}> */}
 
 
                             
 <View style={{flexDirection:'row',width:wp('90%'),justifyContent:'space-between',marginTop:hp('5%'),alignSelf:'center'}}>
                    <View style={{width:wp('60%')}}>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             fontFamily:'displayB',
                             fontSize: 25,
                         }}>Add money</Text>
                        
</View>
<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                       fontFamily:'roundedBold',
                             marginTop:hp('0.5%'),
                             fontSize: 20,
                         }}>₹ 1500</Text>

</View>
<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 15,
                             width:wp('90%'),
                             marginTop:hp('3%')
                         }}>UPI</Text>





<View style={{width:wp('90%'),flexDirection:'row',alignSelf:'center',marginTop:hp('2%'),marginBottom:hp('1%')}}>
<View>
{/* <Image source={require('./assets/ridhima.png')} style={{height:40,width:40,borderRadius:50}} resizeMode="contain" /> */}
      <View style={{height:hp('7%'),width:wp('14%'),backgroundColor:'green',borderRadius:50,alignItems:'center',justifyContent:'center'}}>
      <Ionicons name={'whatsapp'} size={30} color={'white'}></Ionicons>
      </View>
          </View>
          <View style={{marginLeft:wp('10%')}}>
{/* <Image source={require('./assets/ridhima.png')} style={{height:40,width:40,borderRadius:50}} resizeMode="contain" /> */}
<TouchableOpacity onPress={()=>setUpi(true)}>
<View style={{height:hp('7%'),width:wp('14%'),backgroundColor:'#3F3F3F',borderRadius:50,alignItems:'center',justifyContent:'center'}}>
      <Ionicons name={'plus'} size={20} color={'#E8EB59'}></Ionicons>
      </View>
      </TouchableOpacity>
          </View>
              </View>

<View style={{width:wp('90%'),backgroundColor:'#333333',alignSelf:'center',justifyContent:'center',alignItems:'center',borderRadius:10,marginTop:hp('5%')}}>
    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
        <Image source={require('./assets/upi.png')} style={{height:40,width:40}} resizeMode={'contain'}></Image>
<Text style={{width:wp('70%'),color:'white',marginLeft:wp('4%'),fontSize:13}}>Shubhamsharma11999@frizpay</Text>
    </View>

    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
        <Image source={require('./assets/upi.png')} style={{height:40,width:40}} resizeMode={'contain'}></Image>
<Text style={{width:wp('70%'),color:'white',marginLeft:wp('4%'),fontSize:13}}>Shubhamsharma11999@frizpay</Text>
    </View>
</View>
{showUpi==true?
<View>
<View style={{width:wp('90%'),backgroundColor:'#333333',alignSelf:'center',justifyContent:'center',alignItems:'center',borderRadius:10,marginTop:hp('5%')}}>
<TextInput style={{width:wp('90%'),marginLeft:wp('3%'),fontSize:13,color:'#D9D9D9'}} placeholder={'Enter your UPI ID'} placeholderTextColor={'#D9D9D9'} keyboardType={'number-pad'}></TextInput>
   
</View>

<CheckBox
  title='Save UPI ID'
  checked={saveUPI}
  textStyle={{color:'white',bottom:2}}
  checkedColor={'#E8EB59'}
  uncheckedColor={'#E8EB59'}
  style={{backgroundColor:'black'}}
  containerStyle={{backgroundColor:'#060606',borderColor:'#060606',width:wp('90%'),justifyContent:'center',marginTop:hp('2%')}}
  size={20}
  onPress={()=>setUPI(!saveUPI)}
/>

<View style={{ height: hp('6%'), width: wp('85%'), marginTop: hp('1%'), backgroundColor: '#E8EB59', borderRadius: 10, alignItems: 'center', justifyContent: 'center',alignSelf:'center' }}>
            <Text style={{fontWeight:'700',color:'black'}}>Verify and pay</Text>
          </View>
          </View>:null}

<View style={{ borderBottomColor: 'white', borderBottomWidth: 0.8, width: wp('90%'), alignSelf: 'center',marginTop:hp('5%') }}></View>


<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 15,
                             width:wp('90%'),
                             marginTop:hp('3%')
                         }}>Credit/Debit Card</Text>

<View style={{width:wp('90%'),backgroundColor:'white',alignSelf:'center',justifyContent:'center',alignItems:'center',borderRadius:10,marginTop:hp('5%')}}>
    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
        <Image source={require('./assets/icici.png')} style={{height:40,width:70}} resizeMode={'contain'}></Image>
<Text style={{width:wp('60%'),color:'#727272',marginLeft:wp('4%'),fontSize:15,top:1}}>***********************17</Text>
    </View>

    
</View>
<TouchableOpacity onPress={()=>setCardForm(true)}>
<View style={{height:hp('6%'),width:wp('90%'),backgroundColor:'#3F3F3F',borderRadius:10,alignItems:'center',justifyContent:'center',marginTop:hp('3%')}}>
      <Ionicons name={'plus'} size={20} color={'#E8EB59'}></Ionicons>
      </View>
      </TouchableOpacity>
{showCardForm==true?
<View>
    <ImageBackground source={require('./assets/backCard.png')} style={{height:hp('30%'),width:wp('90%'),marginTop:hp('5%'),alignSelf:'center',justifyContent:'center',alignItems:'center'}} resizeMode={'stretch'}>
<View style={{width:wp('80%'),height:hp('25%'),backgroundColor:'black',alignSelf:'center',borderRadius:20}}>
<TextInput style={{width:wp('60%'),marginLeft:wp('3%'),marginTop:hp('2%'),fontSize:10,color:'white'}} placeholder={'Name on card'} placeholderTextColor={'white'}></TextInput>
<View style={{height:hp('0.1%'),width:wp('60%'),marginLeft:wp('3%'),backgroundColor:'white',bottom:hp('2%')}}></View>
<TextInput style={{width:wp('60%'),marginLeft:wp('3%'),fontSize:10,color:'white'}} placeholder={'Card number'} placeholderTextColor={'white'} keyboardType={'number-pad'}></TextInput>
<View style={{height:hp('0.1%'),width:wp('60%'),marginLeft:wp('3%'),backgroundColor:'white',bottom:hp('2%')}}></View>

<View style={{flexDirection:'row',}}>
    <View>
    <TextInput style={{width:wp('25%'),marginLeft:wp('3%'),fontSize:10,color:'white'}} placeholder={'Valid through'} placeholderTextColor={'white'}></TextInput>
<View style={{height:hp('0.1%'),width:wp('25%'),marginLeft:wp('3%'),backgroundColor:'white',bottom:hp('2%')}}></View>
    </View>

    <View style={{marginLeft:wp('5%')}}>
    <TextInput style={{width:wp('25%'),marginLeft:wp('3%'),fontSize:10,color:'white'}} placeholder={'CVV'} placeholderTextColor={'white'} keyboardType={'number-pad'} secureTextEntry={true}></TextInput>
<View style={{height:hp('0.1%'),width:wp('25%'),marginLeft:wp('3%'),backgroundColor:'white',bottom:hp('2%')}}></View>
    </View>
</View>



</View>


    </ImageBackground>
    <CheckBox
  title='Securely save details'
  checked={saveCard}
  textStyle={{color:'white',bottom:2}}
  checkedColor={'#E8EB59'}
  uncheckedColor={'#E8EB59'}
  style={{backgroundColor:'black'}}
  containerStyle={{backgroundColor:'#060606',borderColor:'#060606',width:wp('90%'),justifyContent:'center',marginTop:hp('2%')}}
  size={20}
  onPress={()=>setCard(!saveCard)}
/>

<View style={{ height: hp('6%'), width: wp('80%'), marginTop: hp('2%'), backgroundColor: '#E8EB59', borderRadius: 10, alignItems: 'center',marginRight:wp('2%'), justifyContent: 'center',alignSelf:'center' }}>
            <Text style={{fontWeight:'700',color:'black'}}>Verify and pay</Text>
          </View>
</View>
    
    :null}



      <View style={{ borderBottomColor: 'white', borderBottomWidth: 0.8, width: wp('90%'), alignSelf: 'center',marginTop:hp('5%') }}>

   
      </View>

      <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 15,
                             width:wp('90%'),
                             marginTop:hp('3%')
                         }}>Net Banking</Text>

<View style={{width:wp('90%'),flexDirection:'row',alignSelf:'center',marginTop:hp('2%'),marginBottom:hp('1%')}}>
<View>
<View style={{height:hp('7%'),width:wp('14%'),backgroundColor:'white',borderRadius:50,alignItems:'center',justifyContent:'center'}}>
<Image source={require('./assets/sbi.png')} style={{height:40,width:35}} resizeMode="contain" />

      </View>
          </View>
          <View style={{marginLeft:wp('10%')}}>
{/* <Image source={require('./assets/ridhima.png')} style={{height:40,width:40,borderRadius:50}} resizeMode="contain" /> */}
<View style={{height:hp('7%'),width:wp('14%'),backgroundColor:'white',borderRadius:50,alignItems:'center',justifyContent:'center'}}>
<Image source={require('./assets/sbi.png')} style={{height:40,width:35}} resizeMode="contain" />

      </View>
          </View>
              </View>

              <TextInput style={{  color: 'grey', alignSelf: 'center', width: wp('90%'), right: 5,fontSize:15, }} placeholder={'Select Bank'} placeholderTextColor={'grey'}></TextInput>
              <View style={{ borderBottomColor: 'grey', borderBottomWidth: 1, width: wp('90%'), alignSelf: 'center',marginBottom:hp('1%') }}></View>







              <View style={{ borderBottomColor: 'white', borderBottomWidth: 0.8, width: wp('90%'), alignSelf: 'center',marginTop:hp('5%') }}></View>









              <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 15,
                             width:wp('90%'),
                             marginTop:hp('3%')
                         }}>Wallets</Text>

<View style={{width:wp('90%'),flexDirection:'row',alignSelf:'center',marginTop:hp('2%'),marginBottom:hp('10%')}}>
<View>
<View style={{height:hp('7%'),width:wp('14%'),backgroundColor:'white',borderRadius:50,alignItems:'center',justifyContent:'center'}}>
<Image source={require('./assets/paytm.png')} style={{height:40,width:35}} resizeMode="contain" />

      </View>
          </View>
          <View style={{marginLeft:wp('10%')}}>
{/* <Image source={require('./assets/ridhima.png')} style={{height:40,width:40,borderRadius:50}} resizeMode="contain" /> */}
<View style={{height:hp('7%'),width:wp('14%'),backgroundColor:'white',borderRadius:50,alignItems:'center',justifyContent:'center'}}>
<Image source={require('./assets/airtel.png')} style={{height:30,width:35}} resizeMode="contain" />

      </View>
          </View>


          <View style={{marginLeft:wp('10%')}}>
{/* <Image source={require('./assets/ridhima.png')} style={{height:40,width:40,borderRadius:50}} resizeMode="contain" /> */}
<View style={{height:hp('7%'),width:wp('14%'),backgroundColor:'white',borderRadius:50,alignItems:'center',justifyContent:'center'}}>
<Image source={require('./assets/paytm.png')} style={{height:40,width:35}} resizeMode="contain" />

      </View>
          </View>

              </View>

                 </View>
 
             </ScrollView>
              
 
             </View>
         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default Payment;
 