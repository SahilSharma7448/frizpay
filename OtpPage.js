/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Alert,
    Image, Button, TouchableHighlight, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard
} from 'react-native';

import OTPInputView from '@twotalltotems/react-native-otp-input'
import * as Utility from './App/utility'
import { useDispatch, useSelector } from 'react-redux'
import { Otp_Send_Api } from './App/Action/Auth'
import Loader from './App/Components/Loader'
const OtpPage = ({ navigation }) => {
    const mobile = useSelector(state => state.REGISTER_MOBILE);
    const dispatch = useDispatch()
    const [otp, setOTP] = useState('')
    const [loading, setLoading] = useState(false)

    const goToRoute = async () => {
        // navigation.navigate('NameAndPinPage')
        console.log('checkkkkk otp', otp)
        if (await Utility.isFieldEmpty(otp)) {
            return Alert.alert('Please fill otp field')
        }

        let gender = await Utility.getFromLocalStorge('gender')
        if (gender == 'parent') {
            navigation.navigate('NameAndPinPage')
        }
        else {
            navigation.navigate('BlueCard')
        }
    }

    const Resend_Otp = async () => {
        setLoading(true)
        // navigation.navigate('OtpPage')
        let res = await dispatch(Otp_Send_Api(mobile))
        if (res.status == 'success') {
            setLoading(false)
            return Alert.alert('Otp Send Successfull')
        }
        else {
            setLoading(false)

            return Alert.alert('Something went wrong')
        }
    }
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <Loader isLoading={loading}></Loader>

            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>

                <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-evenly' }}>
                    <View style={{
                        flex: 0.8, backgroundColor: '#060606', flexDirection: 'column',
                        justifyContent: 'flex-start'
                    }}>

                        <View style={{
                            flex: 0.25, justifyContent: 'space-evenly', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent'
                        }}>
                            <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                            <Image source={require('./assets/Frizpay.png')} style={styles.image} />
                        </View>

                        <View style={{
                            flex: 0.06, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'flex-start', backgroundColor: 'transparent', marginLeft: 10, marginRight: 10
                        }}>
                            <Text style={{
                                color: '#E0E0E0',

                                width: '100%',

                                textAlign: 'left',

                                fontFamily: 'SFPRODISPLAYMEDIUM',
                                fontSize: 32
                            }}>Enter the code to verify your number</Text>

                            <TouchableHighlight
                                style={{ backgroundColor: 'transparent' }}
                                onPress={() => { }}
                                underlayColor='#E8EB59'>
                                <Text style={styles.resendotp}>+91 9999999999 Change</Text>
                            </TouchableHighlight>

                        </View>




                        <View style={{
                            flex: 0.4, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent'
                        }}>
                            <OTPInputView
                                style={{ width: '80%', height: 50, backgroundColor: 'transparent' }}
                                pinCount={4}
                                // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                // onCodeChanged = {code => { this.setState({code})}}
                                autoFocusOnLoad={false}
                                codeInputFieldStyle={styles.underlineStyleBase}
                                codeInputHighlightStyle={styles.underlineStyleHighLighted}

                                onCodeFilled={(code) => {
                                    console.log(`Code is ${code}, you are good to go!`)
                                    setOTP(code)
                                }}
                            />
                        </View>


                    </View>






                    <View style={{
                        flex: 0.2, justifyContent: 'space-evenly', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent'
                    }}>


                        <TouchableHighlight
                            style={{ backgroundColor: 'transparent' }}
                            onPress={() => Resend_Otp()}
                            underlayColor='#E8EB59'>
                            <Text style={styles.resendotp}>Resend OTP</Text>
                        </TouchableHighlight>



                        <TouchableHighlight
                            style={styles.doneButton}
                            onPress={() => goToRoute()}
                            underlayColor='#E8EB59'>
                            <Text style={styles.submitTitle}>Continue</Text>
                        </TouchableHighlight>

                    </View>


                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 90,
        height: 25,
        resizeMode: 'contain'

    },
    imagelogo: {
        width: 36,
        height: 40,
        // marginTop: 32,
        // marginBottom: 20
    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '85%',
        height: 51,
        backgroundColor: '#E8EB59',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 17,
        color: '#000000',


    },
    resendotp: {
        textAlign: 'center',

        fontFamily: 'SFProText-Regular',
        fontSize: 12,
        color: '#E0E0E0',


    },
    borderStyleBase: {
        width: 30,
        height: 45
    },

    borderStyleHighLighted: {
        borderColor: "#03DAC6",
    },

    underlineStyleBase: {
        width: 60,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
    },

    underlineStyleHighLighted: {
        borderColor: "#03DAC6",
    },
});

export default OtpPage;
