/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/Ionicons';

 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
 
 const NotificationAlert = ({ navigation }) => {
 const [offer,setOffer]=useState(false)
 const [event,setEvent]=useState(false)
 const [whatsapp,setWhatsapp]=useState(false)
 const [family,setFamily]=useState(false)
 
     return (
         <SafeAreaView style={{ flex: 1,backgroundColor:'#060606' }}>
             <View style={{width:wp('95%'),alignSelf:'center',marginTop:hp('5%'),flexDirection:'row'}}>
<TouchableOpacity
 
                             onPress={() => {
                                
 
                                 navigation.goBack();
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{top:hp('1.5%')}} />
 
 
                         </TouchableOpacity>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
 
                             fontFamily: 'SFPRODISPLAYBOLD',
                            // fontWeight:'800',
                             fontSize: 28
                         }}>Notificaton and alert</Text>
</View>



<View style={{ width: wp('90%'), alignSelf: "center", justifyContent: "center", alignItems: "center", flexDirection: "row" ,marginTop:hp('5%')}}>
          <Image source={require('./assets/offer.png')} style={{height:35,width:35}} resizeMode="contain" />
          <View style={{ width: wp('60%'),  marginLeft:wp('6%'),marginTop:hp('1%') }}>
            <Text style={{color:'#C0C0C0',fontSize:18,fontFamily:'SFPRODISPLAYMEDIUM'}}>Offers notifications</Text>
            <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}></Text>

          </View>
          <Switch value={offer} color="#8C8C8C" style={{bottom:5}} onChange={()=>setOffer(!offer)} thumbColor={'#E8EB59'} />

        </View>
        <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('81%'), alignSelf: 'flex-end',marginTop:hp('1%')}}></View>
 





        <View style={{ width: wp('90%'), alignSelf: "center", justifyContent: "center", alignItems: "center", flexDirection: "row",marginTop:hp('3%') }}>
          <Image source={require('./assets/calendar.png')} style={{height:35,width:35}} resizeMode="contain" />
          <View style={{ width: wp('60%'),  marginLeft:wp('6%'),marginTop:hp('1%') }}>
            <Text style={{color:'#C0C0C0',fontSize:18,fontFamily:'SFPRODISPLAYMEDIUM'}}>Event notification</Text>
            <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}></Text>

          </View>
          <Switch value={event} color="#8C8C8C"  style={{bottom:5}} onChange={()=>setEvent(!event)} thumbColor={'#E8EB59'} />

        </View>
        <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('81%'), alignSelf: 'flex-end',marginTop:hp('1%') }}></View>
 







        <View style={{ width: wp('90%'), alignSelf: "center", justifyContent: "center", alignItems: "center", flexDirection: "row",marginTop:hp('3%') }}>
          <Image source={require('./assets/whatsapp.png')} style={{height:35,width:35}} resizeMode="contain" />

         
          <View style={{ width: wp('60%'),  marginLeft:wp('6%'),marginTop:hp('1%') }}>
            <Text style={{color:'#C0C0C0',fontSize:18,fontFamily:'SFPRODISPLAYMEDIUM'}}>Whatsapp</Text>
            <Text style={{color:'#C0C0C0',fontSize:10,fontFamily:'SFPRODISPLAYMEDIUM'}}>For reminders of offers and alerts</Text>

          </View>
          <Switch value={whatsapp} color="#8C8C8C"  style={{bottom:5}} onChange={()=>setWhatsapp(!whatsapp)} thumbColor={'#E8EB59'}/>

        </View>
        <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('81%'), alignSelf: 'flex-end',marginTop:hp('3%') }}></View>
 















 
 <View style={{ width: wp('90%'), alignSelf: "center", justifyContent: "center", alignItems: "center", flexDirection: "row",marginTop:hp('3%') }}>
          <Image source={require('./assets/parents_logo.png')} style={{height:35,width:35}} resizeMode="contain" />
          <View style={{ width: wp('60%'),  marginLeft:wp('6%'),marginTop:hp('1%') }}>
            <Text style={{color:'#C0C0C0',fontSize:18,fontFamily:'SFPRODISPLAYMEDIUM'}}>Family Notifications</Text>
            <Text style={{color:'#C0C0C0',fontSize:10,fontFamily:'SFPRODISPLAYMEDIUM'}}>For reminders of family people</Text>

          </View>
          <Switch value={family} color="#8C8C8C"  style={{bottom:5}} onChange={()=>setFamily(!family)} thumbColor={'#E8EB59'}/>

        </View>
        <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('81%'), alignSelf: 'flex-end',marginTop:hp('3%') }}></View>

                 
             
         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default NotificationAlert;
 