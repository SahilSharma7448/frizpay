/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/Ionicons';

 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
 
 const TaskList = ({ navigation }) => {
    const [activeTab, setActiveTab] = useState('0')

 
     return (
         <SafeAreaView style={{ flex: 1 , backgroundColor: '#060606',}}>
           
               
<ScrollView>
<View style={{width:wp('95%'),alignSelf:'center',marginTop:hp('5%'),flexDirection:'row'}}>
<TouchableOpacity
 
                             onPress={() => {
                                
 
                                 navigation.goBack();
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{top:hp('0.8%')}} />
 
 
                         </TouchableOpacity>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
 
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                            fontWeight:'800',
                             fontSize: 32
                         }}>Tasks</Text>
</View>




<View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', width: wp('80%'), marginTop: hp('2%') }}>
        <View style={{ height: hp('10%'), width: wp('40%'), alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          {/* <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center', }}>Total Sent</Text>
          <Text style={{ fontSize: 22, color: 'white', textAlign: 'center',fontWeight:'700' }}>₹ 2500</Text> */}
          <Text style={{ fontSize: 30, color: 'white', textAlign: 'center',fontWeight:'700' }}>2</Text>
      
          <Text style={{ fontSize: 12, color: 'white', textAlign: 'center',marginTop:hp('1%') }}> Open Tasks</Text>


        </View>
        <View style={{ height: hp('10%'), width: wp('40%'),  alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
        {/* <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center', }}>Total Spent</Text>
          <Text style={{ fontSize: 22, color: 'white', textAlign: 'center',fontWeight:'700' }}>₹ 2500</Text> */}
          <Text style={{ fontSize: 30, color: 'white', textAlign: 'center',fontWeight:'700' }}>3</Text>
        
          <Text style={{ fontSize: 12, color: 'white', textAlign: 'center',marginTop:hp('1%') }}> Due This  week</Text>


        </View>


      </View>
<TouchableOpacity onPress={()=>navigation.navigate('AddTask')}>
      <View style={{ height: hp('7%'), width: wp('88%'), marginTop: hp('2%'),alignSelf:'center', backgroundColor: '#E8EB59', borderRadius: 10, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{fontFamily: 'SF-Pro-Rounded-Semibold',color:'black'}}>Create a Task</Text>
          </View>
          </TouchableOpacity>

          <View style={{ width: wp('90%'), height: hp('5%'), backgroundColor: '#313131', alignSelf: 'center', borderRadius: 10, flexDirection: 'row',marginTop:hp('3%') }}>
        <TouchableOpacity onPress={() => setActiveTab('0')}>
          <View style={{ width: wp('45%'), height: hp('5%'), backgroundColor: activeTab == '0' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{color:'white',fontWeight:'900'}}>Open Task</Text>

          </View>
        </TouchableOpacity >
       
        <TouchableOpacity onPress={() => setActiveTab('2')}>

          <View style={{ width: wp('45%'), height: hp('5%'), backgroundColor: activeTab == '2' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{color:'white',fontWeight:'900'}}>Completed</Text>

          </View>
        </TouchableOpacity>
      </View>


      <Text style={{width:wp('90%'),alignSelf:'center',marginTop:hp('3%'),color:'white',fontSize:17,fontFamily:'SF-Pro-Rounded-Semibold'}}>Due this week</Text>


      <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: wp('90%'),marginTop:hp('2%') }}>

<View style={{height:hp('6%'),borderRadius:40,backgroundColor:'#4B4B4B',width:wp('12%'),alignItems:'center',justifyContent:'center'}}>
<Text style={{ fontSize: 15, color: 'white', }}>SS</Text>

</View>
<View style={{ width: wp('35%'), }}>
  <Text style={{ fontSize: 12, color: 'white',fontFamily:'SF-Pro-Rounded-Semibold' }}>Room clean</Text>
  <Text style={{ fontSize: 12, color: 'white', marginTop: hp('3%'),fontFamily:'SF-Pro-Rounded-Semibold' }}>To: Shubham</Text>

</View>
{/* <Text style={{ fontSize: 17, color: 'white', textAlign: 'center', }}>23 oct</Text> */}
<View style={{ width: wp('35%'),alignItems:'flex-end' }}>
  <Text style={{ fontSize: 12, color: 'white',fontFamily:'SF-Pro-Rounded-Semibold' }}>28 oct</Text>
  <Text style={{ fontSize: 12, color: 'white', marginTop: hp('3%'),fontFamily:'SF-Pro-Rounded-Semibold' }}>No Rewards</Text>

</View>

</View>


<View style={{ borderWidth: 0.5, borderColor: 'grey', marginTop: hp('2%'),width:wp('90%') ,alignSelf:'center'}}></View>

<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: wp('90%'),marginTop:hp('3%') }}>

<View style={{height:hp('6%'),borderRadius:40,backgroundColor:'#4B4B4B',width:wp('12%'),alignItems:'center',justifyContent:'center'}}>
<Text style={{ fontSize: 15, color: 'white', }}>SS</Text>

</View>
<View style={{ width: wp('35%'), }}>
  <Text style={{ fontSize: 12, color: 'white',fontFamily:'SF-Pro-Rounded-Semibold' }}>Room clean</Text>
  <Text style={{ fontSize: 12, color: 'white', marginTop: hp('3%'),fontFamily:'SF-Pro-Rounded-Semibold' }}>To: Shubham</Text>

</View>
{/* <Text style={{ fontSize: 17, color: 'white', textAlign: 'center', }}>23 oct</Text> */}
<View style={{ width: wp('35%'),alignItems:'flex-end' }}>
  <Text style={{ fontSize: 12, color: 'white',fontFamily:'SF-Pro-Rounded-Semibold' }}>28 oct</Text>
  <Text style={{ fontSize: 12, color: 'white', marginTop: hp('3%'),fontFamily:'SF-Pro-Rounded-Semibold' }}>No Rewards</Text>

</View>

</View>


<View style={{ borderWidth: 0.5, borderColor: 'grey', marginTop: hp('2%'),width:wp('90%'),alignSelf:'center' }}></View>








<Text style={{width:wp('90%'),alignSelf:'center',marginTop:hp('3%'),color:'white',fontSize:17,fontFamily:'SF-Pro-Rounded-Semibold'}}>Due Later</Text>


<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: wp('90%'),marginTop:hp('2%') }}>

<View style={{height:hp('6%'),borderRadius:40,backgroundColor:'#4B4B4B',width:wp('12%'),alignItems:'center',justifyContent:'center'}}>
<Text style={{ fontSize: 15, color: 'white', }}>SS</Text>

</View>
<View style={{ width: wp('35%'), }}>
<Text style={{ fontSize: 12, color: 'white',fontFamily:'SF-Pro-Rounded-Semibold' }}>Room clean</Text>
<Text style={{ fontSize: 12, color: 'white', marginTop: hp('3%') ,fontFamily:'SF-Pro-Rounded-Semibold'}}>To: Shubham</Text>

</View>
{/* <Text style={{ fontSize: 17, color: 'white', textAlign: 'center', }}>23 oct</Text> */}
<View style={{ width: wp('35%'),alignItems:'flex-end' }}>
<Text style={{ fontSize: 12, color: 'white',fontFamily:'SF-Pro-Rounded-Semibold' }}>28 oct</Text>
<Text style={{ fontSize: 12, color: 'white', marginTop: hp('3%'),fontFamily:'SF-Pro-Rounded-Semibold' }}>No Rewards</Text>

</View>

</View>


<View style={{ borderWidth: 0.5, borderColor: 'grey', marginTop: hp('2%'),width:wp('90%'),alignSelf:'center' }}></View>

<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: wp('90%'),marginTop:hp('3%') }}>

<View style={{height:hp('6%'),borderRadius:40,backgroundColor:'#4B4B4B',width:wp('12%'),alignItems:'center',justifyContent:'center'}}>
<Text style={{ fontSize: 15, color: 'white', }}>SS</Text>

</View>
<View style={{ width: wp('35%'), }}>
<Text style={{ fontSize: 12, color: 'white',fontFamily:'SF-Pro-Rounded-Semibold' }}>Room clean</Text>
<Text style={{ fontSize: 12, color: 'white', marginTop: hp('3%') ,fontFamily:'SF-Pro-Rounded-Semibold'}}>To: Shubham</Text>

</View>
{/* <Text style={{ fontSize: 17, color: 'white', textAlign: 'center', }}>23 oct</Text> */}
<View style={{ width: wp('35%'),alignItems:'flex-end' }}>
<Text style={{ fontSize: 12, color: 'white', fontFamily:'SF-Pro-Rounded-Semibold'}}>28 oct</Text>
<Text style={{ fontSize: 12, color: 'white', marginTop: hp('3%'),fontFamily:'SF-Pro-Rounded-Semibold' }}>No Rewards</Text>

</View>

</View>


<View style={{ borderWidth: 0.5, borderColor: 'grey', marginTop: hp('2%'),width:wp('90%'),alignSelf:'center' }}></View>





</ScrollView>

         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default TaskList;
 