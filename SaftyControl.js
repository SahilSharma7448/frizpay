/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/Ionicons';

 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
 import MaterialIcons from "react-native-vector-icons/MaterialIcons"
 const SaftyControl = ({ navigation }) => {
 const [biomatric,setBioMatric]=useState(false)
 const [card,setCard]=useState(false)
 
     return (
         <SafeAreaView style={{ flex: 1 ,backgroundColor:'#060606'}}>
          <View style={{width:wp('95%'),alignSelf:'center',marginTop:hp('5%'),flexDirection:'row'}}>
<TouchableOpacity
 
                             onPress={() => {
                                
 
                                 navigation.goBack();
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{top:hp('0.5%')}} />
 
 
                         </TouchableOpacity>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
 
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                            fontFamily:'displayB',
                             fontSize: 28
                         }}>Safety and control</Text>
</View>



<View style={{ width: wp('90%'), alignSelf: "center", justifyContent: "center", alignItems: "center", flexDirection: "row",marginTop:hp('5%') }}>
          {/* <Image source={require('./assets/offer.png')} style={{height:30,width:30}} resizeMode="contain" /> */}
          <Ionicons
            name={'finger-print'}
            size={35}
            color={'yellow'}
            // style={{ alignSelf: 'center', alignItems: 'center', justifyContent: "center", marginRight: 20 }}
          />
          <View style={{ width: wp('60%'),  marginLeft:wp('6%'),marginTop:hp('1%') }}>
            <Text style={{color:'#C0C0C0',fontSize:15,fontFamily:'SFPRODISPLAYMEDIUM'}}>Enable Boimetric lock</Text>
            <Text style={{color:'grey',fontSize:10,}}></Text>

          </View>
          <Switch value={biomatric} color="#E8EB59" onChange={()=>setBioMatric(!biomatric)} />

        </View>
        <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('3%')}}></View>
 

        <View style={{ width: wp('90%'), alignSelf: "center", justifyContent: "center", alignItems: "center", flexDirection: "row",marginTop:hp('3%') }}>

          <MaterialIcons
            name={'verified-user'}
            size={35}
            color={'yellow'}
            // style={{ alignSelf: 'center', alignItems: 'center', justifyContent: "center", marginRight: 20 }}
          />
          <View style={{ width: wp('60%'),  marginLeft:wp('6%'),marginTop:hp('1%') }}>
            <Text style={{color:'#C0C0C0',fontSize:15,fontFamily:'SFPRODISPLAYMEDIUM'}}>Allow the card for online transational portals</Text>
            <Text style={{color:'grey',fontSize:10,}}></Text>

          </View>
          <Switch value={card} color="#E8EB59" onChange={()=>setCard(!card)} />

        </View>
        <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('3%') }}></View>
 




        {/* <View style={{ width: wp('90%'), alignSelf: "center", justifyContent: "center", alignItems: "center", flexDirection: "row",marginTop:hp('2%') }}>
          <Image source={require('./assets/eventLogo.png')} style={{height:40,width:40,marginTop:hp('2%')}} resizeMode="contain" />

          <View style={{ width: wp('50%'),  marginLeft:wp('6%'),top:hp('2%') }}>
            <Text style={{color:'#C0C0C0',fontSize:15,fontFamily:'displayB'}}>Transactional limit</Text>
            <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}></Text>

          </View>
          <Text style={{color:'yellow',fontSize:15,marginTop:hp('1%'),fontWeight:'700',width:wp('20%'),textAlign:"center",left:wp('2%')}}>Edit</Text>

        </View> */}


{/* 
        <View style={{flexDirection:'row',width:wp('55%'),alignSelf:'center',justifyContent:'space-between',marginTop:hp('2%')}}>
        <Text style={{color:'white',fontSize:15,fontFamily:'proTxt'}}>₹ 10,000/</Text>
        <Text style={{color:'grey',fontSize:15,left:wp('13%'),fontFamily:'proTxt'}}>₹15,000</Text>

        </View>
        <View style={{ borderWidth: 0.5, borderColor: '#464646', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('3%') }}></View>
  */}







       














         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default SaftyControl;
 