import React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerData from "../../LeftDrawerSettingsPage"
import BottomTab from './ChildBottomTab';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from '../utility'
const Drawer = createDrawerNavigator();

const SideDrawerChild = () => {
    return (
        <Drawer.Navigator
            drawerContent={DrawerData}
            screenOptions={{ headerShown: false ,
            
            drawerStyle:{width:wp('100%'),}}}
            drawerStyle={{width:200}}
           >
            <Drawer.Screen
                name="Home"
                component={BottomTab}
                options={{ drawerLabel: 'open drawer' }}
            />
        </Drawer.Navigator>
    );
};

export default SideDrawerChild;
