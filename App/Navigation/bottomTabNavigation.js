import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Alert, Image, StyleSheet, Text } from 'react-native';
import Home from '../../LoggedInHomePage';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();
const BottomTabNavigator = () => {
  return (
    // <Tab.Navigator
    //   tabBarOptions={
    //     {
    //       // activeTintColor: constants.title_Colors,
    //       // inactiveTintColor: constants.grey_Text,
    //     }
    //   }>
    //   <Tab.Screen
    //     name="Home"
    //     component={Home}
    //     options={({ route }) => ({
    //       tabBarLabel: ({ focused }) => (
    //         <Text
    //           style={{

    //           }}>
    //           Home
    //         </Text>
    //       ),
    //     })}
    //   />

    // </Tab.Navigator>


    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = focused
              ? 'home-sharp'
              : 'home-outline';
          } else if (route.name === 'Settings') {
            iconName = focused ? 'add' : 'add-circle';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: 'white',
        tabBarInactiveTintColor: 'black',
        initialRouteName: "Home",
        headerShown: false,
        tabBarActiveBackgroundColor: 'black',

        tabBarStyle: {
          position: 'absolute',
          borderTopColor: 'transparent',
          borderTopWidth: 0
        },
      })}


    >
      <Tab.Screen name="Home" component={Home} style={{ backgroundColor: 'black' }} />
      {/* <Tab.Screen name="Settings" component={SettingsScreen} /> */}
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;
const styles = StyleSheet.create({});
