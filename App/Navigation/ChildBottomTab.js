import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Alert, Image, StyleSheet, Text } from 'react-native';
import Home from '../Screens/Child/ChildDashBoard';
import Account from '../Screens/Child/Account';
import CoinFund from '../Screens/Child/CoinFund';
import OffersChild from '../Screens/Child/OffersChild'

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
const Tab = createBottomTabNavigator();
const BottomTabNavigator = () => {
  return (



    <Tab.Navigator
      // screenOptions={({ route }) => ({
      //   tabBarIcon: ({ focused, color, size }) => {
      //     let iconName;
      //     if (route.name === 'Home') {
      //       iconName = focused
      //         ? 'home-sharp'
      //         : 'home-outline';
      //     } else if (route.name === 'Settings') {
      //       iconName = focused ? 'add' : 'add-circle';
      //     }

      //     // You can return any component that you like here!
      //     return <Ionicons name={iconName} size={size} color={color} />;
      //   },
 
      //   headerShown: false,

     
      // })}
      screenOptions={({ route }) => ({
        tabBarActiveTintColor: 'white',
        initialRouteName: "SAVINGS",
        headerShown: false,
        tabBarInactiveTintColor: '#7D7D7D',
        tabBarActiveBackgroundColor: 'black',
        tabBarInactiveBackgroundColor:'black',
        tabBarStyle: {
          position: 'absolute',
          borderTopColor: 'transparent',
          borderTopWidth: 0
        },
      })}



    >
 <Tab.Screen name="HOME" component={Home} style={{ backgroundColor: 'black' }}  
      
      options={({ route }) => ({
        tabBarIcon: ({ focused }) => (
            <Ionicons
                name={focused
                          ? 'home-sharp'
                           : 'home-outline'}
                size={24}
                color={focused ? "white" : '#7D7D7D'}
            />
        ),
    })}
      />








<Tab.Screen name="ACCOUNT" component={Account} style={{ backgroundColor: 'black' }}  
      
      options={({ route }) => ({
        tabBarIcon: ({ focused }) => (
          <Image source={focused? require('../../assets/accountw.png'):require('../../assets/accountg.png')} style={{height:20,width:20}} resizeMode={'contain'}></Image>
           
        ),
    })}
      />

     
     
<Tab.Screen name="SHOP" component={OffersChild} style={{ backgroundColor: 'black' }}  
      
      options={({ route }) => ({
        tabBarIcon: ({ focused }) => (
            <MaterialIcons
                name={'shopping-bag'}
                size={24}
                color={focused ? "white" : '#7D7D7D'}
            />
        ),
    })}
      />



    
     
      <Tab.Screen name="SAVINGS" component={CoinFund} style={{ backgroundColor: 'black' }}  
      
      options={({ route }) => ({
        tabBarIcon: ({ focused }) => (
            // <MaterialCommunityIcons
            //     name={'piggy-bank'}
            //     size={24}
            //     color={focused ? "white" : '#7D7D7D'}
            // />


            <Image source={focused? require('../../assets/piggyw.png'):require('../../assets/piggyg.png')} style={{height:25,width:25}} resizeMode={'contain'}></Image>
        ),
    })}
      />
      <Tab.Screen name="REWARDS" component={CoinFund} style={{ backgroundColor: 'black' }}  
      
      options={({ route }) => ({
        tabBarIcon: ({ focused }) => (
          <Image source={focused? require('../../assets/badge.png'):require('../../assets/privacyg.png')} style={{height:20,width:20}} resizeMode={'contain'}></Image>
           
        ),
    })}
      />

      {/* <Tab.Screen name="Settings" component={SettingsScreen} /> */}
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;
const styles = StyleSheet.create({});



