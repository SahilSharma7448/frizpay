import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AppIntro from '../../AppIntro';
import ParentOrTeenPage from "../../ParentOrTeenPage"
import PhoneNumberPage from "../../PhoneNumberPage"
import OtpPage from "../../OtpPage"
import NameAndPinPage from "../../NameAndPinPage"
import PanCardDetailsPage from "../../PanCardDetailsPage"
import PanCardNumberPage from "../../PanCardNumberPage"
import PanDeclarationPage from "../../PanDeclarationPage"
import BottomTab from "./bottomTabNavigation"
import DrawerPage from "./DrawerNavigator"
import NotificationAlert from "../../NotificationAlert"
import GetSupport from "../../GetSupport"
import SaftyControl from "../../SaftyControl"
import ReferInvite from "../../ReferInvite"
import EditFamily from "../../EditFamily"
import EditProfile from "../../EditProfile"
import FamilyDetail from "../../FamilyDetail"
import AddTask from "../../AddTask"
import TaskList from "../../TaskList"
import SendMoney from "../../SendMoney"
import Payment from "../../Payment" 
import NotificationList from "../../NotificationList"
import Learning from "../../Learning"
import  BlueCard from "../Screens/Child/BlueCard"
import GaurdianPhone from "../Screens/Child/GaurdianPhone"
import ParentKYC from "../Screens/Child/ParentKYC"
import VisaCard from "../Screens/Child/VisaCard"
import ChildDashBoard from "../Screens/Child/ChildDashBoard"
import EventList from "../Screens/Child/EventList"
import SelectedEvent from "../Screens/Child/SelectedEvent"
import CoinFund from '../Screens/Child/CoinFund'
import Account from '../Screens/Child/Account'
import ChildAddMoney from '../Screens/Child/ChildAddMoney'
import SideDrawerChild from './SideDrawerCHild'
import OffersChild from '../Screens/Child/OffersChild'
import LeftDrawerSettingsPage from '../../LeftDrawerSettingsPage'
import Login from '../../Login'
const Stack = createStackNavigator();
const MainStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="AppIntro"
        component={AppIntro}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NameAndPinPage"
        component={NameAndPinPage}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="PanCardDetailsPage"
        component={PanCardDetailsPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
          <Stack.Screen
        name="VisaCard"
        component={VisaCard}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="LeftDrawerSettingsPage"
        component={LeftDrawerSettingsPage}
        options={{headerShown: false}}
      />
       
       <Stack.Screen
        name="Account"
        component={Account}
        options={{headerShown: false}}
      />
        
      
     
       
        
      
       <Stack.Screen
        name="AddTask"
        component={AddTask}
        options={{ headerShown: false }}
      />
     
       <Stack.Screen
        name="BottomTab"
        component={BottomTab}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="SideDrawerChild"
        component={SideDrawerChild}
        options={{headerShown: false}}
      />
    
      
      
          
      <Stack.Screen
        name="OffersChild"
        component={OffersChild}
        options={{headerShown: false}}
      />
 
     
      
       <Stack.Screen
        name="ChildAddMoney"
        component={ChildAddMoney}
        options={{headerShown: false}}
      />
     
       <Stack.Screen
        name="CoinFund"
        component={CoinFund}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SelectedEvent"
        component={SelectedEvent}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="EventList"
        component={EventList}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="ChildDashBoard"
        component={ChildDashBoard}
        options={{ headerShown: false }}
      />
  
        <Stack.Screen
        name="ParentKYC"
        component={ParentKYC}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="GaurdianPhone"
        component={GaurdianPhone}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="BlueCard"
        component={BlueCard}
        options={{headerShown: false}}
      />
       
  <Stack.Screen
        name="Learning"
        component={Learning}
        options={{ headerShown: false }}
      />
 
      <Stack.Screen
        name="PanDeclarationPage"
        component={PanDeclarationPage}
        options={{ headerShown: false }}
      />
  

<Stack.Screen
        name="NotificationList"
        component={NotificationList}
        options={{ headerShown: false }}
      />
      
      <Stack.Screen
        name="ParentOrTeenPage"
        component={ParentOrTeenPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PhoneNumberPage"
        component={PhoneNumberPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="OtpPage"
        component={OtpPage}
        options={{ headerShown: false }}
      />


      <Stack.Screen
        name="PanCardNumberPage"
        component={PanCardNumberPage}
        options={{ headerShown: false }}
      />

     
      <Stack.Screen
        name="DrawerPage"
        component={DrawerPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NotificationAlert"
        component={NotificationAlert}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="GetSupport"
        component={GetSupport}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="SaftyControl"
        component={SaftyControl}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="ReferInvite"
        component={ReferInvite}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="EditFamily"
        component={EditFamily}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="FamilyDetail"
        component={FamilyDetail}
        options={{ headerShown: false }}
      />
      
      <Stack.Screen
        name="TaskList"
        component={TaskList}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="SendMoney"
        component={SendMoney}
        options={{ headerShown: false }}
      />
        <Stack.Screen
        name="Payment"
        component={Payment}
        options={{ headerShown: false }}
      />
       
    </Stack.Navigator>
  );
};

export default MainStackNavigator;
