import * as Url from '../api/Url'
import * as Service from '../api/services'
import * as type from '../Redux/Const'
export const Login_Api=(mobile,pin,type)=>async()=>{
    let body ={
        mobile:mobile,
        pin:pin,
        type:type
    }

    console.log('checkkkkkkkkkkkkkk',body)
    let res=await Service.post(Url.LOGIN,'',body)
    console.log('res of login',res)
    return res
}

export const Otp_Send_Api = (mobile) => async (dispatch) => {
let res=await Service.get(Url.Generate_OTP+mobile)
    console.log('checlkkkkkkkkkk', res)
    await dispatch({
        type: type.REGISTER_MOBILE,
        payload: mobile
    })
    return res
}

export const Create_Child_Api = (name, pin, number) => async (dispatch) => {
    let body = {
        name: name,
        pin: pin,
        mobile: number,
        email: '',
        image: '',
        parentKycDone: false

    }
    console.log('check body', body)
    let res = await Service.post(Url.Create_Child, "", body)
    console.log('check response of crerate child', res)
    return res
}

export const Set_parent_Data = (data) => async (dispatch) => {
    console.log('check data', data)
    await dispatch({
        type: type.PARENT_DETAIL,
        payload: data
    })
    return true
}

export const Create_Parent_Api = (data, pancard) => async (dispatch) => {
    let body = {
        name: data.name,
        firstName: data.firstName,
        lastName: data.lastName,
        mobile: data.mobileNum,
        email: '',
        image: '',
        pin: data.pin,
        panCard: pancard,
        gender: data.gender,
        dateOfBirth: data.date,
        memberOfPoliticalParty: false,
        "boolean taxPayerInOtherCountry": false,
        "annualPayment": 100000
    }
    console.log('check body', body)
    let res = await Service.post(Url.Create_Parent, "", body)
    console.log('check response', res)
    return res
}