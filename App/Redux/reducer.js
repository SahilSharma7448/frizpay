import {
SHOW_HEADER,
  REGISTER_MOBILE,
  PARENT_DETAIL
} from './Const';
const initialState = {
  SHOW_HEADER: false,
  REGISTER_MOBILE: '',
  PARENT_DETAIL: {}

};
export default function (state = initialState, { type, payload }) {
  switch (type) {


    case SHOW_HEADER:
      return {
        ...state,
        SHOW_HEADER: payload,
      };
    case REGISTER_MOBILE:
      return {
        ...state,
        REGISTER_MOBILE: payload,
      };
    case PARENT_DETAIL:
      return {
        ...state,
        PARENT_DETAIL: payload,
      };
     
    default: {
      return state;
    }
  }
}
