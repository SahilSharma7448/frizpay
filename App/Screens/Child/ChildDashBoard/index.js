import React from 'react';
import { Text, View,Image,FlatList,TouchableHighlight,StyleSheet } from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "../../../utility"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import Icon from "react-native-vector-icons/FontAwesome"
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
const ChildDashBoard = ({navigation}) => {
  return (
    <View style={{ flex: 1, backgroundColor:'#060606' }}>
   {/* <View style={{height:hp('15%'),width:wp('80%'),
   backgroundColor:'#060606',
   alignSelf:'center',alignItems:'center',borderColor:'#E8EB59',borderWidth:1,borderRadius:10,marginTop:hp('5%'),paddingTop:hp('1%'),
// shadowColor: "#E8EB59",
// shadowOffset: {
// 	width: 0,
// 	height: 1,
// },
// shadowOpacity: 0.20,
// shadowRadius: 1.41,

// elevation: 2,
shadowColor: "#E8EB59",
shadowOffset: {
	width: 0,
	height: 12,
},
shadowOpacity: 0.58,
shadowRadius: 16.00,

elevation: 24,
}}>
<Text style={{color:'white',textAlign:'center'}}>Swipe up for Friz card</Text>
<Icon name={'angle-double-up'} size={20} color={'#E8EB59'}></Icon>
   </View> */}
    <View style={{
                         justifyContent: 'space-between', flexDirection: 'row',
                         alignSelf:'center',
                        alignItems: 'center', backgroundColor: 'transparent', width: wp('95%'),
                        marginTop:hp('2%')
                    }}>
                        {/* <Image source={require('./assets/top-drawer-icon.png')} width={22} height={20} style={{ marginLeft: 10, marginRight: 0 }} /> */}

                        <TouchableHighlight
                                style={{  backgroundColor:'transparent', justifyContent:'center', alignItems:'center'}}

                                onPress={() => navigation.openDrawer()}
                                underlayColor='grey'>
                                 
                                    <Image source={require('../../../../assets/top-drawer-icon.png')} resizeMode='contain'  />

                                
                            </TouchableHighlight>


                        <Image source={require('../../../../assets/Frizpay.png')} style={styles.image} />
                        <TouchableOpacity onPress={()=>navigation.navigate('NotificationList')}>
                        <Image source={require('../../../../assets/top-bell-icon.png')} width={26} height={28} />
                        </TouchableOpacity>
                    </View>
<ScrollView>
<Image source={require('../../../../assets/swipeUp.png')} style={{height:hp('16%'),width:wp('80%'),alignSelf:'center',marginTop:hp('2%')}}
resizeMode={'contain'}
>

</Image>

   <View style={{height:hp('12%'),
   flexDirection:'row',
   justifyContent:'space-between',
   borderRadius:8,
   width:wp('90%'),alignSelf:'center',alignItems:'center',backgroundColor:'#484848', bottom:hp('5%')}}>
<View style={{marginLeft:wp('3%')}}>
    <Text style={{color:'#F9F9F9',fontSize:10,fontWeight:'600',fontFamily:'opensans'}}>Your Balance</Text>
    <Text style={{color:'white',fontWeight:'900',fontSize:25,marginTop:hp('1%')}}>₹ 380.35</Text>

</View>
<TouchableOpacity onPress={()=>navigation.navigate('ChildAddMoney')}>
<View style={{ height: hp('6%'), width: wp('33%'), marginRight:wp('7%'), backgroundColor: '#E8EB59', borderRadius: 10, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{fontFamily:'displayB',color:'black',fontSize:13,left:3,}}>Add Money </Text>
          </View>
          </TouchableOpacity>
   </View>
   <View style={{height:hp('22%'),
   bottom:hp('2%'),
   borderRadius:12,
   width:wp('90%'),alignSelf:'center',backgroundColor:'#484848',}}>
       <View style={{

flexDirection:'row',
justifyContent:'space-between',
       }}>
<View style={{marginLeft:wp('3%')}}>
   
    <Text style={{color:'white',fontFamily:'SFPRODISPLAYBOLD', fontSize:20,marginTop:hp('1%')}}>Quick Pick</Text>

</View>

            <Text style={{color:'#BABABA',marginTop:hp('1.5%'),marginRight:wp('3%')}}>View all </Text>
            </View>

            <FlatList  
                    data={[  
                        {key: 'Android'},{key: 'iOS'}, {key: 'Java'},{key: 'Swift'},  
                       
                    ]}  
                    renderItem={({item}) =>  
                     

  <View style={{height:hp('12%'),width:wp('20%'),backgroundColor:'black',marginLeft:wp('2%'),marginTop:hp('2%'),borderRadius:4,alignItems:'center',justifyContent:'center',
  shadowColor: "white",
  shadowOffset: {
      width: 0,
      height: 12,
  },
  shadowOpacity: 0.58,
  shadowRadius: 16.00,
  
  elevation: 24,
  
  }}>
  <View style={{height:hp('6%'),backgroundColor:'white',borderRadius:60,width:wp('12%'),textAlign:'center',alignSelf:'center',justifyContent:'center',alignItems:'center'}}>
<Text style={{color:'#49443D'}}>AS</Text>

</View>
<Text style={{color:'#E0E0E0',fontSize:12,marginTop:hp('1%')}}>Akshit</Text>

</View>  
                }  
                horizontal
                />  





   </View>




<View style={{flexDirection:'row',width:wp('90%'),alignSelf:'center',alignItems:'center',justifyContent:'space-between',marginTop:hp('2%'),}}>
   
<TouchableOpacity onPress={()=>navigation.navigate('Account')}>
   
    <View style={{height:hp('20%'),width:wp('42%'),backgroundColor:'#484848',borderRadius:10}}>

        <View style={{flexDirection:'row',paddingTop:hp('2%'),marginLeft:wp('3%')}}>
            <Icon name={'credit-card'} size={20} color={'white'}></Icon>
            <Text style={{color:'white',marginLeft:wp('3%'),
            fontSize:21,fontFamily:'SF-Pro-Rounded-Semibold', bottom:hp('0.9%')}}>
                Account</Text>
        </View>
        <Text style={{color:'white',marginLeft:wp('3%'),fontSize:25,fontWeight:'700',marginTop:hp('2%'),}}>₹ 380.35</Text>
        <Text style={{color:'#E0E0E0',marginLeft:wp('3%'),fontSize:13,fontWeight:'700',marginTop:hp('0.3%')}}>Current Account</Text>

    </View>
    </TouchableOpacity>
    <TouchableOpacity onPress={()=>navigation.navigate('TaskList')}>
    <View style={{height:hp('20%'),width:wp('42%'),backgroundColor:'#484848',borderRadius:10}}>



    <View style={{flexDirection:'row',paddingTop:hp('2%'),marginLeft:wp('3%')}}>
            <FontAwesome5
 name={'clipboard-list'} size={20} color={'white'}></FontAwesome5>
            <Text style={{color:'white',marginLeft:wp('3%'),fontSize:21,fontFamily:'SF-Pro-Rounded-Semibold',bottom:hp('0.9%')}}>Tasks</Text>

       
        </View>




        <View style={{flexDirection:'row',paddingTop:hp('2.5%'),marginLeft:wp('3%')}}>
            <View style={{backgroundColor:'red',borderRadius:40,height:13,width:13}}></View>
            <Text style={{color:'white',marginLeft:wp('3%'),fontSize:13,fontFamily:'roundedBold', bottom:hp('0.8%')}}> Read a book for ..</Text>
       
       
        </View>


        <View style={{flexDirection:'row',paddingTop:hp('2%'),marginLeft:wp('3%')}}>
            <View style={{backgroundColor:'#00FF19',borderRadius:40,height:13,width:13,bottom:hp('1')}}></View>
            <Text style={{color:'white',marginLeft:wp('3%'),fontSize:13,fontFamily:'roundedBold', bottom:hp('2%')}}> Clean your room ..</Text>
       
       
        </View>
    </View>
    </TouchableOpacity>

</View>
















<View style={{flexDirection:'row',width:wp('90%'),alignSelf:'center',alignItems:'center',justifyContent:'space-between',marginTop:hp('4%'),}}>
<TouchableOpacity onPress={()=>navigation.navigate('CoinFund')}>
 
    <View style={{height:hp('20%'),width:wp('42%'),backgroundColor:'#484848',borderRadius:10}}>

        <View style={{flexDirection:'row',paddingTop:hp('2%'),marginLeft:wp('3%')}}>
            <MaterialCommunityIcons name={'piggy-bank'} size={25} color={'white'}></MaterialCommunityIcons>
            <Text style={{color:'white',marginLeft:wp('3%'),fontSize:21,fontFamily:'SF-Pro-Rounded-Semibold',bottom:hp('0.6%')}}>Savings</Text>
        </View>
        <Text style={{color:'white',marginLeft:wp('3%'),fontSize:25,fontWeight:'700',marginTop:hp('2%'),}}>₹ 380.35</Text>
        <Text style={{color:'#E0E0E0',marginLeft:wp('3%'),fontSize:13,fontFamily:'roundedSemi',}}>Current Account</Text>

    </View>
    </TouchableOpacity>
<TouchableOpacity onPress={()=>navigation.navigate('EventList')}>

    <View style={{height:hp('20%'),width:wp('42%'),backgroundColor:'#484848',borderRadius:10,}}>


    <View style={{flexDirection:'row',paddingTop:hp('2%'),marginLeft:wp('3%'),}}>
            <Icon
 name={'calendar'} size={20} color={'white'}></Icon>
            <Text style={{color:'white',marginLeft:wp('3%'),fontSize:21,fontFamily:'SF-Pro-Rounded-Semibold',bottom:hp('0.9%')}}>Events
 </Text>

       
        </View>



        {/* <View style={{flexDirection:'row',paddingTop:hp('4%'),marginLeft:wp('3%')}}> */}
        {/* <Text style={{color:'white',fontSize:33,fontWeight:'bold',bottom:hp('0.6%')}}>3</Text>

            <Text style={{color:'white',marginLeft:wp('3%'),fontSize:13,fontWeight:'700',width:wp('30%'),top:hp('0.6%')}}>Upcomming
events</Text>
       
       
        </View>


        <View style={{flexDirection:'row',paddingTop:hp('2%'),marginLeft:wp('3%')}}>
            <Text style={{color:'white',fontSize:15,fontWeight:'bold',bottom:hp('0.6%')}}> View Events
</Text>
       
       
        </View> */}
        <View style={{flexDirection:'row',paddingTop:hp('2.5%'),marginLeft:wp('3%'),bottom:hp('1.5%')}}>
        <Text style={{color:'white',fontSize:33,fontWeight:'bold',bottom:hp('0.6%')}}>3</Text>
        <Text style={{color:'white',marginLeft:wp('3%'),fontSize:13,fontFamily:'roundedSemi', width:wp('30%'),top:hp('0.2%')}}>Upcomming
events</Text>
       
        </View>
        <View style={{flexDirection:'row'}}>
        <Text style={{color:'#E0E0E0',marginLeft:wp('3%'),fontSize:13,fontFamily:'roundedSemi',bottom:hp('2%')}}>View Events
</Text>
<AntDesign name={'doubleright'} size={12} color={'#E8EB59'} style={{bottom:hp('1%'),left:hp('4%')}}></AntDesign>
</View>

    </View>
    </TouchableOpacity>

</View>





<View style={{ height: hp('6%'), width: wp('30%'), backgroundColor: '#E8EB59', borderRadius: 10, alignItems: 'center', justifyContent: 'center',marginTop:hp('3%'),marginBottom:hp('10%'),alignSelf:'center' }}>
            <Text style={{fontWeight:'900',color:'black'}}>Scan to pay </Text>
          </View>






</ScrollView>


    </View>
  );
}
const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 113,
        height: 30,
        // marginVertical: 32,

    },
    imagelogo: {
        width: 46,
        height: 50,

    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '40%',
        // backgroundColor:'pink'
        // height: 51,
        // backgroundColor: 'red',
        // justifyContent: 'center',
        // alignItems: 'center',
        // borderRadius: 15,
        backgroundColor:'pink'
    },
    submitTitle: {
        // textAlign: 'center',

        // fontFamily: 'SFPRODISPLAYMEDIUM',
        fontSize: 16,
        fontWeight:'900',
        color: '#000000',
        // backgroundColor:'red',
        // height:hp('5%'),
        // alignSelf:'center',justifyContent:'center',
        // alignItems:'center',


    }
});

export default ChildDashBoard;