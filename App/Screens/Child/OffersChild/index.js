import React ,{useState} from 'react';
import { Text, View ,ScrollView,TouchableOpacity,Image} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "../../../utility"
import Ionicons from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
const OffersChild = ({navigation}) => {
    const [activeTab, setActiveTab] = useState('0')

  return (
    <View style={{ flex: 1, backgroundColor:'#060606' }}>
        <ScrollView>
    <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '90%',
                             alignSelf:'center',
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 30,
                             marginTop:hp('5%')
                         }}>Offers</Text>
                             
<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '90%',
                             alignSelf:'center',
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 15,
                             marginTop:hp('2%')
                         }}></Text>
                           <View style={{ width: wp('90%'), height: hp('5%'), backgroundColor: '#313131', alignSelf: 'center', borderRadius: 10, flexDirection: 'row',marginTop:hp('2%') }}>
        <TouchableOpacity onPress={() => setActiveTab('0')}>
          <View style={{ width: wp('45%'), height: hp('5%'), backgroundColor: activeTab == '0' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{color:'white'}}>Offers on Friz-card
</Text>

          </View>
        </TouchableOpacity >
       
        <TouchableOpacity onPress={() => setActiveTab('2')}>

          <View style={{ width: wp('45%'), height: hp('5%'), backgroundColor: activeTab == '2' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{color:'white'}}>Offers on Friz-coins
</Text>

          </View>
        </TouchableOpacity>
      </View>


      <View style={{marginBottom:hp('10%')}}>
<Image source={require('../../../../assets/cardTrend.png')} style={{height:hp('60%'),width:wp('90%'),alignSelf:'center'}} resizeMode={'contain'}>

</Image>

</View>

</ScrollView>

    </View>
  );
}

export default OffersChild;