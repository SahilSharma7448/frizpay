/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     Image, Button, TouchableHighlight, TextInput, TouchableWithoutFeedback, KeyboardAvoidingView,
     Keyboard,
     TouchableOpacity
 } from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "../../../utility"
 
 
 // import {  } from 'react-navigation-stack';
 
 const VisaCard = ({ navigation }) => {
 
 
     return (
         <SafeAreaView style={{ flex: 1 }}>
 
 
             <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                 {/* <KeyboardAvoidingView
 //   keyboardVerticalOffset = { -160}  
   style = {{ flex: 1, backgroundColor:'red' }}
 //   behavior = "position" 
 behavior={Platform.OS == "ios" ? "padding" : "height"}
   > */}
                 <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-evenly' }}>
                     <View style={{ flex: 0.8, backgroundColor: '#060606', justifyContent: 'flex-end' }}>
 
                         <View style={{
                             flex: 0.25, justifyContent: 'space-evenly', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent'
                         }}>
                             <Image source={require('../../../../assets/frizpay-logo.png')} style={styles.imagelogo} />
                             <Image source={require('../../../../assets/Frizpay.png')} style={styles.image} />
                         </View>
 
                         <View style={{
                             flex: 0.25, justifyContent: 'flex-start', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent',
                         }}>
                             <Text style={{
                                 color: '#E0E0E0',
 
                                 width: '90%',
                                 height: '100%',
 
                                 textAlign: 'left',
 
                                 fontFamily: 'SFPRODISPLAYMEDIUM',
                                 fontSize: 24,
                                //  fontWeight:'600'
 
                             }}>{'Congratulations\nYou are now registered \nand your friz-card is ready \nfor use'}</Text>
 
                         </View>
 
 
    
 
 
 
                         <View style={{
                             flex: 0.4, justifyContent: 'flex-start', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent'
                         }}>
                             <Text style={{
                                 color: '#E0E0E0',
 
                                 width: '90%',
 
                                 textAlign: 'left',
 
                                 fontFamily: 'SFProText-Regular',
                                 fontSize: 12
                             }}></Text>
 
                         </View>
 
 
                         <Image source={require('../../../../assets/visaCard.png')} style={{width:wp('55%'),height:hp('45%'),alignSelf:'center',bottom:hp('6%')}} resizeMode={'stretch'} >
 </Image>
 
 
                     </View>
                     <View style={{
                         flex: 0.1, justifyContent: 'center', flexDirection: 'column',
                         alignItems: 'center', backgroundColor: 'transparent'
                     }}>
                         {/* <TouchableHighlight
                             style={styles.doneButton}
                             onPress={() => navigation.navigate('SideDrawerChild')}
                             underlayColor='#E8EB59'>
                             <Text style={styles.submitTitle}>Continue</Text>
                         </TouchableHighlight> */}
 <TouchableOpacity onPress={()=>navigation.navigate('SideDrawerChild')}>
<View style={{ height: hp('6.5%'), width: wp('85%'),alignSelf:'center', backgroundColor: '#E8EB59', borderRadius: 13, alignItems: 'center', justifyContent: 'center' }}>
        
        <Text style={{color:'black',fontFamily: 'SF-Pro-Rounded-Semibold',}}>Continue</Text>
      </View>
          </TouchableOpacity>
                     </View>
                     {/* <View style={{ flex: 0.1}}/> */}
 
                 </View>
                 {/* </KeyboardAvoidingView> */}
             </ScrollView>
 
 
         </SafeAreaView>
 
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
        width: 90,
        height: 20,
        resizeMode:'contain',
        marginTop:hp('1%')
    
    
    },
    imagelogo: {
        width: 45,
        height: 50,
        // marginTop: 32,
        // marginBottom: 20
        marginTop:hp('1%')
    },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '85%',
         height: 51,
         backgroundColor: '#E8EB59',
         justifyContent: 'center',
         alignItems: 'center',
         borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
        //  fontFamily: 'SF-Pro-Rounded-Semibold',
        fontWeight:'800',
         fontSize: 17,
       color:'black'
 
 
     }
 });
 
 export default VisaCard;
 