import React ,{useState} from 'react';
import { Text, View, Image, StyleSheet, TouchableHighlight, ScrollView, TouchableOpacity, ImageBackground, TextInput, Alert } from 'react-native';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "../../../utility"
import { Input, CheckBox, BottomSheet, ListItem } from 'react-native-elements';
import * as Utility from '../../../utility'
import { useDispatch, useSelector } from 'react-redux'
import { Create_Child_Api } from '../../../Action/Auth'
import Loader from '../../../Components/Loader'
const BlueCard = ({ navigation }) => {
    const dispatch = useDispatch()
    const GetMobileNumber = useSelector(state => state.REGISTER_MOBILE);
    console.log('gettt mobile,get', GetMobileNumber)
  const [isVisible, setIsVisible] = useState(false);
    const [selectedImg, setSelectedImg] = useState('')
    const [loading, setLoading] = useState(false)
    const [name, setName] = useState('')
    const [dob, setDob] = useState('')
  const launchCameras = () => {
    let options = {
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    };

    launchCamera(options, (response) => { // Use launchImageLibrary to open image gallery
        console.log('Response = ', response);

        if (response.didCancel) {
            console.log('User cancelled image picker');
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
        } else {
            // const source = { uri: response.uri };
            setSelectedImg(response.assets[0].uri)
            setIsVisible(false)
            // You can also display the image using data:
            // const source = { uri: 'data:image/jpeg;base64,' + response.data };

            // console.log(source)
        }

    })
}



const launchImageLibrarys = () => {
    let options = {
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    };
    launchImageLibrary(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
            console.log('User cancelled image picker');
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
        } else {
            console.log("check response uri", response.uri)
            setSelectedImg(response.assets[0].uri)
            setIsVisible(false)
        }
    });

}

const list = [
    {
        title: 'Choose from Gallery',
        onPress: () => launchImageLibrarys(),
    },
    {
        title: 'Choose from camera',
        onPress: () => launchCameras(),

    },
    {
        title: 'Cancel',
        containerStyle: { backgroundColor: 'red' },
        titleStyle: { color: 'white' },
        onPress: () => setIsVisible(false),
    },
];










    const goToRoute = async () => {
        // 
        if (await Utility.isFieldEmpty(name, dob)) {
            await Alert.alert('Please fill all fields')
        }
        else if (await dob.length < 4) {
            return Alert.alert('Please fill 4 number pin')
        }
        else {
            // navigation.navigate('GaurdianPhone')
            setLoading(true)
            let res = await dispatch(Create_Child_Api(name, dob, GetMobileNumber))
            if (res.id) {
                setLoading(false)
                Alert.alert(
                    '',
                    'Child Create Successfully',
                    [

                        { text: 'OK', onPress: () => navigation.navigate('GaurdianPhone') },
                    ]
                );
            }
            else {
                setLoading(false)
                return Alert.alert(res.message)
            }
        }
    }
  return (
      <View style={{ flex: 1, backgroundColor: '#060606', alignItems: 'center' }}>
          <Loader isLoading={loading}></Loader>
      <ScrollView showsVerticalScrollIndicator={false}>
                          <View style={{alignSelf:'center',alignItems:'center'}}>
                            <Image source={require('../../../../assets/frizpay-logo.png')} style={styles.imagelogo} />
                            <Image source={require('../../../../assets/Frizpay.png')} style={styles.image} />

                       <TouchableOpacity onPress={()=>setIsVisible(true)}>
                          
                                <Image source={selectedImg==''? require('../../../../assets/profile-signup-default.png'):{uri:selectedImg}}
                                    style={{ backgroundColor: 'transparent',marginTop:hp('5%'),borderRadius:selectedImg==''?0: 40 }} width={50} height={50} resizeMode={'cover'} />
</TouchableOpacity>
                        <ImageBackground source={require('../../../../assets/blueCard.png')} style={{width:wp('60%'),height:hp('45%'),alignSelf:'center',marginTop:hp('5%')}} resizeMode={'stretch'} >
                        <Image source={require('../../../../assets/frizpay-logo.png')} style={{height:30,width:30,alignSelf:'flex-end',marginRight:10,marginTop:hp('2%')}} resizeMode={'contain'} />
                      <TextInput style={{ width: wp('30%'), marginLeft: wp('3%'), marginTop: hp('15%'), fontSize: 10, color: 'white' }}
                          onChangeText={(text) => setName(text)}
                          placeholder={'Enter your full name'} placeholderTextColor={'white'}></TextInput>
<View style={{height:hp('0.1%'),width:wp('30%'),marginLeft:wp('3%'),backgroundColor:'white',bottom:hp('2%')}}></View>
<View style={{bottom:hp('2%')}}>
                          <TextInput style={{ width: wp('30%'), marginLeft: wp('3%'), fontSize: 10, color: 'white', }}
                              onChangeText={(text) => setDob(text)}
                              keyboardType="number-pad"
                              placeholder={'Enter Pin'} placeholderTextColor={'white'} maxLength={4} secureTextEntry={true}></TextInput>
<View style={{height:hp('0.1%'),width:wp('30%'),marginLeft:wp('3%'),backgroundColor:'white',bottom:hp('2%')}}></View>
</View>

</ImageBackground>
                  <TouchableOpacity onPress={() => goToRoute()}>
<View style={{ height: hp('6.5%'), width: wp('85%'), marginTop: hp('5%'),alignSelf:'center', backgroundColor: '#E8EB59', borderRadius: 13, alignItems: 'center', justifyContent: 'center' }}>
        
        <Text style={{color:'black',fontFamily: 'SF-Pro-Rounded-Semibold',}}>Continue</Text>
      </View>
          </TouchableOpacity>
</View>
</ScrollView>
<BottomSheet
                isVisible={isVisible}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                {list.map((l, i) => (
                    <ListItem key={i} containerStyle={l.containerStyle} onPress={l.onPress}>
                        <ListItem.Content>
                            <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
                        </ListItem.Content>
                    </ListItem>
                ))}
            </BottomSheet>
    </View>
  );
}

export default BlueCard;

const styles = StyleSheet.create({
 
    image: {
        width: 90,
        height: 20,
        resizeMode:'contain',
        marginTop:hp('4%')


    },
    imagelogo: {
        width: 55,
        height: 60,
        // marginTop: 32,
        // marginBottom: 20
        marginTop:hp('3%')
    },
  
});
