import React  ,{useState} from 'react';
import { Text, View ,Image,TouchableOpacity,ScrollView} from 'react-native';
import { heightPercentageToDP as hp,widthPercentageToDP as wp } from '../../../utility';
import Modal from "react-native-modal";
import { set } from 'react-native-reanimated';
const CoinFund = ({navigation}) => {
    const [investVisible,setInvestVisible]=useState(false)
    const [showInvest,setshowInvest]=useState('show')
  return (
    <View style={{ flex: 1, backgroundColor:'#060606', }}>
        <ScrollView>
        <TouchableOpacity onPress={()=>navigation.goBack()} activeOpacity={1}>
     <Image source={require('../../../../assets/offerHeading.png')} style={{height:hp('12%'),width:wp('95%'),marginTop:hp('5%'),alignSelf:'center',}} resizeMode={'cover'}>

     </Image>
     </TouchableOpacity>


     <View style={{ marginTop: hp('5%'), width: wp('90%'), alignItems: 'center', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
     <TouchableOpacity onPress={()=>navigation.navigate('OffersChild')}>

<View style={{ height: hp('15%'), width: wp('40%'), borderRadius: 10, backgroundColor: '#313131', }}>
    <Text style={{ marginTop: hp('1%'), textAlign: 'center',color:'white',fontWeight:'800' }}>Offers</Text>

    <View style={{ flexDirection: 'row', marginTop: hp('1%'), justifyContent: 'space-around' }}>
<Image source={require('../../../../assets/offersPer.png')} style={{ height: hp('8%'), width: wp('12%'), }} resizeMode={'contain'}></Image>
        <Text style={{ color: 'white', fontSize: 50, bottom: hp('0.7%') }}>₹</Text>

        <Image source={require('../../../../assets/wave.png')} style={{ height: hp('8%'), width: wp('12%'), }} resizeMode={'contain'}></Image>


    </View>
</View>
</TouchableOpacity>




<View style={{ height: hp('15%'), width: wp('40%'), borderRadius: 10, backgroundColor: '#313131', }}>
    <Text style={{ marginTop: hp('1%'), textAlign: 'center',color:'white',fontWeight:'800' }}> Level and Rewards</Text>

    <View style={{ flexDirection: 'row', marginTop: hp('2%'), justifyContent: 'space-around' }}>
        <Image source={require('../../../../assets/privacy.png')} style={{ height: hp('7%'), width: wp('10%'), }} resizeMode={'contain'}></Image>

        <Image source={require('../../../../assets/badge.png')} style={{ height: hp('5.5%'), width: wp('7%'),top:hp('0.7%') }} resizeMode={'contain'}></Image>


    </View>
</View>
</View>
<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '90%',
                             alignSelf:'center',
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 30,
                             marginTop:hp('2%')
                         }}>Coin funds</Text>
<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '90%',
                             alignSelf:'center',
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontSize: 16,
                             marginTop:hp('1%')
                         }}>12 hours funds
                         </Text>
                         





 <View style={{ width: wp('90%'), alignSelf: 'center', marginTop: hp('2%'), flexDirection: 'row', alignItems: 'center', }}>
                <Image source={require('../../../../assets/cardLogo.png')} style={{ height: hp('12%'), width: wp('18%') }} resizeMode={'contain'}>

                </Image>
                <View style={{ width: wp('18%'), borderRightColor: 'grey', borderRightWidth: 1, marginLeft: wp('3%') }}>
                    <Text style={{color:'white',fontWeight:'800',fontSize:12}}>Returns
</Text>
                    <Text style={{ marginTop: hp('1%') ,color:'white',fontWeight:'800',fontSize:15}}>10%</Text>

                </View>
                <View style={{ width: wp('30%'), borderRightColor: 'grey', borderRightWidth: 1, marginLeft: wp('3%') }}>
                    <Text style={{color:'white',fontWeight:'800',fontSize:10}}>Participants getting returns
</Text>
<Text style={{ marginTop: hp('1%') ,color:'white',fontWeight:'800',fontSize:15}}>90%</Text>


                </View>
                <View style={{ width: wp('20%'),  marginLeft: wp('3%') }}>
                    <Text style={{color:'white',fontWeight:'800',fontSize:12}}>Risk profile
</Text>
                    <Text style={{ marginTop: hp('1%'),color:'white',fontWeight:'800',fontSize:15 }}>Low</Text>

                </View>
            </View>
            <TouchableOpacity onPress={()=>setInvestVisible(true)}>
            <View style={{ height: hp('6%'), width: wp('90%'), marginTop: hp('2%'), backgroundColor: '#313131', borderRadius: 10,
            alignSelf:'center',
            alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{fontWeight:'900',color:'white'}}>Invest
</Text>
          </View>
          </TouchableOpacity>




          <View style={{ width: wp('90%'), alignSelf: 'center', marginTop: hp('2%'), flexDirection: 'row', alignItems: 'center', }}>
                <Image source={require('../../../../assets/cardLogo.png')} style={{ height: hp('12%'), width: wp('18%') }} resizeMode={'contain'}>

                </Image>
                <View style={{ width: wp('18%'), borderRightColor: 'grey', borderRightWidth: 1, marginLeft: wp('3%') }}>
                    <Text style={{color:'white',fontWeight:'800',fontSize:12}}>Returns
</Text>
                    <Text style={{ marginTop: hp('1%') ,color:'white',fontWeight:'800',fontSize:15}}>10%</Text>

                </View>
                <View style={{ width: wp('30%'), borderRightColor: 'grey', borderRightWidth: 1, marginLeft: wp('3%') }}>
                    <Text style={{color:'white',fontWeight:'800',fontSize:10}}>Participants getting returns
</Text>
<Text style={{ marginTop: hp('1%') ,color:'white',fontWeight:'800',fontSize:15}}>90%</Text>


                </View>
                <View style={{ width: wp('20%'),  marginLeft: wp('3%') }}>
                    <Text style={{color:'white',fontWeight:'800',fontSize:12}}>Risk profile
</Text>
                    <Text style={{ marginTop: hp('1%'),color:'white',fontWeight:'800',fontSize:15 }}>Low</Text>

                </View>
            </View>
            <TouchableOpacity onPress={()=>setInvestVisible(true)}>
            <View style={{ height: hp('6%'), width: wp('90%'), marginTop: hp('2%'), backgroundColor: '#313131', borderRadius: 10,
            alignSelf:'center',
            alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{fontWeight:'900',color:'white'}}>Invest
</Text>
          </View>
          </TouchableOpacity>
          </ScrollView>

          <Modal isVisible={investVisible}>
        <View style={{ flex: 1,alignItems:'center',alignSelf:'center',justifyContent:'center' }}>
       {showInvest=='show'?
        <View style={{height:hp('50%'),width:wp('90%'),borderColor:'#E8EB59' ,borderWidth:1,borderRadius:10,alignSelf:'center',backgroundColor:'black'}}>
<View style={{flexDirection:'row',alignSelf:'center',justifyContent:'center',marginTop:hp('3%'),alignItems:'center'}}>
    <Text style={{fontSize:28,fontFamily:'SFProText-Semibold', color:'white'}}>2000</Text>
    <Image source={require('../../../../assets/wave2.png')} style={{ height: hp('6%'), width: wp('9%'), marginLeft:10}} resizeMode={'contain'}></Image>

</View>
<View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('80%'), alignSelf: 'center',marginTop:hp('1%') }}></View>



<View style={{ width: wp('90%'), alignSelf: 'center', marginTop: hp('5%'), flexDirection: 'row', alignItems: 'center',justifyContent:'center' }}>
               
                <View style={{ width: wp('20%'),  marginLeft: wp('3%') }}>
                    <Text style={{color:'white',fontWeight:'800',fontSize:12,bottom:hp('1.2%')}}>Returns
</Text>
                    <Text style={{ top: hp('2%') ,color:'white',fontWeight:'800',fontSize:15}}>10%</Text>

                </View>
                <View style={{ width: wp('30%'),  marginLeft: wp('3%') }}>
                    <Text style={{color:'white',fontWeight:'800',fontSize:10}}>Participants getting returns
</Text>
<Text style={{ marginTop: hp('1%') ,top:hp('1%'),color:'white',fontWeight:'800',fontSize:15}}>90%</Text>


                </View>
                <View style={{ width: wp('20%'), marginLeft: wp('3%') }}>
                    <Text style={{color:'white',fontWeight:'800',fontSize:12,bottom:hp('0.8%')}}>Risk profile
</Text>
                    <Text style={{ marginTop: hp('1%'),color:'white',fontWeight:'800',fontSize:15,top:hp('1.5%') }}>Low</Text>

                </View>
            </View>


            <View style={{flexDirection:'row',alignSelf:'center',justifyContent:'space-between',width:wp('80%'), marginTop:hp('3%'),alignItems:'center'}}>
            <Text style={{fontSize:15,color:'#E0E0E0'}}>Expected Returns:
</Text>
  
   <View style={{flexDirection:'row'}}>
    <Text style={{fontSize:25,fontWeight:'bold',color:'white'}}>2000</Text>
    <Image source={require('../../../../assets/wave2.png')} style={{ height: hp('6%'), width: wp('9%'), marginLeft:10}} resizeMode={'contain'}></Image>
    </View>
</View>
<View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('80%'), alignSelf: 'center',marginTop:hp('1%') }}></View>
<TouchableOpacity onPress={()=>setshowInvest('')}>
<View style={{ height: hp('7%'), width: wp('70%'), marginTop: hp('5%'), backgroundColor: '#E8EB59', borderRadius: 10,
            alignSelf:'center',
            alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{fontFamily:'SF-Pro-Rounded-Semibold' ,color:'black'}}>Invest
</Text>
          </View>
          </TouchableOpacity>
        </View>:

<View style={{height:hp('60%'),width:wp('80%'),alignSelf:'center',borderRadius:10,borderWidth:1,borderColor:'#E8EB59',backgroundColor:'#060606'}}>

<TouchableOpacity onPress={()=>setInvestVisible(false)} activeOpacity={1}>
<Image source={require('../../../../assets/investSucessfull.png')} style={{ height: hp('50%'), width: wp('70%'), marginLeft:10,alignSelf:'center'}} resizeMode={'contain'}></Image>
</TouchableOpacity>
</View>
}
        </View>
      </Modal>
    </View>
  );
}

export default React.memo(CoinFund);