import React, { useState } from 'react';
import { Text, View, Image, StyleSheet, TouchableHighlight, ScrollView, TextInput, Alert } from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "../../../utility"
import Icon from "react-native-vector-icons/FontAwesome"
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as   Utility from '../../../utility'
const ParentKYC = ({ navigation }) => {
  const [mobile, setMobile] = useState('')
  const goToRoute = async () => {
    if (await Utility.isFieldEmpty(mobile)) {
      return Alert.alert('Please fill mobile number')
    }
    else if (await mobile.length < 10) {
      return Alert.alert('Please fill valid mobile number')
    }
    else {
      navigation.navigate('VisaCard')
    }

  }
  return (
    <View style={{ flex: 1, backgroundColor:'#060606',alignItems:'center' }}>
      <ScrollView showsVerticalScrollIndicator={false}>
                          <View style={{alignSelf:'center',alignItems:'center'}}>
                            <Image source={require('../../../../assets/frizpay-logo.png')} style={styles.imagelogo} />
                            <Image source={require('../../../../assets/Frizpay.png')} style={styles.image} />

                       
                          
                                <Image source={require('../../../../assets/profile-signup-default.png')}
                                    style={{ backgroundColor: 'transparent',marginTop:hp('5%') }} width={50} height={50} />


          <TextInput style={{ height: hp('7%'), width: wp('80%',), alignItems: 'center', color: 'grey', textAlign: 'center', fontFamily: 'displayB', fontSize: 19, marginTop: hp('5%'), letterSpacing: 2 }}
            onChangeText={(text) => setMobile(text)}
            placeholder={'+91999999999999'}
            placeholderTextColor={'grey'}
            maxLength={10} ></TextInput>
<View style={{width:wp('78%'),height:1,backgroundColor:'grey',alignSelf:'center'}}></View>



<TextInput style={{height:hp('7%'),width:wp('80%',),alignItems:'center',color:'grey',textAlign:'center',fontFamily:'displayB',fontSize:18,marginTop:hp('5%')}} value={'Parent KYC Pending'}></TextInput>
<View style={{width:wp('78%'),height:1,backgroundColor:'grey',alignSelf:'center'}}></View>


<View style={{flexDirection:'row',alignSelf:'center',width:wp('80%'),marginTop:hp('8%'),marginBottom:hp('7%'),alignItems:'center'}}>
<Icon name={'whatsapp'} size={25} color={'green'} style={{top:hp('0.2%')}}></Icon>
<Text style={{color:'white',fontSize:11,marginLeft:wp('2%'),fontFamily:'SFPRODISPLAYMEDIUM'}}>we will send a invite link on there whatsapp number. </Text>
</View>

          <TouchableOpacity onPress={() => goToRoute()}>
<View style={{ height: hp('6.5%'), width: wp('85%'), marginTop: hp('5%'),alignSelf:'center', backgroundColor: '#E8EB59', borderRadius: 13, alignItems: 'center', justifyContent: 'center' }}>
        
        <Text style={{color:'black',fontFamily: 'SF-Pro-Rounded-Semibold',}}>Send Invite</Text>
      </View>
          </TouchableOpacity>
</View>
</ScrollView>
    </View>
  );
}

export default ParentKYC;

const styles = StyleSheet.create({
 
  
  image: {
    width: 90,
    height: 20,
    resizeMode:'contain',
    marginTop:hp('4%')


},
imagelogo: {
    width: 55,
    height: 60,
    // marginTop: 32,
    // marginBottom: 20
    marginTop:hp('3%')
},
  
});
