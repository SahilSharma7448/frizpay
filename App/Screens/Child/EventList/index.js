import React ,{useState} from 'react';
import { Text, View,TouchableOpacity,Image,FlatList } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "../../../utility"

const EventList = ({navigation}) => {
    const [activeTab, setActiveTab] = useState('0')

  return (
    <View style={{ flex: 1, backgroundColor:'#060606', }}>
      <ScrollView>
      <View style={{width:wp('95%'),alignSelf:'center',marginTop:hp('5%'),flexDirection:'row'}}>
<TouchableOpacity
 
                             onPress={() => {
                                
 
                                 navigation.goBack();
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('../../../../assets/back-icon.png')} width={22} height={20} style={{top:8}} />
 
 
                         </TouchableOpacity>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
 
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                            fontWeight:'800',
                             fontSize: 32
                         }}>Events</Text>
</View>




<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '90%',
                             alignSelf:'center',
                             fontFamily: 'proTxt',
                        
                             fontSize: 17,
                             marginTop:hp('2%')
                         }}>Total Money in Events
                         </Text>

                         <View style={{width:wp('70%'),flexDirection:"row",justifyContent:'space-between',marginTop:hp('2%'),marginLeft:hp('3%')}}>
    <Text style={{color:'white',fontWeight:'900',fontSize:30,marginTop:hp('1%')}}>₹ 750 
</Text>
<TouchableOpacity onPress={()=>navigation.navigate('SelectedEvent')}>
    <View style={{ height: hp('6%'), width: wp('35%'), marginRight:wp('5%'), backgroundColor: '#E8EB59', borderRadius: 12, alignItems: 'center', justifyContent: 'center',marginTop:hp('1%') }}>
            <Text style={{fontFamily:'SF-Pro-Rounded-Semibold', color:'black',fontSize:13}}>Create a Event </Text>
          </View>
</TouchableOpacity>
                         </View>





                         <View style={{ width: wp('90%'), height: hp('5%'), backgroundColor: '#313131', alignSelf: 'center', borderRadius: 10, flexDirection: 'row',marginTop:hp('5%') }}>
        <TouchableOpacity onPress={() => setActiveTab('0')}>
          <View style={{ width: wp('45%'), height: hp('5%'), backgroundColor: activeTab == '0' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{color:'white',fontFamily:'SF-Pro-Rounded-Semibold'}}>Upcomming Events</Text>

          </View>
        </TouchableOpacity >
       
        <TouchableOpacity onPress={() => setActiveTab('2')}>

          <View style={{ width: wp('45%'), height: hp('5%'), backgroundColor: activeTab == '2' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{color:'white',fontFamily:'SF-Pro-Rounded-Semibold'}}>Old Events (4)</Text>

          </View>
        </TouchableOpacity>
      </View>






<TouchableOpacity onPress={()=>navigation.navigate('SelectedEvent')}>
      <View style={{height:hp('25%'),
   
   borderRadius:16,
   width:wp('90%'),alignSelf:'center',backgroundColor:'#484848',marginTop:hp('5%')}}>
       <View style={{

flexDirection:'row',
justifyContent:'space-between',
       }}>
<View style={{marginLeft:wp('3%')}}>
   
    <Text style={{color:'white',fontFamily:'SF-Pro-Rounded-Semibold', fontSize:16,marginTop:hp('0.5%')}}>Shubham,s Birthday</Text>

</View>

            <Text style={{color:'#BABABA',marginTop:hp('1.2%'),marginRight:wp('3%'),fontSize:11,fontFamily:'SF-Pro-Rounded-Semibold',}}>Event Date: 01 Jul </Text>
            </View>

          
            <View style={{width:wp('80%'),flexDirection:'row',alignSelf:'center',marginTop:hp('1%'),marginBottom:hp('1%')}}>
<View>
<Image source={require('../../../../assets/ridhima.png')} style={{height:35,width:35,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 11, marginTop: hp('1%'),color:'#E0E0E0',fontWeight:'700' }}>Ridhima</Text>
          </View>
          <View style={{marginLeft:wp('7%')}}>
<Image source={require('../../../../assets/ridhima.png')} style={{height:35,width:35,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 11, marginTop: hp('1%'),color:'#E0E0E0',fontWeight:'700' }}>Ridhima</Text>
          </View>


          <View style={{marginLeft:wp('7%')}}>
<Image source={require('../../../../assets/ridhima.png')} style={{height:35,width:35,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 11, marginTop: hp('1%'),color:'#E0E0E0',fontWeight:'700' }}>Ridhima</Text>
          </View>
          <View style={{marginLeft:wp('7%')}}>
<Image source={require('../../../../assets/2.png')} style={{height:40,width:40,borderRadius:50}} resizeMode="contain" />
          </View>
              </View>


              <View style={{marginLeft:wp('3%')}}>
   
   <Text style={{color:'white',fontWeight:'900',fontSize:18,marginTop:hp('1%')}}>₹ 2000

  





</Text>

</View>
<View style={{

flexDirection:'row',
justifyContent:'space-between',
       }}>

<View style={{marginLeft:wp('3%'),marginBottom:hp('2%')}}>
   
<Text style={{color:'#BABABA',marginTop:hp('0.5%'),marginRight:wp('3%'),fontSize:11,fontFamily:'SF-Pro-Rounded-Semibold',}}>Total Amount Gathered </Text>

</View>

            <Text style={{color:'#BABABA',marginTop:hp('0.5%'),marginRight:wp('3%'),fontSize:11,fontFamily:'SF-Pro-Rounded-Semibold',}}>Event Created: 01 Jul </Text>
            </View>
   </View>






   </TouchableOpacity>







   <TouchableOpacity onPress={()=>navigation.navigate('SelectedEvent')}>
      <View style={{height:hp('25%'),
   
   borderRadius:16,
   width:wp('90%'),alignSelf:'center',backgroundColor:'#484848',marginTop:hp('5%')}}>
       <View style={{

flexDirection:'row',
justifyContent:'space-between',
       }}>
<View style={{marginLeft:wp('3%')}}>
   
    <Text style={{color:'white',fontFamily:'SF-Pro-Rounded-Semibold', fontSize:16,marginTop:hp('0.5%')}}>Shubham,s Birthday</Text>

</View>

            <Text style={{color:'#BABABA',marginTop:hp('1.2%'),marginRight:wp('3%'),fontSize:11,fontFamily:'SF-Pro-Rounded-Semibold',}}>Event Date: 01 Jul </Text>
            </View>

          
            <View style={{width:wp('80%'),flexDirection:'row',alignSelf:'center',marginTop:hp('1%'),marginBottom:hp('1%')}}>
<View>
<Image source={require('../../../../assets/ridhima.png')} style={{height:35,width:35,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 11, marginTop: hp('1%'),color:'#E0E0E0',fontWeight:'700' }}>Ridhima</Text>
          </View>
          <View style={{marginLeft:wp('7%')}}>
<Image source={require('../../../../assets/ridhima.png')} style={{height:35,width:35,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 11, marginTop: hp('1%'),color:'#E0E0E0',fontWeight:'700' }}>Ridhima</Text>
          </View>


          <View style={{marginLeft:wp('7%')}}>
<Image source={require('../../../../assets/ridhima.png')} style={{height:35,width:35,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 11, marginTop: hp('1%'),color:'#E0E0E0',fontWeight:'700' }}>Ridhima</Text>
          </View>
          <View style={{marginLeft:wp('7%')}}>
<Image source={require('../../../../assets/2.png')} style={{height:40,width:40,borderRadius:50}} resizeMode="contain" />
          </View>
              </View>


              <View style={{marginLeft:wp('3%')}}>
   
   <Text style={{color:'white',fontWeight:'900',fontSize:18,marginTop:hp('1%')}}>₹ 2000

  





</Text>

</View>
<View style={{

flexDirection:'row',
justifyContent:'space-between',
       }}>

<View style={{marginLeft:wp('3%'),marginBottom:hp('2%')}}>
   
<Text style={{color:'#BABABA',marginTop:hp('0.5%'),marginRight:wp('3%'),fontSize:11,fontFamily:'SF-Pro-Rounded-Semibold',}}>Total Amount Gathered </Text>

</View>

            <Text style={{color:'#BABABA',marginTop:hp('0.5%'),marginRight:wp('3%'),fontSize:11,fontFamily:'SF-Pro-Rounded-Semibold',}}>Event Created: 01 Jul </Text>
            </View>
   </View>






   </TouchableOpacity>










   </ScrollView>
    </View>
  );
}

export default EventList;