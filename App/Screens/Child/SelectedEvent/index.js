import React ,{useState} from 'react';
import { Text, View,TouchableOpacity,Image,FlatList } from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "../../../utility"
import Icon from 'react-native-vector-icons/FontAwesome'
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { Input ,Slider} from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
const SelectedEvent = ({navigation}) => {
    const [activeTab, setActiveTab] = useState('0')

  return (
    <View style={{ flex: 1, backgroundColor:'#060606', }}>
      <ScrollView>
 <View style={{width:wp('95%'),alignSelf:'center',marginTop:hp('5%'),flexDirection:'row'}}>
<TouchableOpacity
 
                             onPress={() => {
                                
 
                                 navigation.goBack();
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('../../../../assets/back-icon.png')} width={22} height={20} style={{top:5}} />
 
 
                         </TouchableOpacity>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
 
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                            fontWeight:'800',
                             fontSize: 32
                         }}>Events</Text>
</View>




<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '90%',
                             alignSelf:'center',
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'700',
                             fontSize: 15,
                             marginTop:hp('2%')
                         }}>Event Participants

                         </Text>

                         
                         <View style={{width:wp('80%'),flexDirection:'row',alignSelf:'center',marginTop:hp('3%'),marginBottom:hp('1%')}}>
<View>
<Image source={require('../../../../assets/ridhima.png')} style={{height:45,width:45,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 12, marginTop: hp('1%'),color:'#E0E0E0',fontWeight:'700' }}>Ridhima</Text>
          </View>
          <View style={{marginLeft:wp('7%')}}>
<Image source={require('../../../../assets/ridhima.png')} style={{height:45,width:45,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 12, marginTop: hp('1%'),color:'#E0E0E0',fontWeight:'700' }}>Ridhima</Text>
          </View>


          <View style={{marginLeft:wp('10%'),marginTop:hp('1.5%')}}>
<AntDesign name={'pluscircle'}  size={30} color={'#E8EB59'} ></AntDesign>
          </View>
              </View>




<View style={{width:wp('90%'),justifyContent:'space-between',alignSelf:'center',flexDirection:'row'}}>
    <View>
    <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             alignSelf:'center',
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontFamily:'SF-Pro-Rounded-Semibold',
                             fontSize: 17,
                             marginTop:hp('3%')
                         }}>Total Amount 


                         </Text>
                        <View style={{flexDirection:'row',left:wp('2%')}}>
                        <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             alignSelf:'center',
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'700',
                             fontSize: 30,
                             marginTop:hp('3%'),
                            
                         }}>₹


                         </Text>
                         <Input containerStyle={{height:hp('3%'),width:wp('30%')}} leftIcon={<Text style={{color:'white',fontSize:20}}></Text>}></Input>
                         </View>

    </View>



    <View>
    <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontSize: 17,
                             marginTop:hp('3%'),
                             fontFamily:'SF-Pro-Rounded-Semibold'
                         }}>Set Date

                         </Text>
                         <Input containerStyle={{height:hp('3%'),width:wp('30%'),right:10}} leftIcon={<Text style={{color:'white',fontSize:20}}></Text>}></Input>

    </View>
</View>
<View style={{width:wp('90%'),flexDirection:"row",justifyContent:'space-between',marginTop:hp('2%'),marginLeft:hp('3%'),alignSelf:'center',alignItems:'center'}}>
    <Text style={{color:'white', fontFamily:'SFPRODISPLAYBOLD',fontSize:15,marginTop:hp('1%')}}>Event Individual Share 

</Text>
    <View style={{ height: hp('5%'), width: wp('35%'), marginRight:wp('5%'), backgroundColor: '#E8EB59', borderRadius: 16, alignItems: 'center', justifyContent: 'center',marginTop:hp('1%') }}>
            <Text style={{color:'black', fontFamily:'SF-Pro-Rounded-Semibold'}}>Divide Equally 
 </Text>
          </View>

                         </View>













                         <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: wp('90%'),marginTop:hp('3%') }}>

{/* <View style={{height:hp('6%'),borderRadius:40,backgroundColor:'#4B4B4B',width:wp('12%'),alignItems:'center',justifyContent:'center'}}>
<Text style={{ fontSize: 15, color: 'white', }}>SS</Text>

</View> */}
<Image source={require('../../../../assets/shubham.png')} style={{height:45,width:45,borderRadius:50}} resizeMode="contain" />

<View style={{ width: wp('35%'), }}>
  <Text style={{ fontSize: 12, color: 'white',fontFamily:'SFProText-Semibold' }}>Chakshu pay for
</Text>
  <Text style={{ fontSize: 17, color: 'white', marginTop: hp('2%'),fontFamily:'SFProText-Semibold' }}>0% 
</Text>

</View>
{/* <Text style={{ fontSize: 17, color: 'white', textAlign: 'center', }}>23 oct</Text> */}
<View style={{ width: wp('35%'),alignItems:'flex-end' }}>
  <Text style={{ fontSize: 19, color: 'white',top:hp('1%'),fontFamily:'SFProText-Semibold' }}>₹ 0 </Text>
  <Text style={{ fontSize: 12, color: 'white', marginTop: hp('2%') }}></Text>

</View>



</View>
<View style={{flexDirection:'row',width:wp('90%'),alignSelf:'center',alignItems:'center',justifyContent:'space-between',marginTop:hp('1%')}}>
<Slider
    value={0}
    // onValueChange={(value) => this.setState({ value })}
    thumbStyle={{backgroundColor:'#E8EB59',height:25,width:15, borderRadius:20}}
    trackStyle={{backgroundColor:'red'}}
    style={{width:wp('80%'),alignSelf:'center',top:hp('0.5%')}}
  />
  <MaterialIcons name={'lock'} size={25} color={'grey'}></MaterialIcons>
  </View>
<View style={{ borderWidth: 0.5, borderColor: 'grey', marginTop: hp('3%'),width:wp('90%'),alignSelf:'center' }}></View>
















<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: wp('90%'),marginTop:hp('3%') }}>

{/* <View style={{height:hp('6%'),borderRadius:40,backgroundColor:'#4B4B4B',width:wp('12%'),alignItems:'center',justifyContent:'center'}}>
<Text style={{ fontSize: 15, color: 'white', }}>SS</Text>

</View> */}
<Image source={require('../../../../assets/ridhima.png')} style={{height:45,width:45,borderRadius:50}} resizeMode="contain" />

<View style={{ width: wp('35%'), }}>
  <Text style={{ fontSize: 12, color: 'white',fontFamily:'SFProText-Semibold' }}>Ridhima pay for

</Text>
  <Text style={{ fontSize: 19, color: 'white', marginTop: hp('2%'),fontFamily:'SFProText-Semibold' }}>0% 
</Text>

</View>
{/* <Text style={{ fontSize: 17, color: 'white', textAlign: 'center', }}>23 oct</Text> */}
<View style={{ width: wp('35%'),alignItems:'flex-end' }}>
  <Text style={{ fontSize: 17, color: 'white',top:hp('1%'),fontFamily:'SFProText-Semibold' }}>₹ 0 </Text>
  <Text style={{ fontSize: 12, color: 'white', marginTop: hp('2%') }}></Text>

</View>



</View>
<View style={{flexDirection:'row',width:wp('90%'),alignSelf:'center',alignItems:'center',justifyContent:'space-between',marginTop:hp('1%')}}>
<Slider
    value={0}
    // onValueChange={(value) => this.setState({ value })}
    thumbStyle={{backgroundColor:'#E8EB59',height:25,width:15, borderRadius:20}}
    trackStyle={{backgroundColor:'red'}}
    style={{width:wp('80%'),alignSelf:'center',top:hp('0.5%')}}
  />
  <MaterialIcons name={'lock'} size={25} color={'grey'}></MaterialIcons>
  </View>


<View style={{ height: hp('6%'), width: wp('30%'), backgroundColor: '#E8EB59', borderRadius: 10, alignItems: 'center', justifyContent: 'center',marginTop:hp('5%'),marginBottom:hp('10%'),alignSelf:'center' }}>
            <Text style={{fontWeight:'900',color:'black'}}>Pay the Bill </Text>
          </View>
          </ScrollView>
    </View>
  );
}

export default SelectedEvent;