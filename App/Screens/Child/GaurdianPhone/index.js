/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
    TouchableOpacity,
    Alert,
     Image, Button, TouchableHighlight, TextInput, TouchableWithoutFeedback, KeyboardAvoidingView,
     Keyboard
 } from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "../../../utility"
 
import * as Utility from '../../../utility'
 // import {  } from 'react-navigation-stack';
 
 const GaurdianPhone = ({ navigation }) => {
     const [mobile, setMobile] = useState('')
     const goToRoute = async () => {
         if (await Utility.isFieldEmpty(mobile)) {
             return Alert.alert('Please fill mobile Number')
         }
         else if (await mobile.length < 10) {
             return Alert.alert('Please fill valid mobile Number')

         }
         else {
             navigation.navigate('ParentKYC')
         }
     }
     return (
         <SafeAreaView style={{ flex: 1 }}>
 
 
             <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                 {/* <KeyboardAvoidingView
 //   keyboardVerticalOffset = { -160}  
   style = {{ flex: 1, backgroundColor:'red' }}
 //   behavior = "position" 
 behavior={Platform.OS == "ios" ? "padding" : "height"}
   > */}
                 <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-evenly' }}>
                     <View style={{ flex: 0.8, backgroundColor: '#060606', justifyContent: 'flex-end' }}>
 
                         <View style={{
                             flex: 0.25, justifyContent: 'space-evenly', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent'
                         }}>
                             <Image source={require('../../../../assets/frizpay-logo.png')} style={styles.imagelogo} />
                             <Image source={require('../../../../assets/Frizpay.png')} style={styles.image} />
                         </View>
 
                         <View style={{
                             flex: 0.25, justifyContent: 'flex-start', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent',
                         }}>
                             <Text style={{
                                 color: '#E0E0E0',
 
                                 width: '90%',
                                 height: '100%',
 
                                 textAlign: 'left',
 
                                 fontFamily: 'SFPRODISPLAYMEDIUM',
                                 fontSize: 28,
 
                             }}>{'Enter the \nGaurdian'}s {'phone \nnumber'}</Text>
 
                         </View>
 
 
    
 
                         <View style={{
                             flex: 0.2, justifyContent: 'center', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent', alignContent: 'space-between', alignSelf: 'auto',
                             bottom:30
                         }}>
 
                             <View style={{
                                 flex: 1, justifyContent: 'center', flexDirection: 'row',
                                 alignItems: 'center', backgroundColor: 'transparent', alignContent: 'space-between', alignSelf: 'auto'
                             }}>
                                 <Text style={{
                                     color: '#E0E0E0',
                                     textAlign: 'left',
 
                                     fontFamily: 'SFProText-Regular',
                                     fontSize: 30,
                                     marginBottom: -8
                                 }}>+91</Text>
 
                                 <TextInput
                                     style={{
                                         color: 'white', fontSize: 30, backgroundColor: 'transparent',
                                         fontFamily: 'SFProText-Regular',
                                         marginBottom: 0,
                                         paddingBottom: 0,
                                         width: wp('45%')
                                     }}
                                     onChangeText={(text) => setMobile(text)}

                                     placeholder="99999999"
                                     keyboardType="numeric"
                                     maxLength={10}
                                 />
                             </View>
 
 
 
 
                             <View style={{
                                 flex: 0.2,
                                 width: '95%', height: 1, backgroundColor: 'transparent',
                                 alignItems: 'center'
                             }}>
                                 <View style={{ width: '95%', height: 1, backgroundColor: '#B4B4B4' }}></View>
                             </View>
 
 
 
                         </View>
 
 
 
                         <View style={{
                             flex: 0.4, justifyContent: 'flex-start', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent'
                         }}>
                             <Text style={{
                                 color: '#E0E0E0',
 
                                 width: '90%',
 
                                 textAlign: 'left',
 
                                 fontFamily: 'SFProText-Regular',
                                 fontSize: 12
                             }}></Text>
 
                         </View>
 
 
 
 
 
                     </View>
 <Text style={{color:'white',textAlign:'center',fontSize:12,bottom:10,fontFamily:'SFProText-Heavy'}}>Resend OTP</Text>
                     <View style={{
                         flex: 0.1, justifyContent: 'center', flexDirection: 'column',
                         alignItems: 'center', backgroundColor: 'transparent'
                     }}>
                         {/* <TouchableHighlight
                             style={styles.doneButton}
                             onPress={() => navigation.navigate('ParentKYC')}
                             underlayColor='#E8EB59'>
                             <Text style={styles.submitTitle}>Send invite</Text>
                         </TouchableHighlight> */}

                         <TouchableOpacity onPress={() => goToRoute()}>
  <View style={{ height: hp('6.5%'), width: wp('85%'), marginTop: hp('2%'),alignSelf:'center', backgroundColor: '#E8EB59', borderRadius: 13, alignItems: 'center', justifyContent: 'center' }}>
        
        <Text style={{color:'black',fontFamily: 'SF-Pro-Rounded-Semibold',}}>Send invite</Text>
      </View>
      </TouchableOpacity>
 
                     </View>
                     {/* <View style={{ flex: 0.1}}/> */}
 
                 </View>
                 {/* </KeyboardAvoidingView> */}
             </ScrollView>
 
 
         </SafeAreaView>
 
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
        width: 90,
        height: 20,
        resizeMode:'contain',


    },
    imagelogo: {
        width: 35,
        height: 40,
        // marginTop: 32,
        // marginBottom: 20
    },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '85%',
         height: 51,
         backgroundColor: '#E8EB59',
         justifyContent: 'center',
         alignItems: 'center',
         borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
        //  fontFamily: 'SF-Pro-Rounded-Semibold',
        fontWeight:'800',
         fontSize: 17,
       color:'black'
 
 
     }
 });
 
 export default GaurdianPhone;
 