/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TextInput,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/Ionicons';

 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "../../../utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
 
 const ChildAddMoney = ({ navigation }) => {
    const [activeTab, setActiveTab] = useState('0')

 
     return (
         <SafeAreaView style={{ flex: 1,backgroundColor:'#060606' }}>
             <View style={{width:wp('95%'),alignSelf:'center',marginTop:hp('5%'),flexDirection:'row'}}>
<TouchableOpacity
 
                             onPress={() => {
                                
 
                                 navigation.goBack();
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('../../../../assets/back-icon.png')} width={22} height={20} style={{top:12}} />
 
 
                         </TouchableOpacity>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
 
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                            fontFamily:'SFPRODISPLAYBOLD',
                            
                             fontSize: 32
                         }}>Add money</Text>
</View>
                             
 <View style={{flexDirection:'row',width:wp('90%'),justifyContent:'space-between',alignSelf:'center'}}>
                    <View style={{width:wp('60%')}}>
                        
                          <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             alignSelf:'center',
                             fontSize: 12,
                             fontFamily:'SF-Pro-Rounded-Semibold',
                             right:wp('2.5%'),
                         }}>Ridhima’s Available Balance:   ₹ 1500
                         </Text>
</View>

</View>
<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             fontFamily:'SF-Pro-Rounded-Semibold',



                             fontSize: 15,
                             width:wp('90%'),
                             marginTop:hp('6%'),
                             alignSelf:'center'
                         }}>Add money</Text>
<TextInput style={{  color: 'grey', alignSelf: 'center', width: wp('90%'), right: 5,fontSize:25,fontFamily:'displayB' }} placeholder={'₹ Enter amount'} placeholderTextColor={'grey'} keyboardType={'number-pad'}></TextInput>
<View style={{ borderBottomColor: 'grey', borderBottomWidth: 1, width: wp('90%'), alignSelf: 'center', }}></View>



<View style={{flexDirection:'row',width:wp('90%'),alignSelf:'center'}}>



<TextInput style={{  color: 'grey', alignSelf: 'center', width: wp('90%'), right: 5,fontSize:25,marginTop:hp('10%') }} placeholder={'its for-'} placeholderTextColor={'grey'} keyboardType={'number-pad'}></TextInput>
<Ionicons name={'caret-down-outline'} size={17} color={'white'} style={{marginTop:hp('13%'),right:wp('7%')}}></Ionicons>
</View>
<View style={{ borderBottomColor: 'grey', borderBottomWidth: 1, width: wp('90%'), alignSelf: 'center', }}></View>

<TouchableOpacity onPress={()=>navigation.navigate('Payment')}>
<View style={{ height: hp('6%'), width: wp('80%'),
alignSelf:'center',
marginTop: hp('20%'), backgroundColor: '#434343', borderRadius: 10, alignItems: 'center', justifyContent: 'center',marginBottom:hp('1%') }}>
            <Text style={{fontWeight:'900',color:'white',}}>Requesting from Dad
</Text>
          </View>
          </TouchableOpacity>


<TouchableOpacity onPress={()=>navigation.navigate('Payment')}>
<View style={{ height: hp('7%'), width: wp('90%'),
alignSelf:'center',

marginTop: hp('5%'), backgroundColor: '#E8EB59', borderRadius: 10, alignItems: 'center', justifyContent: 'center',marginBottom:hp('10%') }}>
            <Text style={{fontWeight:'900',color:'black',}}>Add Money</Text>
          </View>
          </TouchableOpacity>
               
         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default ChildAddMoney;
 