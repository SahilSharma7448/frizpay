import React ,{useState} from 'react';
import { Text, View ,ScrollView,TouchableOpacity,Image} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "../../../utility"
import Ionicons from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
const Account = ({navigation}) => {
    const [activeTab, setActiveTab] = useState('0')
const [selectedMonth,setSelectedMonth]=useState('This month')
const [showVisible,setVisible]=useState(false)
const clickMonth=(value)=>{
  setSelectedMonth(value)
  setVisible(false)
}
  return (
    <View style={{ flex: 1, backgroundColor:'#060606' }}>
        <ScrollView>
        <View style={{width:wp('95%'),alignSelf:'center',marginTop:hp('5%'),flexDirection:'row'}}>
<TouchableOpacity
 
                             onPress={() => {
                                
 
                                 navigation.goBack();
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('../../../../assets/back-icon.png')} width={22} height={20} style={{top:7}} />
 
 
                         </TouchableOpacity>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
 
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                            fontWeight:'800',
                             fontSize: 32
                         }}>Account</Text>
</View>
                             
                             <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '90%',
                             alignSelf:'center',
                             fontFamily: 'SF-Pro-Rounded-Semibold',
                             fontSize: 14,
                             marginTop:hp('2%')
                         }}>Total Spending Balance  </Text>

<View style={{height:hp('10%'),
   flexDirection:'row',
   justifyContent:'space-between',
   borderRadius:8,
   width:wp('90%'),alignSelf:'center',alignItems:'center',}}>
    <Text style={{color:'white',fontWeight:'900',fontSize:29,bottom:5}}>₹ 380.35</Text>

<TouchableOpacity onPress={()=>navigation.navigate('ChildAddMoney')}>
<View style={{ height: hp('7%'), width: wp('40%'), marginRight:wp('2%'), backgroundColor: '#E8EB59', borderRadius: 10, alignItems: 'center', justifyContent: 'center',bottom:5 }}>
            <Text style={{fontWeight:'900',color:'black',left:3}}>Add Money </Text>
          </View>
   </TouchableOpacity>

   </View>





<View style={{bottom:hp('2%')}}>
   <View style={{ borderWidth: 0.3, borderColor: '#464646', width: wp('90%'), alignSelf: 'center',marginTop:hp('2%') }}></View>











   <View style={{ flexDirection: 'row', justifyContent: 'space-between',  alignSelf: 'center', width: wp('90%'), marginTop: hp('1.5%') }}>
        <View style={{ height: hp('10%'), width: wp('40%'), alignSelf: 'center',  }}>
          <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center', }}>Savings Balance</Text>
          <Text style={{ fontSize: 22, color: 'white', textAlign: 'center',fontWeight:'700',marginTop:hp('2%') }}>₹ 1500</Text>
          {/* <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center',marginTop:hp('3%') }}> Saved by Offers</Text>
          <Text style={{ fontSize: 22, color: 'white', textAlign: 'center',fontWeight:'700' }}>₹ 400</Text> */}


        </View>
        <View style={{ height: hp('5.5%'), width: wp('0.3%'),backgroundColor:'#595959',top:hp('1%') }}></View>
        <View style={{ height: hp('10%'), width: wp('40%'),  alignSelf: 'center', }}>
        <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center', }}>Total Account Balance</Text>
          <Text style={{ fontSize: 22, color: 'white', textAlign: 'center',fontWeight:'700',marginTop:hp('2%') }}>₹ 2000</Text>
          {/* <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center',marginTop:hp('3%') }}> Open Tasks</Text>
          <Text style={{ fontSize: 22, color: 'white', textAlign: 'center',fontWeight:'700' }}>₹ 400</Text> */}


        </View>


      </View>



   <View style={{ borderWidth: 0.3, borderColor: '#464646', width: wp('90%'), alignSelf: 'center',marginTop:hp('0.5%') }}></View>
   </View>
   
   <View style={{ flexDirection: 'row', backgroundColor: '#484848', borderRadius: 12, width: wp('40%'), alignSelf: 'center', justifyContent: 'center' }}>
       <View>
        <Text style={{ fontSize: 15, color: '#F8F8F8',fontFamily:'SF-Pro-Rounded-Semibold', textAlign: 'center',padding:hp('0.4%')}}>{selectedMonth}</Text>
    {showVisible==true?
       <View>
        <Text style={{ fontSize: 15, color: '#F8F8F8', textAlign: 'center', padding: hp('1.2%'),fontFamily:'SF-Pro-Rounded-Semibold',}} onPress={()=>clickMonth('This week')}>This week</Text>
        <Text style={{ fontSize: 15, color: 'white', textAlign: 'center', padding: hp('1.2%'),fontFamily:'SF-Pro-Rounded-Semibold',}} onPress={()=>clickMonth('Last month')}>Last month</Text>
        <Text style={{ fontSize: 15, color: 'white', textAlign: 'center', padding: hp('1.2%'),fontFamily:'SF-Pro-Rounded-Semibold',}} onPress={()=>clickMonth('Last week')}>Last week</Text>
      
        </View>:null}
        </View>
      
      
      <TouchableOpacity onPress={()=>setVisible(!showVisible)}>
      
        <Ionicons name={showVisible==false? 'caret-down':'caret-up'} size={20} color={'#F8F8F8'} style={{left:wp('2%'),top:hp('1%')}}></Ionicons>
        </TouchableOpacity>
      </View>

      <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '90%',
                             alignSelf:'center',
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 14,
                             marginTop:hp('2%')
                         }}>Total Recieved</Text>


<View style={{width:wp('90%'),alignSelf:'center',marginTop:hp('2%'),flexDirection:'row',justifyContent:'space-between'}}>
<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '85%',
                             alignSelf:'center',
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 22,
                         }}>₹ 4000</Text>
                       <Image source={require('../../../../assets/upArrow.png')} style={{height:27,width:27,marginRight:wp('5%')}}></Image>
                         {/* <MaterialCommunityIcons name={'archive-arrow-down-outline'} size={30} color={'#E8EB59'}></MaterialCommunityIcons> */}
</View>

<View style={{ borderWidth: 0.3, borderColor: '#464646', width: wp('90%'), alignSelf: 'center',marginTop:hp('2%') }}></View>

<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '90%',
                             alignSelf:'center',
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 14,
                             marginTop:hp('2%')
                         }}>Total Spent</Text>


<View style={{width:wp('90%'),alignSelf:'center',marginTop:hp('2%'),flexDirection:'row',justifyContent:'space-between'}}>
<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '85%',
                             alignSelf:'center',
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontWeight:'bold',
                             fontSize: 22,
                         }}>₹ 3000</Text>
                         {/* <MaterialCommunityIcons name={'archive-arrow-up-outline'} size={30} color={'#E8EB59'}></MaterialCommunityIcons> */}
                         <Image source={require('../../../../assets/downArrow.png')} style={{height:27,width:27,marginRight:wp('5%')}}></Image>

</View>

<View style={{ borderWidth: 0.3, borderColor: '#464646', width: wp('90%'), alignSelf: 'center',marginTop:hp('2%') }}></View>


<Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
                             width: '90%',
                             alignSelf:'center',
                             fontFamily: 'roundedBold',
                             fontWeight:'bold',
                             fontSize: 15,
                             marginTop:hp('2%')
                         }}>Recent Transactions</Text>
                           <View style={{ width: wp('80%'), height: hp('5%'), backgroundColor: '#313131', alignSelf: 'center', borderRadius: 10, flexDirection: 'row',marginTop:hp('2%') }}>
        <TouchableOpacity onPress={() => setActiveTab('0')}>
          <View style={{ width: wp('25%'), height: hp('5%'), backgroundColor: activeTab == '0' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 9,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12.35,
        
        elevation:activeTab=='0'? 19:0,
        }}>
          <Text style={{color:'white',fontFamily:'roundedBold',top:hp('0.1%'),fontSize:16}}>All</Text>

          </View>
        </TouchableOpacity >
        <TouchableOpacity onPress={() => setActiveTab('1')}>
          <View style={{ width: wp('25%'), height: hp('5%'), backgroundColor: activeTab == '1' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 9,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12.35,
        
        elevation:activeTab=='1'? 19:0,
        }}>
            <Text style={{color:'white',fontFamily:'roundedBold',top:hp('0.1%'),fontSize:16}}>Recieved</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setActiveTab('2')}>

          <View style={{ 
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 9,
            },
            shadowOpacity: 0.30,
            shadowRadius: 12.35,
            
            elevation:activeTab=='2'? 19:0,
            width: wp('25%'), height: hp('5%'), backgroundColor: activeTab == '2' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{color:'white',fontFamily:'roundedBold',top:hp('0.1%'),fontSize:16}}>Spent</Text>

          </View>
        </TouchableOpacity>
      </View>


      <View style={{marginBottom:hp('10%')}}>


<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: wp('90%'),marginTop:hp('3%') }}>

<Image
source={require('../../../../assets/shop.png')} style={{ height: hp("5%"), width: wp("10%"), alignSelf: 'center',top:hp('1%'),right:hp('1%') }}
// resizeMode={FastImage.resizeMode.contain}
resizeMode={'contain'}



/>
<View style={{ width: wp('60%'), }}>
<Text style={{ fontSize: 13.5, color: '#FBFBFB',fontFamily:'SFProText-Semibold' }}>{'Paid To Samya juice \ncorner'}</Text>
<Text style={{ fontSize: 12, color: '#909090', marginTop: hp('2%'),top:hp('1%') }}>Today 4:30 PM</Text>

</View>
<Text style={{ fontSize: 15, color: 'white', textAlign: 'center',fontFamily:'SFProText-Semibold' }}>₹ -200</Text>

</View>

<View style={{ borderWidth: 0.3, borderColor: '#464646', width: wp('85%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>





<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: wp('90%'),marginTop:hp('2%') }}>

<Image
source={require('../../../../assets/shop.png')} style={{ height: hp("5%"), width: wp("10%"), alignSelf: 'center',top:hp('1%'),right:hp('1%') }}
// resizeMode={FastImage.resizeMode.contain}
resizeMode={'contain'}



/>

<View style={{ width: wp('60%'), }}>
<Text style={{ fontSize: 13.5, color: 'white',fontFamily:'SFProText-Semibold' }}>{'Paid To Samya juice \ncorner'}</Text>
<Text style={{ fontSize: 12, color: '#909090', marginTop: hp('2%'),top:hp('1%') }}>Today 4:30 PM</Text>

</View>
<Text style={{ fontSize: 15, color: 'white', textAlign: 'center',fontFamily:'SFProText-Semibold' }}>₹ -200</Text>

</View>
<View style={{ borderWidth: 0.3, borderColor: '#464646', width: wp('85%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>


</View>

</ScrollView>

    </View>
  );
}

export default Account;