/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image, Button, TouchableHighlight, TextInput, TouchableWithoutFeedback, KeyboardAvoidingView,
    Keyboard,
    TouchableOpacity,
    Alert
} from 'react-native';


// import {  } from 'react-navigation-stack';
import {useDispatch} from 'react-redux'
 import {Otp_Send_Api} from './App/Action/Auth'
 import * as Utility from './App/utility'
 import Loader from './App/Components/Loader'
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
const PhoneNumberPage = ({ navigation }) => {
    const dispatch=useDispatch()
    const [loading,setLoading]=useState(false)
    const [mobile, setMobile] = useState('')
const goToRoute=async()=>{

    if (await Utility.isFieldEmpty(mobile)) {
        return Alert.alert('Please fill mobile number')
    }
    else if (await mobile.length < 10) {
        return Alert.alert('Please fill valid number')
    }
    else {
        setLoading(true)
        // navigation.navigate('OtpPage')
        let res=await dispatch(Otp_Send_Api(mobile))
        if(res.status=='success'){
            setLoading(false)
            navigation.navigate('OtpPage')
        }
        else{
            setLoading(false)

            return Alert.alert('Something went wrong')
        }
    }
}

    return (
        <SafeAreaView style={{ flex: 1 }}>
<Loader isLoading={loading}></Loader>

            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                {/* <KeyboardAvoidingView
//   keyboardVerticalOffset = { -160}  
  style = {{ flex: 1, backgroundColor:'red' }}
//   behavior = "position" 
behavior={Platform.OS == "ios" ? "padding" : "height"}
  > */}
                <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-evenly' }}>
                    <View style={{ flex: 0.8, backgroundColor: '#060606', justifyContent: 'flex-end' }}>

                        <View style={{
                            flex: 0.25, justifyContent: 'space-evenly', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent'
                        }}>
                            <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                            <Image source={require('./assets/Frizpay.png')} style={styles.image} />
                        </View>

                        <View style={{
                            flex: 0.25, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent',
                        }}>
                            <Text style={{
                                color: '#E0E0E0',

                                width: '80%',
                                // height: '100%',
right:15,
                                textAlign: 'left',

                                fontFamily: 'SFPRODISPLAYMEDIUM',
                                fontSize: 28,

                            }}>Enter your Phone Number</Text>

                        </View>

                        <View style={{
                            flex: 0.1, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent',
                            bottom:hp('5%')
                        }}>
                            <Text style={{
                                color: '#E0E0E0',

                                width: '90%',

                                textAlign: 'left',

                                fontFamily: 'SFProText-Regular',
                                fontSize: 12
                            }}>We will send you sms for verifcation on the entered phone number.</Text>

                        </View>

                        {/* <View style={{
                            flex: 0.2, justifyContent: 'center', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent', alignContent: 'space-between', alignSelf: 'auto'
                        }}>

                            <View style={{
                                flex: 1,  flexDirection: 'row',
                                alignItems: 'center', backgroundColor: 'transparent',  alignSelf: 'auto'
                            }}>
                                <Text style={{
                                    color: '#E0E0E0',
                                    textAlign: 'left',

                                    fontFamily: 'SFProText-Regular',
                                    fontSize: 30,
                                    marginBottom: -8
                                }}>+91</Text>

                                <TextInput
                                    style={{
                                        color: 'white', fontSize: 30, backgroundColor: 'transparent',
                                        textAlign: 'center',
                                        fontFamily: 'SFProText-Regular',
                                        marginBottom: 0,
                                        paddingBottom: 0,
                                    }}
                                    // onChangeText={onChangeNumber}
                                    value='9999999999'
                                    placeholder="99999999"
                                    keyboardType="numeric"
                                />
                            </View>




                            <View style={{
                                flex: 0.2,
                                width: '90%', height: 1, backgroundColor: 'transparent',
                                alignItems: 'center'
                            }}>
                                <View style={{ width: '95%', height: 1, backgroundColor: '#B4B4B4' }}></View>
                            </View>



                        </View> */}
<View style={{bottom:hp('7%')}}>
                            <View style={{ width: wp('60%'), marginLeft: wp('5%'), flexDirection: 'row', alignItems: 'center', }}>
                       <Text style={{
                                    color: '#E0E0E0',
                                    textAlign: 'left',

                                    fontFamily: 'SFProText-Regular',
                                    fontSize: 22,
                                    marginBottom: -10
                                }}>+91</Text>
                                <View style={{height:hp('4%'),width:wp('0.2%'),backgroundColor:'white',marginLeft:wp('2%'),top:hp('0.8%')}}></View>
                                <TextInput
                                    style={{
                                        color: '#707070', fontSize: 25, backgroundColor: 'transparent',
                                        fontFamily: 'SFProText-Regular',
                                        marginBottom: 0,
                                        paddingBottom: 0,
                                        marginLeft: wp('2%'),
                                        width: wp('40%'),
                                        alignItems: 'center'

                                    }}
                                    // onChangeText={onChangeNumber}
                                    onChangeText={(text) => setMobile(text)}
                                    placeholder="99999999"
                                    keyboardType="numeric"
                                    maxLength={10}
                                />
                       </View>
                                <View style={{ width: wp('57%'),marginLeft:wp('5%'), height: 1, backgroundColor: '#B4B4B4',marginBottom:hp('5%'),marginTop:hp('2%') }}></View>
                     
                     
                                </View>
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                        <View style={{
                            flex: 0.4, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent',
                            bottom:hp('8%')
                        }}>
                            <Text style={{
                                color: '#E0E0E0',

                                width: '90%',

                                textAlign: 'left',

                                fontFamily: 'SFProText-Regular',
                                fontSize: 12
                            }}>By clicking continue you continue you accept terms of service and Privacy Policy of Fri-zpay</Text>

                        </View>

                        </View>




                    <View style={{
                        flex: 0.1, justifyContent: 'center', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent'
                    }}>
                        {/* <TouchableHighlight
                            style={styles.doneButton}
                            onPress={() => goToRoute()}
                            underlayColor='#E8EB59'>
                            <Text style={styles.submitTitle}>Continue</Text>
                        </TouchableHighlight> */}
                        <TouchableOpacity onPress={()=>goToRoute()}>
  <View style={{ height: hp('6.5%'), width: wp('85%'), marginTop: hp('2%'),alignSelf:'center', backgroundColor: '#E8EB59', borderRadius: 13, alignItems: 'center', justifyContent: 'center' }}>
        
        <Text style={{color:'black',fontFamily: 'SF-Pro-Rounded-Semibold',}}>Continue</Text>
      </View>
      </TouchableOpacity>

                    </View>
                    {/* <View style={{ flex: 0.1}}/> */}

                </View>
                {/* </KeyboardAvoidingView> */}
            </ScrollView>


        </SafeAreaView>

    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 105,
        height: 27,
        resizeMode:'contain',


    },
    imagelogo: {
        width: 35,
        height: 40,
        // marginTop: 32,
        // marginBottom: 20
    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '85%',
        height: 51,
        backgroundColor: '#E8EB59',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        // fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 17,
        color: 'black',
        fontWeight:'bold'


    }
});

export default PhoneNumberPage;
