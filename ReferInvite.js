/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from "react-native-vector-icons/FontAwesome"
import {Input} from "react-native-elements"
 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
 
 const ReferInvite = ({ navigation }) => {
 
 
     return (
         <SafeAreaView style={{ flex: 1, backgroundColor: '#060606',  }}>
            
            <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
 
                             fontFamily: 'SFPRODISPLAYBOLD',
                            // fontWeight:'800',
                             fontSize: 28,
                             top:hp('3%'),
                             width:wp('90%'),
                             alignSelf:'center'
                             
                         }}>Invite family</Text>












                     {/* <ScrollView style={{ flex: 1 , height:'10%', width:'100%' }} contentContainerStyle={{ flexGrow: 1, height:'100%', width:'100%', alignContent:'center', flex:1}}> */}
 
 
 
                     {/* <View style={{
                         flex: 0.3, justifyContent: 'space-around', flexDirection: 'column',
                         alignItems: 'flex-start', backgroundColor: 'transparent', width: '90%'
                     }}>
                         <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                             textAlign: 'left',
 
                             fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontSize: 22
                         }}>Notifications and Alerts</Text>

 
               
 </View> */}
   <View style={{ flexDirection: 'row', width: wp('90%'), alignSelf: 'center' }}>
   <TouchableOpacity
 
 onPress={() => {
    

     navigation.goBack();

 }}
 underlayColor='grey'>

 <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{top:hp('6%'),right:wp('3%')}} />


</TouchableOpacity>



          <Input
            placeholder={''}
            leftIcon={
              <Icon
                name={'search'}
                size={17}
                color={'#C9C9C9'}
                style={{ alignSelf: 'center', marginRight: 5,top:hp('0.5%') }}
              />
            }
            inputStyle={{color:'#C0C0C0'}}

            inputContainerStyle={{
              borderColor:'#484848',
              borderWidth: 0,

              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              top: 10,
            }}
            containerStyle={{
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              height: hp('7%'),
              backgroundColor: '#484848',
              width: wp('80%'),
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              borderRadius: 10,
              marginTop:hp('5%')
            }}
            rightIcon={
              <TouchableOpacity>

              </TouchableOpacity>
            }
          />

        </View>


<Text style={{fontSize:20,fontFamily:'SFPRODISPLAYBOLD' ,color:'white',marginTop:hp('3%'),width:wp('80%'),alignSelf:'center'}}>Frizday users</Text>









 
 <View style={{ width: wp('90%'), alignSelf: "center",  alignItems: "center", flexDirection: "row",marginTop:hp('1%') }}>
          <Image source={require('./assets/teenager_logo.png')} style={{height:40,width:40,top:hp('1%')}} resizeMode="contain" />
          <View style={{ width: wp('60%'),  marginLeft:wp('5%'),marginTop:hp('1%') }}>
            <Text style={{color:'white',fontSize:15,fontFamily:'SFPRODISPLAYMEDIUM'}}>Shubham sharma</Text>
            <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}>9056343431</Text>

          </View>
          {/* <Switch value={false} color="orange" /> */}

        </View>
        <View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>

        <View style={{ width: wp('90%'), alignSelf: "center",  alignItems: "center", flexDirection: "row",marginTop:hp('1%') }}>
          <Image source={require('./assets/teenager_logo.png')} style={{height:40,width:40,top:hp('1%')}} resizeMode="contain" />
          <View style={{ width: wp('60%'),  marginLeft:wp('5%'),marginTop:hp('1%') }}>
            <Text style={{color:'white',fontSize:15,fontFamily:'SFPRODISPLAYMEDIUM'}}>Shubham sharma</Text>
            <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}>9056343431</Text>

          </View>
          {/* <Switch value={false} color="orange" /> */}

        </View>
        <View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>


















        <Text style={{fontSize:20,fontFamily:'SFPRODISPLAYBOLD' ,color:'white',marginTop:hp('3%'),width:wp('80%'),alignSelf:'center'}}> Non-Frizpay users</Text>









 
<View style={{ width: wp('90%'), alignSelf: "center",  alignItems: "center", flexDirection: "row",marginTop:hp('2%') }}>
         <Image source={require('./assets/teenager_logo.png')} style={{height:35,width:35,top:hp('1%')}} resizeMode="contain" />
         <View style={{ width: wp('60%'),  marginLeft:wp('5%'),marginTop:hp('1%') }}>
           <Text style={{color:'white',fontSize:15,fontFamily:'SFPRODISPLAYMEDIUM'}}>Shubham sharma</Text>
           <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}>9056343431</Text>

         </View>
         {/* <Switch value={false} color="orange" /> */}

       </View>
       <View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>

       <View style={{ width: wp('90%'), alignSelf: "center",  alignItems: "center", flexDirection: "row",marginTop:hp('1%') }}>
         <Image source={require('./assets/teenager_logo.png')} style={{height:35,width:35,top:hp('1%')}} resizeMode="contain" />
         <View style={{ width: wp('60%'),  marginLeft:wp('5%'),marginTop:hp('1%') }}>
           <Text style={{color:'white',fontSize:15,fontFamily:'SFPRODISPLAYMEDIUM'}}>Shubham sharma</Text>
           <Text style={{color:'grey',fontSize:10,marginTop:hp('1%')}}>9056343431</Text>

         </View>
         {/* <Switch value={false} color="orange" /> */}

       </View>
       <View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>










       <View style={{ height: hp('7%'), width: wp('80%'), marginTop: hp('7%'), backgroundColor: '#E8EB59', borderRadius: 10, alignItems: 'center', justifyContent: 'center',alignSelf:'center' }}>
            <Text style={{ fontFamily:'SF-Pro-Rounded-Semibold',color:'black'}}>Invite</Text>
          </View>
              
         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default ReferInvite;
 