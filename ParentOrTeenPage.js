/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image, Button, TouchableHighlight,
    TouchableOpacity,
    Alert
} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
import * as Utility from './App/utility'
const ParentOrTeenPage = ({ navigation }) => {
const [selectedGender,setSelectedGender]=useState('')
 const goToPhone=async()=>{

if(selectedGender==''){
    return Alert.alert('Please select one option')
}
else{
    await Utility.setInLocalStorge('gender',selectedGender)
    // navigation.navigate('PhoneNumberPage')
    navigation.navigate('Login')

}

 }
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-evenly' }}>
                <View style={{ flex: 0.8, backgroundColor: '#060606', justifyContent: 'flex-start' }}>

                    <View style={{
                        flex: 0.25, justifyContent: 'space-around', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent'
                    }}>
                        <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                        <Image source={require('./assets/Frizpay.png')} style={styles.image} />
                    </View>

                    <View style={{
                        flex: 1, justifyContent: 'space-around', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent'
                    }}>
                        <View style={{
                            flex: 0.4, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent', width: '60%', height: '70%',
                        }}>
                        <TouchableOpacity onPress={()=>setSelectedGender('parent')}>

                            <View style={{ 
                                borderColor:selectedGender=='parent'?'#E8EB59':'#313131',borderWidth:2,
                                backgroundColor: selectedGender=='parent'?'#313131':'#313131', width: wp('60%'), height: '107%', borderRadius: 15, justifyContent: 'space-evenly', alignItems: 'center' }}>
                                <Image source={require('./assets/parents_logo.png')} resizeMode='center' style={styles.parentlogo} width={130} height={130} />
                                
                                <View style={{ color: 'white',
                                    backgroundColor: '#242424',
                                    width: '100%',
                                    height: 35,
                                    textAlign: 'center',
                                    marginBottom: -9,
                                    borderRadius: 15,
                                    //  paddingTop:3
                                    alignItems:'center',
                                    justifyContent:'center'}}>
                                
                                <Text style={{
                                   color: 'white',
                                }}>Parent</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                        </View>
                        <View style={{
                            width: '100%', height: 1, backgroundColor: 'transparent',
                            alignItems: 'center',
                            top:hp('2%')
                        }}>
                            <View style={{ width: '95%', height: 0.5, backgroundColor: '#B4B4B4' }}></View>
                        </View>

                        <View style={{
                            flex: 0.4, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent', width: '60%', height: '70%',
                        }}>
                        <TouchableOpacity onPress={()=>setSelectedGender('child')}>

                            <View style={{ backgroundColor:selectedGender=='child'?'#313131':'#313131', width: wp('60%'), height: '107%', borderRadius: 15, justifyContent: 'space-evenly', alignItems: 'center',
                            borderColor:selectedGender=='child'?'#E8EB59':'#313131',borderWidth:2 }}>
                                <Image source={require('./assets/teenager_logo.png')} resizeMode='center' style={styles.teenagerlogo} width={110} height={130} />
                                {/* <Text style={{
                                    color: 'white',
                                    backgroundColor: '#242424',
                                    width: '100%',
                                    height: 25,
                                    textAlign: 'center',
                                    marginBottom: -13,
                                    borderRadius: 15,
                                    //  paddingTop:3
                                }}>Teenager</Text> */}


<View style={{ color: 'white',
                                    backgroundColor: '#242424',
                                    width: '100%',
                                    height: 35,
                                    textAlign: 'center',
                                    marginBottom: -9,
                                    borderRadius: 15,
                                    //  paddingTop:3
                                    alignItems:'center',
                                    justifyContent:'center'}}>
                                
                                <Text style={{
                                   color: 'white',
                                }}>Teenager</Text>
                                </View>
                            </View>

</TouchableOpacity>
                        </View>

                    </View>



                </View>

                <View style={{
                    flex: 0.2, justifyContent: 'center', flexDirection: 'column',
                    alignItems: 'center', backgroundColor: 'transparent'
                }}>
                    {/* <TouchableHighlight
                        style={styles.doneButton}
                        onPress={() => goToPhone()}
                        underlayColor='#E8EB59'>
                        <Text style={styles.submitTitle}>Continue</Text>
                    </TouchableHighlight> */}
<TouchableOpacity onPress={()=>goToPhone()}>
  <View style={{ height: hp('6.5%'), width: wp('85%'), marginTop: hp('2%'),alignSelf:'center', backgroundColor: '#E8EB59', borderRadius: 13, alignItems: 'center', justifyContent: 'center' }}>
        
        <Text style={{color:'black',fontFamily: 'SF-Pro-Rounded-Semibold',}}>Continue</Text>
      </View>
      </TouchableOpacity>
                </View>
                
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 105,
        height: 27,
        resizeMode:'contain',

    },
    imagelogo: {
        width: 35,
        height: 40,

    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
        left:5
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '85%',
        height: 51,
        backgroundColor: '#E8EB59',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        // fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 17,
        color: '#000000',
fontWeight:'bold'

    }
});

export default ParentOrTeenPage;
