/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid,TouchableOpacity
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/FontAwesome';

 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
 
 const FamilyDetail = ({ navigation }) => {
    const [family,setFamily]=useState(false)
    const [activeTab, setActiveTab] = useState('0')
    const [selectedMonth,setSelectedMonth]=useState('This month')
    const [showVisible,setVisible]=useState(false)
     return (
         <SafeAreaView style={{ flex: 1, backgroundColor: '#060606', }}>
<ScrollView>
<TouchableOpacity
 
                             onPress={() => {
                                
 
                                 navigation.goBack();
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{top:hp('3%')}} />
 
 
                         </TouchableOpacity>





 
                 <View style={{ alignSelf: 'center', alignItems: 'center', }}>
          <Image source={require('./assets/ridhima.png')} style={{height:50,width:50,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 22, marginTop: hp('2%'),color:'white',fontWeight:'700' }}>Ridhima</Text>
          <View style={{ height: hp('6%'), width: wp('40%'), marginTop: hp('2%'), backgroundColor: '#E8EB59', borderRadius: 11, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{color:'black',fontFamily:'SF-Pro-Rounded-Semibold',fontWeight:'700'}}>Send money</Text>
          </View>
          <Text style={{ marginTop: hp('1.5%'), fontSize: 25,color:'white',fontFamily:'SFProText-Semibold' }}>₹ 1500</Text>
          <Text style={{ marginTop: hp('1%'), fontSize: 13,color:'#C0C0C0' }}>balance left</Text>

        </View>
        <View style={{ borderWidth: 0.5, borderColor: 'grey', marginTop: hp('3%'),width:wp('95%'),alignSelf:'center' }}></View>
         
         

        {/* <View style={{ flexDirection: 'row', marginTop: hp('5%'), backgroundColor: '#484848', borderRadius: 8, width: wp('45%'), alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ fontSize: 17, color: 'white', textAlign: 'center', padding: hp('1.2%'),fontWeight:'900'}}>This month</Text>
        <Ionicons name={'caret-down'} size={20} color={'white'} style={{left:wp('3%')}}></Ionicons>

      </View> */}
   <View style={{ flexDirection: 'row', backgroundColor: '#484848', borderRadius: 12, width: wp('35%'), alignSelf: 'center', justifyContent: 'center',marginTop:hp('4%') }}>
       <View>
       <Text style={{ fontSize: 15, color: '#F8F8F8',fontFamily:'SF-Pro-Rounded-Semibold', textAlign: 'center',padding:hp('0.3%')}}>{selectedMonth}</Text>

    {showVisible==true?
       <View>
        <Text style={{ fontSize: 15, color: '#F8F8F8', textAlign: 'center', padding: hp('1.2%'),fontFamily:'SF-Pro-Rounded-Semibold',}} onPress={()=>clickMonth('This week')}>This week</Text>
        <Text style={{ fontSize: 15, color: 'white', textAlign: 'center', padding: hp('1.2%'),fontFamily:'SF-Pro-Rounded-Semibold',}} onPress={()=>clickMonth('Last month')}>Last month</Text>
        <Text style={{ fontSize: 15, color: 'white', textAlign: 'center', padding: hp('1.2%'),fontFamily:'SF-Pro-Rounded-Semibold',}} onPress={()=>clickMonth('Last week')}>Last week</Text>
      
        </View>:null}
        </View>
      
      
      <TouchableOpacity onPress={()=>setVisible(!showVisible)}>
      
        <Ionicons name={showVisible==false? 'caret-down':'caret-up'} size={20} color={'#F8F8F8'} style={{left:wp('1%'),top:hp('1%')}}></Ionicons>
        </TouchableOpacity>
      </View>



      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', width: wp('90%'), marginTop: hp('3%') }}>
        <View style={{ height: hp('10%'), width: wp('40%'), alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center',left:wp('1%'),fontFamily:'SFProText-Bold' }}>Total Sent</Text>
          <Text style={{ fontSize: 30, color: 'white', textAlign: 'center',fontFamily:'SFProText-Regular' }}>₹2500</Text>
          {/* <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center',marginTop:hp('3%') }}> Saved by Offers</Text>
          <Text style={{ fontSize: 22, color: 'white', textAlign: 'center',fontWeight:'700' }}>₹ 400</Text> */}


        </View>
        <View style={{ height: hp('5.5%'), width: wp('0.5%'),backgroundColor:'#595959' }}></View>
        <View style={{ height: hp('10%'), width: wp('40%'),  alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center',left:wp('1%'),fontFamily:'SFProText-Bold' }}>Total Spent</Text>
          <Text style={{ fontSize: 30, color: 'white', textAlign: 'center',fontFamily:'SFProText-Regular'}}>₹2500</Text>
          {/* <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center',marginTop:hp('3%') }}> Open Tasks</Text>
          <Text style={{ fontSize: 22, color: 'white', textAlign: 'center',fontWeight:'700' }}>₹ 400</Text> */}


        </View>


      </View>








      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', alignSelf: 'center', width: wp('90%'), marginTop: hp('5%') }}>
        <View style={{ height: hp('10%'), width: wp('40%'), alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          {/* <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center', }}>Total Sent</Text>
          <Text style={{ fontSize: 22, color: 'white', textAlign: 'center',fontWeight:'700' }}>₹ 2500</Text> */}
          <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center',marginTop:hp('3%'),fontFamily:'SFProText-Bold' }}> Saved by Offers</Text>
          <Text style={{ fontSize: 30, color: 'white', textAlign: 'center',fontFamily:'SFProText-Regular' }}>₹400</Text>


        </View>
        <View style={{ height: hp('5.5%'), width: wp('0.5%'),backgroundColor:'#595959',marginTop:hp('3%') }}></View>
        <View style={{ height: hp('10%'), width: wp('40%'),  alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
        {/* <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center', }}>Total Spent</Text>
          <Text style={{ fontSize: 22, color: 'white', textAlign: 'center',fontWeight:'700' }}>₹ 2500</Text> */}
          <Text style={{ fontSize: 12, color: '#B2B2B2', textAlign: 'center',marginTop:hp('3%'),fontFamily:'SFProText-Bold' }}> Open Tasks</Text>
          <Text style={{ fontSize: 30, color: 'white', textAlign: 'center',fontFamily:'SFProText-Regular' }}>1</Text>


        </View>


      </View>


      <View style={{ borderWidth: 0.5, borderColor: 'grey', marginTop: hp('5%'),alignSelf:'center',width:wp('95%') }}></View>




      <View style={{ width: wp('80%'), height: hp('5%'), backgroundColor: '#313131', alignSelf: 'center', borderRadius: 10, flexDirection: 'row',marginTop:hp('2%') }}>
        <TouchableOpacity onPress={() => setActiveTab('0')}>
          <View style={{ width: wp('25%'), height: hp('5%'), backgroundColor: activeTab == '0' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 9,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12.35,
        
        elevation:activeTab=='0'? 19:0,
        }}>
          <Text style={{color:'white',fontFamily:'roundedBold',top:hp('0.1%'),fontSize:16}}>All</Text>

          </View>
        </TouchableOpacity >
        <TouchableOpacity onPress={() => setActiveTab('1')}>
          <View style={{ width: wp('25%'), height: hp('5%'), backgroundColor: activeTab == '1' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 9,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12.35,
        
        elevation:activeTab=='1'? 19:0,
        }}>
            <Text style={{color:'white',fontFamily:'roundedBold',top:hp('0.1%'),fontSize:16}}>Recieved</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setActiveTab('2')}>

          <View style={{ 
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 9,
            },
            shadowOpacity: 0.30,
            shadowRadius: 12.35,
            
            elevation:activeTab=='2'? 19:0,
            width: wp('25%'), height: hp('5%'), backgroundColor: activeTab == '2' ? '#484848' : null, borderRadius: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{color:'white',fontFamily:'roundedBold',top:hp('0.1%'),fontSize:16}}>Spent</Text>

          </View>
        </TouchableOpacity>
      </View>



      <View style={{marginBottom:hp('10%')}}>


<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: wp('90%'),marginTop:hp('3%') }}>

<Image
source={require('./assets/shop.png')} style={{ height: hp("5%"), width: wp("10%"), alignSelf: 'center',top:hp('0.7%'),right:hp('1%') }}
// resizeMode={FastImage.resizeMode.contain}
resizeMode={'contain'}



/>
<View style={{ width: wp('60%'), }}>
<Text style={{ fontSize: 13.5, color: '#FBFBFB',fontFamily:'SFProText-Semibold' }}>{'Paid To Samya juice \ncorner'}</Text>
<Text style={{ fontSize: 12, color: '#909090', marginTop: hp('2%'),top:hp('1%') }}>Today 4:30 PM</Text>

</View>
<Text style={{ fontSize: 15, color: 'white', textAlign: 'center',fontFamily:'SFProText-Semibold' }}>₹ -200</Text>

</View>

<View style={{ borderWidth: 0.3, borderColor: '#464646', width: wp('85%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>





<View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between', alignItems: 'center', width: wp('90%'),marginTop:hp('2%') }}>

<Image
source={require('./assets/shop.png')} style={{ height: hp("5%"), width: wp("10%"), alignSelf: 'center',top:hp('0.7%'),right:hp('1%') }}
// resizeMode={FastImage.resizeMode.contain}
resizeMode={'contain'}



/>

<View style={{ width: wp('60%'), }}>
<Text style={{ fontSize: 13.5, color: 'white',fontFamily:'SFProText-Semibold' }}>{'Paid To Samya juice \ncorner'}</Text>
<Text style={{ fontSize: 12, color: '#909090', marginTop: hp('2%'),top:hp('1%') }}>Today 4:30 PM</Text>

</View>
<Text style={{ fontSize: 15, color: 'white', textAlign: 'center',fontFamily:'SFProText-Semibold' }}>₹ -200</Text>

</View>
<View style={{ borderWidth: 0.3, borderColor: '#464646', width: wp('85%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>


</View>
</ScrollView>
          
         
         
         
             














         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default FamilyDetail;
 