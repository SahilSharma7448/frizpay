/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    FlatList,
    useColorScheme,
    View,
    Image, Button, TouchableHighlight, TextInput, TouchableWithoutFeedback,
    Keyboard,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import { Switch } from 'react-native-elements';

import {widthPercentageToDP as wp,heightPercentageToDP as hp, } from "./App/utility"
var pauseCard=true
const LeftDrawerSettingsPage = ({ navigation }) => {
    const data=[  
    //    {img:require('./assets/teenager_logo_small.png'),title:'Shubham Sharma',subtitle:'ID, KYC Status',arrow:'true',route:'EditProfile',showToogle:''},
       {img:require('./assets/edit-family.png'),title:'Edit Family',subtitle:'',arrow:'',route:'EditFamily',showToogle:''},
       {img:require('./assets/safety-icon.png'),title:'Safety and control settings',subtitle:'',arrow:'',route:'SaftyControl',showToogle:''},
      
       {img:require('./assets/pause-card.png'),title:'Pause the card',subtitle:'',arrow:'',route:'',showToogle:'true'},
       {img:require('./assets/get-support.png'),title:'Get support',subtitle:'',arrow:'',route:'GetSupport',showToogle:''},
       {img:require('./assets/notif-left.png'),title:'Notifications and alert',subtitle:'',arrow:'',route:'NotificationAlert',showToogle:''},
       {img:require('./assets/refer-icon.png'),title:'Refer and earn',subtitle:'Refer a teen and earn rewards',arrow:'',route:'ReferInvite',showToogle:''},
    //    {img:require('./assets/refer-code.png'),title:'Your refferal code',subtitle:'',arrow:'',route:'',showToogle:''},

    ]
    const chnagValue=async()=>{
        console.log(pauseCard)
        if(pauseCard==true){
            pauseCard=false
        }
        else{
            pauseCard=true

        }
    }

    return (
        <SafeAreaView style={{ flex: 1,backgroundColor:'#060606' }}>
          <ScrollView showsVerticalScrollIndicator={false}>
              <TouchableOpacity onPress={()=>navigation.closeDrawer()}>
<View style={{marginTop:hp('3%'),width:wp('90%'),alignSelf:'center',flexDirection:'row',alignItems:'center'}}>
<Icon name={'angle-left'} size={30} color={'#E8EB59'} style={{top:hp('0.5%')}}></Icon>
{/* <Text style={{
                                color: '#FFFFFF',




                                fontFamily: 'SFPRODISPLAYBOLD',
                                fontSize: 35,
marginLeft:wp('3%')
                            }}>Settings</Text> */}

<Image source={require('./assets/setting.png')} style={{height:hp('6%'),width:wp('35%'),marginLeft:wp('3%'),top:hp('0.5%'),palignSelf:'center',alignItems:'center',justifyContent:'center'}} resizeMode={'stretch'}></Image>
 </View>
 </TouchableOpacity>



{/* 
 <Image source={require('./assets/shubham2.png')} style={{height:hp('9%'),width:wp('100%'),alignSelf:'center',alignItems:'center',justifyContent:'center',marginRight:wp('2%'),marginTop:hp('2%')}} resizeMode={'contain'}></Image> */}

<TouchableOpacity onPress={()=>navigation.navigate('EditProfile')}>
 <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginTop:hp('2%'),width:wp('90%')}}>
     <View style={{width:wp('10%')}}>
<Image source={require('./assets/shubham.png')} style={{height:50,width:50,borderRadius:50,top:hp('1%'),right:wp('2%')}} resizeMode="contain" />

 {/* <Image source={require('./assets/ridhima.png')} resizeMode='stretch' width={53} height={53} style={{ }} /> */}
 </View>                      
 <View style={{width:wp('70%'),marginLeft:wp('7%'),justifyContent:'center',}}>
 <Text style={{
                                        color: '#C0C0C0',
                                        textAlign: 'left',

                                        fontFamily: 'SFPRODISPLAYMEDIUM',
                                        fontSize: 14,
                                        top:hp('1%')
                                    }} >{'Shubham Sharma'}</Text>
                                     <Text style={{
                                        color: '#C0C0C0',
                                        textAlign: 'left',

                                        fontFamily: 'SFPRODISPLAYMEDIUM',
                                        fontSize: 12,
                                        top:hp('1%')

                                    }}>{'ID, KYC Status'}</Text>
 </View>
 <Icon name={'angle-right'} size={30} color={'#E8EB59'}></Icon>
 


 </View>
 </TouchableOpacity>







 <View style={{
                                    width: wp('81%'), height: 1,
                                    backgroundColor: '#464646',
                                    alignSelf:'flex-end',
                                    marginTop:hp('2%')
                                }}></View>

<FlatList  
                    data={data}  
                    // style={{marginTop:hp('2%')}}
                    renderItem={({item}) =>  
                       <View>
                           <TouchableOpacity onPress={()=>navigation.navigate(item.route)}>
 <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginTop:hp('2%'),width:wp('90%')}}>
     <View style={{width:wp('10%')}}>
 <Image source={item.img} resizeMode='contain' width={50} height={50} style={{ left:wp('1.5%')}} />
 </View>                      
 <View style={{width:wp('65%'),marginLeft:wp('7%'),justifyContent:'center'}}>
 <Text style={{
                                        color: '#C0C0C0',
                                        textAlign: 'left',

                                        fontFamily: 'SFPRODISPLAYMEDIUM',
                                        fontSize: 14,
                                        top:hp('1%')
                                    }} >{item.title}</Text>
                                     <Text style={{
                                        color: '#5F5F5F',
                                        textAlign: 'left',

                                        fontFamily: 'SFPRODISPLAYMEDIUM',
                                        fontSize: 12,
                                        top:hp('1%')

                                    }}>{item.subtitle}</Text>
 </View>

 {item.arrow!==''?
<Icon name={'angle-right'} size={30} color={'#E8EB59'} style={{right:hp('2%')}}></Icon>:
         null

}
{item.showToogle!==''?
 <Switch value={pauseCard} color="#8C8C8C"  style={{right:wp('7%')}} onChange={()=>chnagValue()} thumbColor={'#E8EB59'}/>:null}


 </View>
 </TouchableOpacity>
 <View style={{
                                    width: wp('81%'), height: 1,
                                    backgroundColor: '#464646',
                                    alignSelf:'flex-end',
                                    marginTop:hp('2%')
                                }}></View>
                       </View>


                }  
                />  


<TouchableOpacity >
 <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginTop:hp('2%'),width:wp('90%')}}>
     <View style={{width:wp('10%')}}>
 <Image source={require('./assets/refer-code.png')} resizeMode='contain' width={53} height={53} style={{ left:wp('1.5%')}} />
 </View>                      
 <View style={{width:wp('45%'),marginLeft:wp('7%'),justifyContent:'center',}}>
 <Text style={{
                                        color: '#C0C0C0',
                                        textAlign: 'left',

                                        // fontFamily: 'SFPRODISPLAYBOLD',
                                        fontWeight:'600',
                                        fontSize: 14,
                                        top:hp('1%')
                                    }} >{'Your refferal code'}</Text>
                                     <Text style={{
                                        color: '#C0C0C0',
                                        textAlign: 'left',

                                        fontFamily: 'SFPRODISPLAYMEDIUM',
                                        fontSize: 12,
                                        top:hp('1%')

                                    }}>{''}</Text>
 </View>
<View style={{height:hp('4%'),width:wp('30%'),borderColor:'#E8EB59',borderWidth:1,borderRadius:7,alignSelf:'center',justifyContent:'center',alignItems:'center'}}>
<Text style={{
                                        color: '#C0C0C0',
                                        textAlign: 'left',

                                        fontFamily: 'SFPRODISPLAYMEDIUM',
                                        fontSize: 13,

                                    }}>{'YXTR567D'}</Text>
</View>
 


 </View>
 </TouchableOpacity>
























<View style={{
                        flex: 1, justifyContent: 'flex-start', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent', height: 350, marginTop: 50, width:'100%'
                    }}>

                        <View style={{
                            flex: 0.6, justifyContent: 'space-evenly', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent', height: 0
                        }}>
                            <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                            <Image source={require('./assets/Frizpay.png')} style={styles.image} />
                        </View>

                        <Text style={{
                            color: '#E0E0E0',


                            width: '85%',

                            textAlign: 'center',

                            fontFamily: 'SFPRODISPLAYBOLD',
                            fontSize: 15,
                            marginTop: 21,
                            bottom:hp('4%')
                        }}>About us

                        </Text>

                        <Text style={{
                            color: '#E0E0E0',

                            width: '60%',

                            textAlign: 'center',

                            fontFamily: 'SFPRODISPLAYMEDIUM',
                            fontSize: 10,
                            bottom:hp('4%')

                        }}>Frizpay is numberless prepaid card for teenagers from which they can  make payments can learn more about personal finance and investing. Frizpay is marketing partner of Axis bank

                        </Text>

                    </View>

                
          </ScrollView>
        </SafeAreaView>

    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 90,
        height: 20,
resizeMode:'contain',
bottom:hp('1%')
    },
    imagelogo: {
        marginTop:hp('2%'),

        width: 35,
        height: 40,
        // marginTop: 32,
        // marginBottom: 20
    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '85%',
        height: 51,
        backgroundColor: '#E8EB59',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 17,
        color: '#000000',


    }
});

export default LeftDrawerSettingsPage;
