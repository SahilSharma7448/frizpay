// import React, {useState, useEffect} from 'react';
// import {Alert} from 'react-native';
// import {Provider} from 'react-redux';
// import {
//   NavigationContainer,
//   DefaultTheme,
//   DarkTheme,
//   useTheme,
// } from '@react-navigation/native';
// import MainStackNavigator from './App/Navigation/StackNavigator';
// import AppIntro from "./AppIntro"
// const App = () => {
//   return (
//       <NavigationContainer>
//           <AppIntro />
//       </NavigationContainer>
//   );
// };
// export default App;












/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';
 import type { Node } from 'react';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   useColorScheme,
   View,
   Image, Button, TouchableHighlight
 } from 'react-native';
 
 import {
   Colors,
   DebugInstructions,
   Header,
   LearnMoreLinks,
   ReloadInstructions,
 } from 'react-native/Libraries/NewAppScreen';
 
 import SplashScreen from 'react-native-splash-screen'
 
 import AppIntro from './AppIntro'
 import ParentOrTeenPage from './ParentOrTeenPage'
 import PhoneNumberPage from './PhoneNumberPage'
 import OtpPage from './OtpPage'
 import NameAndPinPage from './NameAndPinPage'
 // import AadharNumberPage from './AadharNumberPage'
 // import AadharOTPPage from './AadharOTPPage'
 // import HomeTabPage from './HomeTabPage'
 import PanCardDetailsPage from './PanCardDetailsPage'
 import PanCardNumberPage from './PanCardNumberPage'
 import PanDeclarationPage from './PanDeclarationPage'
 import LeftDrawerPage from './LeftDrawerPage'
 
 
 import MainStackNavigator from './App/Navigation/StackNavigator';
 
 
 
 import { NavigationContainer } from '@react-navigation/native';
 import { createNativeStackNavigator } from '@react-navigation/native-stack';
 
 import { Provider } from 'react-redux';
import configureStore from './App/Redux/configureStore';
const store = configureStore()
 const Stack = createNativeStackNavigator();
 const App: () => Node = () => {
 
   const [showRealApp, setshowRealApp] = useState(false);
 
 
 
   useEffect(() => {
     SplashScreen.hide();
   }, []);
 
   // Ionicons.loadFont();
 
   return (
    <Provider store={store}>
     <NavigationContainer>
 
 
       {/* <Stack.Navigator initialRouteName="AppIntro" screenOptions={{
         headerShown: false,
 
       }}>
         <Stack.Screen name="AppIntro" component={AppIntro} />
         <Stack.Screen name="ParentOrTeenPage" component={ParentOrTeenPage} />
         <Stack.Screen name="PhoneNumberPage" component={PhoneNumberPage} />
         <Stack.Screen name="OtpPage" component={OtpPage} />
         <Stack.Screen name="NameAndPinPage" component={NameAndPinPage} />
         <Stack.Screen name="PanCardDetailsPage" component={PanCardDetailsPage} />
         <Stack.Screen name="PanCardNumberPage" component={PanCardNumberPage} />
         <Stack.Screen name="PanDeclarationPage" component={PanDeclarationPage} />
         
         
         <Stack.Screen name="LeftDrawerPage" component={LeftDrawerPage} />
         
         
 
       </Stack.Navigator> */}
 <MainStackNavigator></MainStackNavigator>
     </NavigationContainer>
     </Provider>
 
   );
 };
 
 const styles = StyleSheet.create({
   sectionContainer: {
     marginTop: 32,
     paddingHorizontal: 24,
   },
   sectionTitle: {
     fontSize: 24,
     fontWeight: '600',
   },
   sectionDescription: {
     marginTop: 8,
     fontSize: 18,
     fontWeight: '400',
   },
   highlight: {
     fontWeight: '700',
   }
 });
 
 export default App;