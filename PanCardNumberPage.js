/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     Image, Button, TouchableHighlight, TextInput, TouchableWithoutFeedback,
     Keyboard,
    Alert,
     
 } from 'react-native';
import { widthPercentageToDP as wp,heightPercentageToDP as hp} from './App/utility';
 
import { useDispatch, useSelector } from 'react-redux'

import Loader from './App/Components/Loader'
import { Create_Parent_Api } from './App/Action/Auth'
 const PanCardNumberPage = ({ navigation }) => {
     const GetParent = useSelector(state => state.PARENT_DETAIL);
     const dispatch = useDispatch()

     console.log('gettt mobile,get', GetParent)
     const [field1, setField1] = useState('')
     const [field2, setField2] = useState('')
     const [field3, setField3] = useState('')
     const [loading, setLoading] = useState(false)
     const goToRoute = async () => {
         let panCard = field1 + field2 + field3
         console.log('check pan card', panCard)
         if (await panCard.length < 10) {
             return Alert.alert('Please fill valid pancard')
         }
         else {
             setLoading(true)
             let res = await dispatch(Create_Parent_Api(GetParent, panCard))
             if (res.id) {
                 setLoading(false)
                 Alert.alert(
                     '',
                     'Parent Create Successfully',
                     [

                         { text: 'OK', onPress: () => navigation.navigate('DrawerPage') },
                     ]
                 );
             }
             else {
                 setLoading(false)
                 return Alert.alert(res.message)
             }
         }
     }
 
     return (
         <SafeAreaView style={{ flex: 1 }}>
             <Loader isLoading={loading}></Loader>
             <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                 <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-evenly' }}>
                     <View style={{ flex: 0.7, backgroundColor: '#060606', justifyContent: 'flex-start' }}>
 
                         <View style={{
                             flex: 0.25, justifyContent: 'space-evenly', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent'
                         }}>
                             <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                             <Image source={require('./assets/Frizpay.png')} style={styles.image} />
                         </View>
 
                         <View style={{
                             flex: 0.04, justifyContent: 'flex-start', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent',
                         }}>
                             <Text style={{
                                 color: '#E0E0E0',
 
                                 width: '90%',
 
 
                                 textAlign: 'left',
 
                                 fontFamily: 'SFPRODISPLAYMEDIUM',
                                 fontSize: 32
                             }}>Enter your
                             Pancard Number</Text>
 
                             <Text style={{
                                 color: '#E0E0E0',
 
                                 width: '90%',
 
                                 textAlign: 'left',
 
                                 fontFamily: 'SFProText-Regular',
                                 fontSize: 12
                             }}>Enter the 10 digit number present on the Pan-card.</Text>
 
 
 
                         </View>
 
 
                         <View style={{flexDirection:'row',alignSelf:'center',width:wp('100%'),alignItems:'center',justifyContent:'center',}}>
                                 <View >
                                 <TextInput style={{ width: wp('20%'), color: 'white', fontSize: 25 }}
                                     onChangeText={(text) => setField1(text)}
                                     maxLength={4}></TextInput>
                                 <View style={{ width: wp('20%'), height: 1, backgroundColor: '#B4B4B4' }}></View>
                              
                                 </View>

                                 <View style={{ width: wp('3%'), height: 1, backgroundColor: '#B4B4B4',marginLeft:wp('4%'),marginRight:wp('4%') }}></View>
                              







                                 <View style={{marginLeft:wp('1%')}}>
                                 <TextInput style={{ width: wp('20%'), color: 'white', fontSize: 25 }}
                                     onChangeText={(text) => setField2(text)}

                                     maxLength={4}></TextInput>

                                 <View style={{ width: wp('20%'), height: 1, backgroundColor: '#B4B4B4' }}></View>
                              
                                 </View>





                                 <View style={{ width: wp('3%'), height: 1, backgroundColor: '#B4B4B4',marginLeft:wp('4%'),marginRight:wp('4%') }}></View>

                                 <View style={{marginLeft:wp('1%')}}>
                                 <TextInput style={{ width: wp('20%'), color: 'white', fontSize: 25 }}
                                     onChangeText={(text) => setField3(text)}

                                     maxLength={4}></TextInput>

                                 <View style={{ width: wp('20%'), height: 1, backgroundColor: '#B4B4B4' }}></View>
                              
                                 </View>
                             </View>
 
 
                         <View style={{
                             flex: 0.7, justifyContent: 'flex-start', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent', alignContent: 'space-between', alignSelf: 'auto'
                         }}>
 
 
                             {/* <TextInput
                                 style={{
                                     color: 'white', fontSize: 30, backgroundColor: 'transparent',
                                     textAlign: 'center',
                                     fontFamily: 'SFProText-Regular',
                                     marginBottom: 0,
                                     paddingBottom: 0,
                                 }}
                                 // onChangeText={onChangeNumber}
                                 value='9999-9999-9999'
                                 placeholder="9999-9999-9999"
                                 keyboardType="numeric"
                             />
 
                             <View style={{
                                 flex: 0.2,
                                 width: '100%', height: 1, backgroundColor: 'transparent',
                                 alignItems: 'center'
                             }}>
                                 <View style={{ width: '95%', height: 1, backgroundColor: '#B4B4B4' }}></View>
                             </View> */}

                            
                         </View>
 
 
                         <View style={{ width: '100%', height: 25, backgroundColor: 'transparent', justifyContent:'center', alignItems:'center', marginTop:20 }}>
                        <Text style={{
                                color: '#E0E0E0',

                                width: '100%',

                                textAlign: 'center',

                                fontFamily: 'SFProText-Regular',
                                fontSize: 12
                             }}>2/2</Text>
                        </View>
 
 
 
                     </View>
                     <View style={{
                         flex: 0.2, justifyContent: 'space-around', flexDirection: 'column',
                         alignItems: 'center', backgroundColor: 'transparent'
                     }}>
                         <Text style={{
                             color: '#E0E0E0',
 
                             width: '85%',
 
                             textAlign: 'left',
 
                             fontFamily: 'SFProText-Regular',
                             fontSize: 12
                         }}>By clicking continue you agree to share your information with our partner bank.
 
                         </Text>
 
                         <TouchableHighlight
                             style={styles.doneButton}
                             onPress={() => goToRoute()}
                             underlayColor='#E8EB59'>
                             <Text style={styles.submitTitle}>Continue</Text>
                         </TouchableHighlight>
 
                     </View>
 
                 </View>
             </ScrollView>
         </SafeAreaView>
 
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 90,
         height: 25,
         resizeMode:'contain'
 
 
     },
     imagelogo: {
         width: 36,
         height: 40,
         resizeMode:'contain'
         // marginTop: 32,
         // marginBottom: 20
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '85%',
         height: 51,
         backgroundColor: '#E8EB59',
         justifyContent: 'center',
         alignItems: 'center',
         borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SF-Pro-Rounded-Semibold',
         fontSize: 14,
         color: '#000000',
         left:wp('1%')
 
     }
 });
 
 export default PanCardNumberPage;
 