import * as React from 'react';
import { Button, View } from 'react-native';
import {
  createDrawerNavigator, DrawerContent,
  DrawerContentComponentProps,
  DrawerScreenProps
} from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import HomeTabPage from './HomeTabPage';
import {
  ParamListBase,
  useNavigation,
  useTheme,
} from '@react-navigation/native';

import LeftDrawerSettingsPage from './LeftDrawerSettingsPage';
import NotificationAlert from "./NotificationAlert"

function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        onPress={() => navigation.navigate('Notifications')}
        title="Go to notifications"
      />
    </View>
  );
}

function NotificationsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
}

const Drawer = createDrawerNavigator();

const CustomDrawerContent = (props) => {
  const { colors } = useTheme();
  const navigation = useNavigation();

  return (
    <>
       <LeftDrawerSettingsPage/>
      {/* <DrawerContent {...props} /> */}
    </>
  );
};

export default function LeftDrawerPage() {
  return (

    <Drawer.Navigator initialRouteName="HomeTabPage" screenOptions={{

      headerShown: false,
      drawerType: 'back',//'permanent' , //: 'back',
      drawerStyle: false ? null : { width: '80%' },
      drawerContentContainerStyle: { paddingTop: 0 },
      overlayColor: 'transparent',
    }}
      backBehavior="none"
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      <Drawer.Screen name="HomeTabPage" component={HomeTabPage} />
      <Drawer.Screen name="Notifications" component={NotificationsScreen} />
      <Drawer.Screen name="NotificationAlert" component={NotificationAlert} />

    </Drawer.Navigator>

  );
}