/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image, Button, TouchableHighlight,
    TouchableOpacity
} from 'react-native';

import AppIntroSlider from 'react-native-app-intro-slider';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"

const slides = [
    {
        key: 'one',
        title: 'One app for all friends and family.',
        text: 'Welcomes you',
        imagelogo: require('./assets/frizpay-logo.png'),
        image: require('./assets/Frizpay.png'),
        backgroundColor: '#060606',
    },
    {
        key: 'two',
        title: 'Win friz-coins Invest them and win exciting offers',
        text: 'Welcomes you',
        imagelogo: require('./assets/frizpay-logo.png'),
        image: require('./assets/Frizpay.png'),
        backgroundColor: '#060606',
    },
    {
        key: 'three',
        title: 'Learn to Invest, Save and earn money smartly',
        text: 'Welcomes you',
        imagelogo: require('./assets/frizpay-logo.png'),
        image: require('./assets/Frizpay.png'),
        backgroundColor: '#060606',
    }
];

const AppIntro = ({ navigation }) => {
const [showButton,setShowButton]=useState(0)
    _renderItem = ({ item }) => {
        // if(item.key=='three'){
        //     setShowButton(false)
        // }else{
        //     setShowButton(true)
        // }
        console.log("item",item)
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View
                    style={[
                        styles.slide,
                        {
                            backgroundColor: item.backgroundColor,
                        },
                    ]}>

                    <View style={{
                        flex: 0.15, justifyContent: 'space-evenly', flexDirection: 'column',
                        alignItems: 'center', marginTop: -100
                    }}>
                        <Image source={item.imagelogo} style={styles.imagelogo} />
                        <Image source={item.image} style={styles.image} />
                        <Text style={styles.text}>{item.text}</Text>
                    </View>

                    <Text style={styles.title}>{item.title}</Text>
                </View>
            </SafeAreaView>
        );
    }

    renderDoneBtn = (value) => {
        console.log("check",value)
        setShowButton(2)
        return (



            <View style={{ height: hp('6.5%'), width: wp('85%'), marginTop: hp('2%'),alignSelf:'center', backgroundColor: '#E8EB59', borderRadius: 13, alignItems: 'center', justifyContent: 'center' }}>
        
            <Text style={{color:'black',fontFamily: 'SF-Pro-Rounded-Semibold',}}>Get Started</Text>
          </View>


            // <TouchableHighlight
            //     style={styles.doneButton}
            //     onPress={() => _onDone()}
            //     underlayColor='#E8EB59'>
            //     <Text style={styles.submitTitle}>Get Started</Text>
            // </TouchableHighlight>
            // <Button  style={styles.doneButton} title="Get Started" color="#E8EB59" />
        );
    };

    _onDone = () => {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
        //  setshowRealApp(true);
        navigation.navigate('ParentOrTeenPage');
    }

    return (
        <View style={{ flex: 1,backgroundColor:'#060606' }}>
            <AppIntroSlider style={{
                flex: 1
            }}
                renderItem={_renderItem}
                data={slides}
                onDone={_onDone}
                showDoneButton={true}
                renderDoneButton={renderDoneBtn}
                bottomButton
                showNextButton={false}
                showPrevButton={false}
                showSkipButton={true}
                dotStyle={{ backgroundColor: 'grey' }}
                activeDotStyle={{ backgroundColor: 'white' }}
                onSlideChange={(value)=>setShowButton(value)}
            />
            {showButton == 0 || showButton == 1 ?
                <TouchableOpacity onPress={() => navigation.navigate('ParentOrTeenPage')}>
<View style={{ height: hp('6.5%'), width: wp('85%'), marginBottom:hp('3%'), alignSelf:'center', backgroundColor: '#E8EB59', borderRadius: 13, alignItems: 'center', justifyContent: 'center' }}>
        
        <Text style={{color:'black',fontFamily: 'SF-Pro-Rounded-Semibold',}}>Continue</Text>
                    </View>
                </TouchableOpacity>
                : null}
        </View>
    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 100,
        height: 24,
        marginVertical: 40,
        resizeMode:'contain',

    },
    imagelogo: {
        width: 35,
        height: 40,
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 22,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 20,
        width: '78%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0,
        bottom:15
    },
    doneButton: {

        height: 51,
        backgroundColor: '#E8EB59',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 17,
        color: '#000000',

    }
});

export default AppIntro;
