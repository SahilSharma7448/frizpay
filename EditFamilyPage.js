/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { selectContactPhone } from 'react-native-select-contact';
import { useNavigation } from '@react-navigation/native';



const EditFamilyPage = ({ navigation }) => {

    const navigationContext = useNavigation();

    function getPhoneNumber() {
        return selectContactPhone()
            .then(selection => {
                if (!selection) {
                    return null;
                }

                let { contact, selectedPhone } = selection;
                console.log(`Selected ${selectedPhone.type} phone number ${selectedPhone.number} from ${contact.name}`);
                return selectedPhone.number;
            });
    }

    const requestContactPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                    title: 'Allow Access Contact?',
                    message:
                        'allow this app to read contact information',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('granted');
                getPhoneNumber();
            } else {
                console.log('denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }


    const screenHeight = Dimensions.get('window').height
    const screenWidth = Dimensions.get('window').width
    const renderItem = ({ item }) => (
        <View style={{ backgroundColor: 'transparent', width: '100%', justifyContent: 'flex-start', flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <Image source={require('./assets/teenager_logo_small.png')} resizeMode='center' width={33} height={44} />
            <View style={{ backgroundColor: 'transparent' }}>
                <Text style={styles.membertitle}>{item.title}</Text>
                <Text style={styles.memberPhone}>{item.phone}</Text>
            </View>

        </View>
    );
    const DATA = [
        {
            id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
            title: 'First name',
            phone: '9999999999'
        },
        {
            id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
            title: 'Second name',
            phone: '9999999999'
        },
        {
            id: '58694a0f-3da1-471f-bd96-145571e29d72',
            title: 'Third name',
            phone: '9999999999'
        },
    ];

    console.log(navigation);
    console.log(navigationContext.getState());
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: '#060606', height: screenHeight }}>
                {/* <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}> */}
                {/* //paddingBottom:450 */}


                <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'flex-start', flexDirection: 'column', alignItems: 'center', alignContent: 'space-between' }}>

                    <View style={{
                        flex: 0.1, justifyContent: 'space-around', flexDirection: 'row',
                        alignItems: 'center', backgroundColor: 'transparent', width: '100%'
                    }}>

                        <TouchableHighlight
                            style={{ width: '10%', height: '100%', backgroundColor: 'transparent', justifyContent: 'center', marginLeft: -120, marginRight: 0, position: 'absolute' }}

                            onPress={() => {
                                // navigation.navigate("LoggedInHomePage")
                                // console.log(navigation.getParent());
                                // navigation.toggleDrawer();
                                // navigationContext.navigate('Notifications');
                                // navigationContext.navigate('HomeTabPage',
                                //     {
                                //         screen: 'Home',
                                //         // initial: false,
                                //         params: {
                                //             screen: 'LoggedInHomePage'
                                //         }
                                //     }
                                // );

                                // navigation.goBack();

                                // navigation.popToTop();
                                // navigation.goBack(navigation.getState().routes[0].key);
                                // navigation.navigate('SettingsScreen2');

                            }}
                            underlayColor='grey'>

                            <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{}} />


                        </TouchableHighlight>

                        <View style={{ width: 113, height: 30, }} >
                            <Image style={{ flex: 1, width: undefined, height: undefined, backgroundColor: 'transparent' }}
                                source={require('./assets/Fri-zpay-banner.png')}
                                resizeMode='center'
                            />
                        </View>
                        {/* <Image source={require('./assets/Fri-zpay-banner.png')}  width={113} height={30} style={{ marginLeft: 0, marginRight: 0 }} resizeMode='stretch' /> */}
                        {/* <Image source={require('./assets/top-bell-icon.png')} width={0} height={0} /> */}

                    </View>
                    {/* <ScrollView style={{ flex: 1 , height:'10%', width:'100%' }} contentContainerStyle={{ flexGrow: 1, height:'100%', width:'100%', alignContent:'center', flex:1}}> */}



                    <View style={{
                        flex: 0.3, justifyContent: 'space-around', flexDirection: 'column',
                        alignItems: 'flex-start', backgroundColor: 'transparent', width: '90%'
                    }}>
                        <Text style={{
                            color: '#E0E0E0',

                            // width: '85%',

                            textAlign: 'left',

                            fontFamily: 'SFPRODISPLAYMEDIUM',
                            fontSize: 22
                        }}>Edit Family</Text>

                        <View style={{
                            flex: 0.9, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent', width: '35%', height: '100%',
                        }}>
                            <TouchableHighlight
                                style={{ width: '100%', height: '100%' }}

                                onPress={() => { }}
                                underlayColor='grey'>
                                <View style={{ backgroundColor: '#484848', width: '100%', height: '100%', borderRadius: 15, justifyContent: 'space-evenly', alignItems: 'center' }}>
                                    <Image source={require('./assets/plus-icon.png')} resizeMode='center' width={110} height={130} />

                                </View>
                            </TouchableHighlight>

                        </View>
                    </View>

                    <View style={{
                        flex: 0.08, justifyContent: 'flex-start', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent', width: '90%', height: '100%',
                    }}>
                        <TouchableHighlight
                            style={{ width: '100%', height: '100%' }}

                            onPress={() => { requestContactPermission() }}
                            underlayColor='grey'>
                            <View style={{ backgroundColor: '#484848', width: '100%', height: '100%', borderRadius: 15, justifyContent: 'space-around', alignItems: 'center', flexDirection: 'row' }}>
                                <Ionicons name="search" color="#C9C9C9" size={30} />
                                <Text style={{
                                    color: '#E0E0E0',

                                    width: '85%',

                                    textAlign: 'left',

                                    fontFamily: 'SFPRODISPLAYMEDIUM',
                                    fontSize: 14
                                }}>Enter phone number or selct a contact to invite in family</Text>
                            </View>
                        </TouchableHighlight>

                    </View>

                    <View style={{ flex: 0.45, backgroundColor: '#484848', width: '90%', height: '100%', borderRadius: 15, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center', marginTop: 20 }}>
                        <FlatList
                            data={DATA}
                            renderItem={renderItem}
                            keyExtractor={item => item.id}
                            contentContainerStyle={{
                                flexGrow: 1,
                                justifyContent: 'flex-start',

                            }}
                            style={{ flex: 1, backgroundColor: 'transparent', width: '100%' }}

                        />
                    </View>


                    {/* </ScrollView> */}

                </View>

                {/* </ScrollView> */}

            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 113,
        height: 30,
        // marginVertical: 32,

    },
    imagelogo: {
        width: 46,
        height: 50,

    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    membertitle: {
        color: '#E0E0E0',
        // color: 'white',
        // textAlign: 'left',
        fontSize: 14,
        // width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    memberPhone: {
        color: '#E0E0E0',
        // color: 'white',
        // textAlign: 'left',
        fontSize: 10,
        // width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '40%',
        // height: 51,
        // backgroundColor: 'red',
        // justifyContent: 'center',
        // alignItems: 'center',
        // borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        fontFamily: 'SFPRODISPLAYMEDIUM',
        fontSize: 16,
        color: '#000000',


    }
});

export default EditFamilyPage;
