/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TouchableOpacity,
    Image, Button, TouchableHighlight, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Alert
} from 'react-native';

import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"

import { Input, CheckBox, BottomSheet, ListItem } from 'react-native-elements';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { useSelector } from 'react-redux';

const NameAndPinPage = ({ navigation }) => {

    const [isVisible, setIsVisible] = useState(false);
    const [selectedImg, setSelectedImg] = useState('')
    const [name, setName] = useState()
    const [pin, setPin] = useState('')
    const launchCameras = () => {
      let options = {
          storageOptions: {
              skipBackup: true,
              path: 'images',
          },
      };
  
      launchCamera(options, (response) => { // Use launchImageLibrary to open image gallery
          console.log('Response = ', response);
  
          if (response.didCancel) {
              console.log('User cancelled image picker');
          } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
          } else {
              // const source = { uri: response.uri };
              setSelectedImg(response.assets[0].uri)
              setIsVisible(false)
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };
  
              // console.log(source)
          }
  
      })
  }
  
  
  
  const launchImageLibrarys = () => {
      let options = {
          storageOptions: {
              skipBackup: true,
              path: 'images',
          },
      };
      launchImageLibrary(options, (response) => {
          console.log('Response = ', response);
  
          if (response.didCancel) {
              console.log('User cancelled image picker');
          } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
              alert(response.customButton);
          } else {
              console.log("check response uri", response.uri)
              setSelectedImg(response.assets[0].uri)
              setIsVisible(false)
          }
      });
  
  }
  
  const list = [
      {
          title: 'Choose from Gallery',
          onPress: () => launchImageLibrarys(),
      },
      {
          title: 'Choose from camera',
          onPress: () => launchCameras(),
  
      },
      {
          title: 'Cancel',
          containerStyle: { backgroundColor: 'red' },
          titleStyle: { color: 'white' },
          onPress: () => setIsVisible(false),
      },
  ];



    const goToRoute = async () => {
        // 
        if (await name == '' || pin == '' || pin.length < 4) {
            return Alert.alert('Please fill valid name and pin')
        }
        else {
            navigation.navigate('PanCardDetailsPage', {
                name: name,
                pin: pin
            })
        }
    }
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>

                <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-between' }} >
                    <View style={{ flex: 0.8, backgroundColor: '#060606', justifyContent: 'flex-start' }}>

                        <View style={{
                            flex: 0.3, justifyContent: 'space-evenly', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent'
                        }}>
                            <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                            <Image source={require('./assets/Frizpay.png')} style={styles.image} />
                        </View>

                        <View style={{
                            flex: 0.2, justifyContent: 'center', flexDirection: 'row',
                            alignItems: 'center', backgroundColor: 'transparent'
                        }}>
                             <TouchableOpacity onPress={()=>setIsVisible(true)}>
                          
                          <Image source={selectedImg==''? require('./assets/profile-signup-default.png'):{uri:selectedImg}}
                              style={{ backgroundColor: 'transparent',borderRadius:selectedImg==''?0: 40 }} width={54} height={54} resizeMode={'cover'} />
</TouchableOpacity>

                        </View>




                        <View style={{
                            flex: 0.5, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent',

                        }}>

                            <TextInput
                                style={{
                                    color: '#818181', fontSize: 17, backgroundColor: 'transparent',
                                    // textAlign: 'center',
                                    fontFamily: 'SFProText-Regular',
                                    marginBottom: 0,
                                    paddingBottom: 0,
                                    width: '90%',
                                    marginTop:hp('5%')

                                }}
                                onChangeText={(text) => setName(text)}

                                // value='9999999999'
                                placeholder="Enter your full name"
                                placeholderTextColor="#818181"
                                keyboardType="default"
                            />

                            <View style={{ width: '90%', height: 1, backgroundColor: '#B4B4B4',marginTop:hp('0.5%') }}></View>



                            <TextInput
                                style={{
                                    color: '#818181', fontSize: 17, backgroundColor: 'transparent',
                                    // textAlign: 'center',
                                    fontFamily: 'SFProText-Regular',
                                    marginTop: hp('12%'),
                                    // marginBottom: 0,
                                    paddingBottom: 0,
                                    width: '90%'
                                }}
                                onChangeText={(text) => setPin(text)}
                                // value='9999999999'
                                placeholder="Enter pin"
                                placeholderTextColor="#818181"
                                keyboardType="numeric"
                                maxLength={4}
                                secureTextEntry={true}
                            />




                            <View style={{ width: '90%', height: 1, backgroundColor: '#B4B4B4' ,marginTop:hp('0.5%')}}></View>



                        </View>






                    </View>


                    <View style={{
                        flex: 0.18, justifyContent: 'center', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent'
                    }}>
                        <TouchableHighlight
                            style={styles.doneButton}
                            onPress={() => goToRoute()}
                            underlayColor='#E8EB59'>
                            <Text style={styles.submitTitle}>Continue</Text>
                        </TouchableHighlight>

                    </View>
                </View>
            </ScrollView>
            <BottomSheet
                isVisible={isVisible}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                {list.map((l, i) => (
                    <ListItem key={i} containerStyle={l.containerStyle} onPress={l.onPress}>
                        <ListItem.Content>
                            <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
                        </ListItem.Content>
                    </ListItem>
                ))}
            </BottomSheet>
        </SafeAreaView>

    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 90,
        height: 25,
        // marginTop: 10,
resizeMode:'contain'
    },
    imagelogo: {
        width: 60,
        height: 70,
        // marginTop: 32,
        // marginBottom: 20
    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '85%',
        height: 51,
        backgroundColor: '#E8EB59',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 15,
        color: '#000000',
        left:wp('1%')


    },
    resendotp: {
        textAlign: 'center',

        fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 17,
        color: '#E0E0E0',


    },
    borderStyleBase: {
        width: 30,
        height: 45
    },

    borderStyleHighLighted: {
        borderColor: "#03DAC6",
    },

    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
    },

    underlineStyleHighLighted: {
        borderColor: "#03DAC6",
    },
});

export default NameAndPinPage;
