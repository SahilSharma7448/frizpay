/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid,TouchableOpacity
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome'

 const NotificationList = ({ navigation }) => {
    const [family,setFamily]=useState(false)
     const [activeTab, setActiveTab] = useState('0')
     const [showBtn, setBtn] = useState(false)
 
     return (
         <SafeAreaView style={{ flex: 1 }}>

             <View style={{ flex: 1, backgroundColor: '#060606', }}>

                 {/* //paddingBottom:450 */}
 
                 <View style={{
                         flex: 0.1, justifyContent: 'space-around', flexDirection: 'row',
                         alignItems: 'center', backgroundColor: 'transparent', width: '100%',
                        //  marginTop:30?
                     }}>
 
                         <TouchableHighlight
                             style={{ width: '10%', height: '100%', backgroundColor: 'transparent', justifyContent: 'center', marginLeft: -120, marginRight: 0, position: 'absolute' }}
 
                             onPress={() => {
                                 // navigation.navigate("LoggedInHomePage")
                                 // console.log(navigation.getParent());
                                 // navigation.toggleDrawer();
                                 // navigationContext.navigate('Notifications');
                                 // navigationContext.navigate('HomeTabPage',
                                 //     {
                                 //         screen: 'Home',
                                 //         // initial: false,
                                 //         params: {
                                 //             screen: 'LoggedInHomePage'
                                 //         }
                                 //     }
                                 // );
 
                                 // navigation.goBack();
 
                                 // navigation.popToTop();
                                 // navigation.goBack(navigation.getState().routes[0].key);
                                 // navigation.navigate('SettingsScreen2');
 navigation.goBack()
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{}} />
 
 
                         </TouchableHighlight>
 
                         <View style={{ width: 113, height: 30, }} >
                             <Image style={{ flex: 1, width: undefined, height: undefined, backgroundColor: 'transparent' }}
                                 source={require('./assets/Frizpay.png')}
                                 resizeMode='center'
                             />
                         </View>
                         {/* <Image source={require('./assets/Fri-zpay-banner.png')}  width={113} height={30} style={{ marginLeft: 0, marginRight: 0 }} resizeMode='stretch' /> */}
                         {/* <Image source={require('./assets/top-bell-icon.png')} width={0} height={0} /> */}
 
                     </View>
                     <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>


                     <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                            //  textAlign: 'left',
 
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontSize: 22,
                             width:wp('90%'),
                             alignItems:'center',
                             alignSelf:'center',
                             marginTop:hp('5%'),
                             fontFamily:'SFPRODISPLAYBOLD'

                         }}>Notifications </Text>


<Text style={{width:wp('90%'),alignSelf:'center',marginTop:hp('5%'),color:'#909090',fontSize:15,fontFamily:'SFProText-Semibold'}}>Today</Text>



      <View style={{ flexDirection: 'row', alignSelf: 'center', width: wp('90%'),marginTop:hp('3%') }}>

<Image
  source={require('./assets/ridhima.png')} style={{ height: hp("5%"), width: wp("10%"), borderRadius:40 }}
  // resizeMode={FastImage.resizeMode.contain}
  resizeMode={'contain'}



/>
<View style={{ width: wp('50%'),marginLeft:wp('3%') }}>
  <Text style={{ fontSize: 13, color: 'white',bottom:3,fontFamily:'SFProText-Semibold' }}>Event request from Shubham </Text>
  <Text style={{ fontSize: 12, color: '#B2B2B2', marginTop: hp('2%') }}>Today 4:30 PM</Text>

                         </View>
                         <TouchableOpacity onPress={() => setBtn(true)}>

<View style={{ height: hp('5%'), width: wp('30%'),  backgroundColor: '#E8EB59', borderRadius: 9, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{fontFamily:'SFProText-Bold',fontSize:12,color:'black',bottom:hp('0.3%')}}>Join and pay</Text>
          </View>
                         </TouchableOpacity>
</View>



<View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>








<View style={{ flexDirection: 'row', alignSelf: 'center', width: wp('90%'),marginTop:hp('3%') }}>

<Image
  source={require('./assets/frizpay-logo.png')} style={{ height: hp("5%"), width: wp("10%"),borderRadius:40 }}
  // resizeMode={FastImage.resizeMode.contain}
  resizeMode={'contain'}



/>
<View style={{ width: wp('70%'),marginLeft:wp('3%') }}>
  <Text style={{ fontSize: 13, color: 'white',fontFamily:'SFProText-Semibold'  }}>Get your KYC done in 1 min free </Text>
  <Text style={{ fontSize: 12, color: '#B2B2B2', marginTop: hp('2%') }}>Today 4:30 PM</Text>

</View>
{/* <View style={{ height: hp('4%'), width: wp('30%'),  backgroundColor: '#E8EB59', borderRadius: 6, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{fontWeight:'800',fontSize:12,color:'black',}}>Join and pay</Text>
          </View> */}

</View>



<View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>








<Text style={{width:wp('90%'),alignSelf:'center',marginTop:hp('5%'),color:'#909090',fontSize:15,fontFamily:'SFProText-Semibold'}}>25 Aug 2021</Text>

      <View style={{marginBottom:hp('10%')}}>


      <View style={{ flexDirection: 'row', alignSelf: 'center', width: wp('90%'),marginTop:hp('3%') }}>

<Image
  source={require('./assets/ridhima.png')} style={{ height: hp("5%"), width: wp("10%"),borderRadius:40 }}
  // resizeMode={FastImage.resizeMode.contain}
  resizeMode={'contain'}



/>
<View style={{ width: wp('50%'),marginLeft:wp('3%') }}>
  <Text style={{ fontSize: 13, color: 'white',bottom:3,fontFamily:'SFProText-Semibold'  }}>Event request from Shubham </Text>
  <Text style={{ fontSize: 12, color: '#B2B2B2', marginTop: hp('2%') }}>Today 4:30 PM</Text>

                             </View>
                             <TouchableOpacity onPress={() => setBtn(true)}>

<View style={{ height: hp('5%'), width: wp('30%'),  backgroundColor: '#E8EB59', borderRadius: 9, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{fontFamily:'SFProText-Bold',fontSize:12,color:'black',bottom:hp('0.3%')}}>Join and pay</Text>
          </View>
                             </TouchableOpacity>
</View>



<View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>








<View style={{ flexDirection: 'row', alignSelf: 'center', width: wp('90%'),marginTop:hp('3%') }}>

<Image
  source={require('./assets/upi.png')} style={{ height: hp("5%"), width: wp("10%"),borderRadius:40 }}
  // resizeMode={FastImage.resizeMode.contain}
  resizeMode={'contain'}



/>
<View style={{ width: wp('50%'),marginLeft:wp('3%') }}>
  <Text style={{ fontSize: 13, color: 'white',fontFamily:'SFProText-Semibold'  }}> 400 requested by  shubham </Text>
  <Text style={{ fontSize: 12, color: '#B2B2B2', marginTop: hp('2%'),fontFamily:'SFProText-Semibold' }}>Today 4:30 PM</Text>

                             </View>
                             <TouchableOpacity onPress={() => setBtn(true)}>

<View style={{ height: hp('5%'), width: wp('30%'),  backgroundColor: '#E8EB59', borderRadius: 9, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{fontFamily:'SFProText-Bold', fontSize:12,color:'black',bottom:hp('0.3%')}}>Confirm</Text>
          </View>
                             </TouchableOpacity>

</View>



<View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('82%'), alignSelf: 'flex-end',marginTop:hp('2%') }}></View>

















</View>

         
         
      </ScrollView>
         
             </View>
             







             <View>
                 <Modal isVisible={showBtn}>
                     <View style={{ height: hp('50%'), backgroundColor: 'black', width: wp('90%'), borderColor: '#E8EB59', borderWidth: 1, borderRadius: 10, alignSelf: 'center', }}>
                         <View style={{ marginTop: hp('1%'), width: wp('80%'), marginLeft: wp('3%'), alignSelf: 'center', flexDirection: 'row', alignItems: 'center', }}>

                             <Icon name={'angle-left'} size={35} color={'#E8EB59'} style={{}}></Icon>
                             <Text style={{
                                 color: '#FFFFFF',




                                 fontFamily: 'SFPRODISPLAYBOLD',
                                 fontSize: 28,
                                 marginLeft: wp('5%')
                             }}>Shubham's Birthday</Text>

                         </View>

                         <View style={{ width: wp('80%'), flexDirection: 'row', alignSelf: 'center', marginTop: hp('1%'), marginBottom: hp('1%'), marginLeft: wp('11%') }}>
                             <View>
                                 <Image source={require('./assets/ridhima.png')} style={{ height: 35, width: 35, borderRadius: 50 }} resizeMode="contain" />
                             </View>
                             <View style={{ marginLeft: wp('9%') }}>
                                 <Image source={require('./assets/ridhima.png')} style={{ height: 35, width: 35, borderRadius: 50 }} resizeMode="contain" />
                             </View>


                             <View style={{ marginLeft: wp('9%') }}>
                                 <Image source={require('./assets/ridhima.png')} style={{ height: 35, width: 35, borderRadius: 50 }} resizeMode="contain" />
                             </View>
                             <View style={{ marginLeft: wp('9%') }}>
                                 <Image source={require('./assets/2.png')} style={{ height: 40, width: 40, borderRadius: 50 }} resizeMode="contain" />
                             </View>
                         </View>

                         <Text style={{
                             color: '#FFFFFF',




                             fontFamily: 'SFProText-Regular',
                             fontSize: 13,
                             textAlign: 'center',
                             marginTop: hp('2%')
                         }}>Total Amount Gathered</Text>


                         <Text style={{
                             textAlign: 'center',
                             fontSize: 23, color: 'white', fontFamily: 'SFProText-Semibold'
                         }}>₹2000</Text>


                         <Text style={{
                             color: '#FFFFFF',




                             fontFamily: 'SFProText-Regular',
                             fontSize: 15,
                             textAlign: 'center',
                             marginTop: hp('5%')
                         }}>Your share</Text>


                         <Text style={{
                             textAlign: 'center',
                             fontSize: 38, color: 'white', fontFamily: 'SFProText-Semibold'
                         }}>₹400</Text>
                         <TouchableOpacity onPress={() => setBtn(false)}>
                             <View style={{
                                 height: hp('5%'),
                                 alignSelf: 'center',
                                 marginTop: hp('4%'),
                                 width: wp('30%'), backgroundColor: '#E8EB59', borderRadius: 9, alignItems: 'center', justifyContent: 'center'
                             }}>
                                 <Text style={{ fontFamily: 'SFProText-Bold', fontSize: 12, color: 'black', bottom: hp('0.3%') }}>Join and pay</Text>
                             </View>


                         </TouchableOpacity>




                     </View>
                 </Modal>
             </View>






         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default NotificationList;
 