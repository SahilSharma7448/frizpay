import * as React from 'react';
import { Text, View , Button} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LoggedInHomePage from './LoggedInHomePage';
import EditFamilyPage from './EditFamilyPage';
import { useNavigation } from '@react-navigation/native';
import NotificationAlert from "./NotificationAlert"


// import LeftDrawerPage from './LeftDrawerPage'


function SettingsScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Settings!</Text>
        </View>
    );
}


function SettingsScreen2({navigation}) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Settings2!</Text>
            <Button onPress={() => useNavigation().goBack()} title="Go back edit" />
        </View>
    );
}

const Tab = createBottomTabNavigator();

const HomeStack = createNativeStackNavigator();

function HomeStackScreen() {
    return (
        <HomeStack.Navigator  initialRouteName="LoggedInHomePage" screenOptions={{
            headerShown: false,
    
          }}>
            <HomeStack.Screen name="LoggedInHomePage" component={LoggedInHomePage} />
            <HomeStack.Screen name="EditFamilyPage" component={EditFamilyPage} />
            <HomeStack.Screen name="SettingsScreen2" component={SettingsScreen2} />
        <HomeStack.Screen name="NotificationAlert" component={NotificationAlert} />

        </HomeStack.Navigator>);
}

export default function HomeTabPage() {

    return (

        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    if (route.name === 'Home') {
                        iconName = focused
                            ? 'home-sharp'
                            : 'home-outline';
                    } else if (route.name === 'Settings') {
                        iconName = focused ? 'add' : 'add-circle';
                    }

                    // You can return any component that you like here!
                    return <Ionicons name={iconName} size={size} color={color} />;
                },
                tabBarActiveTintColor: 'white',
                tabBarInactiveTintColor: 'black',
                initialRouteName: "Home",
                headerShown: false,
                tabBarActiveBackgroundColor: 'black',

                tabBarStyle: {
                    position: 'absolute',
                    borderTopColor: 'transparent',
                    borderTopWidth: 0
                },
            })}


        >
            <Tab.Screen name="Home" component={HomeStackScreen} style={{ backgroundColor: 'black' }} />
            {/* <Tab.Screen name="Settings" component={SettingsScreen} /> */}
        </Tab.Navigator>
    );
}