/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid,TouchableOpacity
 } from 'react-native';
 import Entypo from 'react-native-vector-icons/FontAwesome';
import MultiSlider from "@ptomasroos/react-native-multi-slider"
 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
 
 const Learning = ({ navigation }) => {
    const [family,setFamily]=useState(false)
    const [activeTab, setActiveTab] = useState('0')
 
     return (
         <SafeAreaView style={{ flex: 1 }}>

             <View style={{ flex: 1, backgroundColor: '#060606', }}>
<ScrollView>

                 {/* <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}> */}
                 {/* //paddingBottom:450 */}
 
                 <View style={{
                         flex: 0.1, justifyContent: 'space-around', flexDirection: 'row',
                         alignItems: 'center', backgroundColor: 'transparent', width: '100%'
                     }}>
 
                         <TouchableHighlight
                             style={{ width: '10%', height: '100%', backgroundColor: 'transparent', justifyContent: 'center', marginLeft: -120, marginRight: 0, position: 'absolute' }}
 
                             onPress={() => {
                                 // navigation.navigate("LoggedInHomePage")
                                 // console.log(navigation.getParent());
                                 // navigation.toggleDrawer();
                                 // navigationContext.navigate('Notifications');
                                 // navigationContext.navigate('HomeTabPage',
                                 //     {
                                 //         screen: 'Home',
                                 //         // initial: false,
                                 //         params: {
                                 //             screen: 'LoggedInHomePage'
                                 //         }
                                 //     }
                                 // );
 
                                 // navigation.goBack();
 
                                 // navigation.popToTop();
                                 // navigation.goBack(navigation.getState().routes[0].key);
                                 // navigation.navigate('SettingsScreen2');
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{}} />
 
 
                         </TouchableHighlight>
 
                         <View style={{ width: 113, height: 30, }} >
                             <Image style={{ flex: 1, width: undefined, height: undefined, backgroundColor: 'transparent' }}
                                 source={require('./assets/Fri-zpay-banner.png')}
                                 resizeMode='center'
                             />
                         </View>
                         {/* <Image source={require('./assets/Fri-zpay-banner.png')}  width={113} height={30} style={{ marginLeft: 0, marginRight: 0 }} resizeMode='stretch' /> */}
                         {/* <Image source={require('./assets/top-bell-icon.png')} width={0} height={0} /> */}
 
                     </View>


                     <Text style={{
                             color: '#E0E0E0',
 
                             // width: '85%',
 
                            //  textAlign: 'left',
 
                            //  fontFamily: 'SFPRODISPLAYMEDIUM',
                             fontSize: 22,
                             width:wp('90%'),
                             alignItems:'center',
                             alignSelf:'center',
                             marginTop:hp('5%'),
                             fontWeight:'700'

                         }}>Learning </Text>


<Text style={{width:wp('90%'),alignSelf:'center',marginTop:hp('5%'),color:'#C0C0C0',fontSize:17}}>Basics of Personal finance</Text>












<FlatList  
                    data={[  
                        {key: 'Android'},{key: 'iOS'}, {key: 'Java'},{key: 'Swift'},  
                       
                    ]}  
                    horizontal
                    renderItem={({item}) =>  
<View style={{ height: hp('20%'), width: wp('55%'), backgroundColor:'#484848',borderRadius:10,marginLeft:wp('5%'),marginTop:hp('3%') }}>
        <View style={{ bottom: 0, position: 'absolute', }}>
          <View style={{ flexDirection: 'row', top: hp('5%') ,}}>
            <Entypo name={'play'} size={20} color={'white'} style={{marginLeft:wp('2%'),alignSelf:'center'}}></Entypo>
          <View>
            <Text style={{ width: wp('30%'), marginLeft:wp('3%'),color:'#C0C0C0',fontSize:12 }}>Save first spend late</Text>
            <Text style={{ width: wp('30%'), marginLeft:wp('3%'),color:'#C0C0C0',fontSize:12 }}>5:20 mins</Text>
           
            </View>
          </View>
          <MultiSlider

            values={[3]}
            customMarker={() => {
              return <Text></Text>
            }}
            sliderLength={190}
            containerStyle={{ top: 22 }}
            trackStyle={{ height: 8,backgroundColor:'#484848', }}
            selectedStyle={{backgroundColor:'#D39800'}}
          />
        </View>
      </View>

                      }  
                   
                />  





<View style={{width:wp('90%'),justifyContent:'space-between',flexDirection:'row',alignSelf:'center',alignItems:'center'}}>

<Text style={{alignSelf:'center',marginTop:hp('5%'),color:'#C0C0C0',fontSize:17,top:hp('1%')}}>Friz learn first course</Text>

<View style={{ height: hp('4%'), width: wp('30%'),  backgroundColor: '#E8EB59', borderRadius: 6, alignItems: 'center', justifyContent: 'center',top:hp('3%') }}>
            <Text style={{fontWeight:'800',fontSize:12,color:'black',}}>Assign as Task </Text>
            </View>
          </View> 










<FlatList  
                    data={[  
                        {key: 'Android'},{key: 'iOS'}, {key: 'Java'},{key: 'Swift'},  
                       
                    ]}  
                    horizontal
                    renderItem={({item}) =>  
<View style={{ height: hp('20%'), width: wp('55%'), backgroundColor:'#484848',borderRadius:10,marginLeft:wp('5%'),marginTop:hp('3%') }}>
        <View style={{ bottom: 0, position: 'absolute', }}>
          <View style={{ flexDirection: 'row', top: hp('5%') ,}}>
            <Entypo name={'play'} size={20} color={'white'} style={{marginLeft:wp('2%'),alignSelf:'center'}}></Entypo>
          <View>
            <Text style={{ width: wp('30%'), marginLeft:wp('3%'),color:'#C0C0C0',fontSize:12 }}>Save first spend late</Text>
            <Text style={{ width: wp('30%'), marginLeft:wp('3%'),color:'#C0C0C0',fontSize:12 }}>5:20 mins</Text>
           
            </View>
          </View>
          <MultiSlider

            values={[3]}
            customMarker={() => {
              return <Text></Text>
            }}
            sliderLength={190}
            containerStyle={{ top: 22 }}
            trackStyle={{ height: 8,backgroundColor:'#484848', }}
            selectedStyle={{backgroundColor:'#D39800'}}
          />
        </View>
      </View>

                      }  
                   
                />  





<View style={{width:wp('90%'),justifyContent:'space-between',flexDirection:'row',alignSelf:'center',alignItems:'center'}}>

<Text style={{alignSelf:'center',marginTop:hp('5%'),color:'#C0C0C0',fontSize:17,textAlign:'center',top:hp('1%')}}>Friz learn first course</Text>

<View style={{ height: hp('4%'), width: wp('30%'),  backgroundColor: '#E8EB59', borderRadius: 6, alignItems: 'center', justifyContent: 'center',top:hp('3%') }}>
            <Text style={{fontWeight:'800',fontSize:12,color:'black',}}>Gift a course </Text>
            </View>
          </View> 





          <Image source={require('./assets/buys.png')} width={100} height={100} style={{marginTop:hp('5%'),marginBottom:hp('10%'),marginLeft:hp('2%')}} />










         
         
      </ScrollView>
         
             </View>
             














         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default Learning;
 