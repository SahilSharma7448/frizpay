/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image, Button, TouchableHighlight, TextInput, TouchableWithoutFeedback,
    Keyboard
} from 'react-native';



const AadharNumberPage = ({ navigation }) => {


    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-evenly' }}>
                    <View style={{ flex: 0.7, backgroundColor: '#060606', justifyContent: 'flex-start' }}>

                        <View style={{
                            flex: 0.25, justifyContent: 'space-evenly', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent'
                        }}>
                            <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                            <Image source={require('./assets/Fri-zpay-banner.png')} style={styles.image} />
                        </View>

                        <View style={{
                            flex: 0.04, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent',
                        }}>
                            <Text style={{
                                color: '#E0E0E0',

                                width: '90%',


                                textAlign: 'left',

                                fontFamily: 'SFPRODISPLAYMEDIUM',
                                fontSize: 32
                            }}>Enter your Aadhar Number</Text>

                            <Text style={{
                                color: '#E0E0E0',

                                width: '90%',

                                textAlign: 'left',

                                fontFamily: 'SFProText-Regular',
                                fontSize: 12
                            }}>You will recieve a OTP on mobile number registered with your adhaar.</Text>



                        </View>




                        <View style={{
                            flex: 0.5, justifyContent: 'flex-start', flexDirection: 'column',
                            alignItems: 'center', backgroundColor: 'transparent', alignContent: 'space-between', alignSelf: 'auto'
                        }}>


                            <TextInput
                                style={{
                                    color: 'white', fontSize: 30, backgroundColor: 'transparent',
                                    textAlign: 'center',
                                    fontFamily: 'SFProText-Regular',
                                    marginBottom: 0,
                                    paddingBottom: 0,
                                }}
                                // onChangeText={onChangeNumber}
                                value='9999-9999-9999'
                                placeholder="9999-9999-9999"
                                keyboardType="numeric"
                            />

                            <View style={{
                                flex: 0.2,
                                width: '100%', height: 1, backgroundColor: 'transparent',
                                alignItems: 'center'
                            }}>
                                <View style={{ width: '95%', height: 1, backgroundColor: '#B4B4B4' }}></View>
                            </View>

                        </View>






                    </View>
                    <View style={{
                        flex: 0.2, justifyContent: 'space-around', flexDirection: 'column',
                        alignItems: 'center', backgroundColor: 'transparent'
                    }}>
                        <Text style={{
                            color: '#E0E0E0',

                            width: '85%',

                            textAlign: 'left',

                            fontFamily: 'SFProText-Regular',
                            fontSize: 12
                        }}>By clicking continue you agree to share your information with our partner bank.

                        </Text>

                        <TouchableHighlight
                            style={styles.doneButton}
                            onPress={() => navigation.navigate('AadharOTPPage')}
                            underlayColor='#E8EB59'>
                            <Text style={styles.submitTitle}>Continue</Text>
                        </TouchableHighlight>

                    </View>

                </View>
            </ScrollView>
        </SafeAreaView>

    );
};

const styles = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'blue',
    },
    image: {
        width: 113,
        height: 30,


    },
    imagelogo: {
        width: 46,
        height: 50,
        // marginTop: 32,
        // marginBottom: 20
    },
    parentlogo: {
        width: "50%",
        height: "65%",
    },
    teenagerlogo: {
        width: "40%",
        height: "75%",
    },
    text: {
        color: '#E0E0E0',
        textAlign: 'center',
        fontSize: 25,
        fontFamily: 'SF-Pro-Rounded-Semibold',
    },
    title: {
        color: '#E0E0E0',
        // color: 'white',
        textAlign: 'center',
        fontSize: 25,
        width: '90%',
        fontFamily: 'SF-Pro-Rounded-Semibold',
        marginBottom: 0
    },
    doneButton: {
        width: '85%',
        height: 51,
        backgroundColor: '#E8EB59',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
    },
    submitTitle: {
        textAlign: 'center',

        fontFamily: 'SF-Pro-Rounded-Semibold',
        fontSize: 17,
        color: '#000000',


    }
});

export default AadharNumberPage;
