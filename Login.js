/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     Alert,
     View,
     TouchableOpacity,
     Image, Button, TouchableHighlight, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard
 } from 'react-native';
 
 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
import {useDispatch} from 'react-redux'
 import {Login_Api} from './App/Action/Auth'
 import * as Utility from './App/utility'
 import Loader from './App/Components/Loader'
 const Login = ({ navigation }) => {
     const dispatch=useDispatch()
     const [loading,setLoading]=useState(false)
     const [mobile,setMobile]=useState('')
 const [pin,setPin]=useState('')
    const goToRoute=async()=>{
        if(await mobile.length<10){
            return Alert.alert('Please fill valid mobile number')
        }
        else if(await pin.length<4){
            return Alert.alert('Please fill valid pin')
        }
        else{
            setLoading(true)
let type=await Utility.getFromLocalStorge('gender')
let res=await dispatch(Login_Api(mobile,pin,type))
if(res.status=='Success'){
    setLoading(false)
    if(type=='child'){

    navigation.navigate('SideDrawerChild')
}
else{
    navigation.navigate('DrawerPage')

}
}
else{
    setLoading(false)

    return Alert.alert('Wrong credential')
}
        }

    }
     return (
         <SafeAreaView style={{ flex: 1 }}>
             <Loader isLoading={loading}></Loader>
             <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
 
                 <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'space-between' }} >
                     <View style={{ flex: 0.8, backgroundColor: '#060606', justifyContent: 'flex-start' }}>
 
                         <View style={{
                             flex: 0.3, justifyContent: 'space-evenly', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent'
                         }}>
                             <Image source={require('./assets/frizpay-logo.png')} style={styles.imagelogo} />
                             <Image source={require('./assets/Frizpay.png')} style={styles.image} />
                         </View>
 
                        
 
 
 
 
                         <View style={{
                             flex: 0.5, justifyContent: 'flex-start', flexDirection: 'column',
                             alignItems: 'center', backgroundColor: 'transparent',
 
                         }}>
 
                             <TextInput
                                 style={{
                                     color: '#818181', fontSize: 17, backgroundColor: 'transparent',
                                     // textAlign: 'center',
                                     fontFamily: 'SFProText-Regular',
                                     marginBottom: 0,
                                     paddingBottom: 0,
                                     width: '90%',
                                     marginTop:hp('5%')
 
                                 }}
                                 onChangeText={(text)=>setMobile(text)}
                                 // value='9999999999'
                                 placeholder="Enter your mobile number"
                                 placeholderTextColor="#818181"
                                 keyboardType='number-pad'
                                 maxLength={10}
                             />
 
                             <View style={{ width: '90%', height: 1, backgroundColor: '#B4B4B4',marginTop:hp('0.5%') }}></View>
 
 
 
                             <TextInput
                                 style={{
                                     color: '#818181', fontSize: 17, backgroundColor: 'transparent',
                                     // textAlign: 'center',
                                     fontFamily: 'SFProText-Regular',
                                     marginTop: hp('12%'),
                                     // marginBottom: 0,
                                     paddingBottom: 0,
                                     width: '90%'
                                 }}
                                 // onChangeText={onChangeNumber}
                                 // value='9999999999'
                                 onChangeText={(text)=>setPin(text)}

                                 placeholder="Enter pin"
                                 placeholderTextColor="#818181"
                                 keyboardType="numeric"
                                 maxLength={4}
                                 secureTextEntry={true}
                             />
 
 
 
 
                             <View style={{ width: '90%', height: 1, backgroundColor: '#B4B4B4' ,marginTop:hp('0.5%')}}></View>
 
 
 
                         </View>
 
 
 
 
 
 
                     </View>
 
 
                     <View style={{
                         flex: 0.18, justifyContent: 'center', flexDirection: 'column',
                         alignItems: 'center', backgroundColor: 'transparent'
                     }}>
                         <TouchableHighlight
                             style={styles.doneButton}
                             onPress={() => goToRoute()}
                             underlayColor='#E8EB59'>
                             <Text style={styles.submitTitle}>Continue</Text>
                         </TouchableHighlight>
 
                     </View>
                     <Text style={{color:'white',fontFamily:'SFProText-Semibold',textAlign:'center',textDecorationLine:'underline',marginBottom:hp('5%')}} onPress={()=>navigation.navigate('PhoneNumberPage')}>Create an Account?</Text>
                 </View>
             </ScrollView>
             
         </SafeAreaView>
 
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 90,
         height: 25,
         // marginTop: 10,
 resizeMode:'contain'
     },
     imagelogo: {
         width: 60,
         height: 70,
         // marginTop: 32,
         // marginBottom: 20
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '85%',
         height: 51,
         backgroundColor: '#E8EB59',
         justifyContent: 'center',
         alignItems: 'center',
         borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SF-Pro-Rounded-Semibold',
         fontSize: 15,
         color: '#000000',
         left:wp('1%')
 
 
     },
     resendotp: {
         textAlign: 'center',
 
         fontFamily: 'SF-Pro-Rounded-Semibold',
         fontSize: 17,
         color: '#E0E0E0',
 
 
     },
     borderStyleBase: {
         width: 30,
         height: 45
     },
 
     borderStyleHighLighted: {
         borderColor: "#03DAC6",
     },
 
     underlineStyleBase: {
         width: 30,
         height: 45,
         borderWidth: 0,
         borderBottomWidth: 1,
     },
 
     underlineStyleHighLighted: {
         borderColor: "#03DAC6",
     },
 });
 
 export default Login;
 