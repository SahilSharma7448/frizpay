/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/Ionicons';
 import { selectContactPhone } from 'react-native-select-contact';
 import { useNavigation } from '@react-navigation/native';
import { heightPercentageToDP as hp,widthPercentageToDP as wp } from './App/utility';
import Icon from "react-native-vector-icons/FontAwesome"
import {Input} from "react-native-elements"
 const screenHeight = Dimensions.get('window').height
 const screenWidth = Dimensions.get('window').width
 
 const EditProfile = ({ navigation }) => {
 
     const navigationContext = useNavigation();
 
     return (
         <SafeAreaView style={{ flex: 1 }}>
             <View style={{ flex: 1, backgroundColor: '#060606', height: screenHeight }}>
                 {/* <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}> */}
                 {/* //paddingBottom:450 */}
                 <TouchableOpacity
                             style={{ backgroundColor: 'transparent',   }}
 
                             onPress={() => {
                                
 navigation.goBack()
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{}} />
 
 
                         </TouchableOpacity>










                 <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'flex-start', flexDirection: 'column', alignItems: 'center', alignContent: 'space-between', }}>
 
                   
                     {/* <ScrollView style={{ flex: 1 , height:'10%', width:'100%' }} contentContainerStyle={{ flexGrow: 1, height:'100%', width:'100%', alignContent:'center', flex:1}}> */}
 
 
                    



<ScrollView showsVerticalScrollIndicator={false}>




                 <View style={{ alignSelf: 'center', }}>
         
          
                 <View style={{}}>
            {/* <Image source={require('./assets/avtar.png')} style={{height:60,width:60}} resizeMode="contain" /> */}
            <Image source={require('./assets/shubham.png')} style={{height:60,width:60,alignSelf:'center',marginTop:hp('2%'),borderRadius:60}} resizeMode="contain" />

              <Text style={{ marginTop: hp('1%'),color:'white',textAlign:"center" }}>Shubham sharma</Text>

            </View>
        </View>
<View style={{width:wp('75%'),alignSelf:'center',alignItems:'flex-start',marginTop:hp('2%')}}>

<Text style={{color:'#C0C0C0',marginTop:hp('2%'),fontFamily:'SF-Pro-Rounded-Semibold'}}>First Name</Text>
<Input
            placeholder={'Dummy'}
           
            inputContainerStyle={{
              borderColor:'#060606',
              borderWidth: 0,

              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              top: 10,
              
              
            }}
            inputStyle={{color:'#C0C0C0'}}
            labelStyle={{color:'white'}}
            containerStyle={{
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              height: hp('5%'),
              backgroundColor: '#060606',
              width: wp('80%'),
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              borderRadius: 10,
              marginTop:hp('1%'),
              borderBottomColor:'grey',
              borderBottomWidth:0.5,
              right:5

            }}
            rightIcon={
              <TouchableOpacity>
 <Icon
                name={'chevron-right'}
                size={17}
                color={'#E8EB59'}
                style={{ alignSelf: 'baseline', marginRight: 5 }}
              />
              </TouchableOpacity>
            }
          /> 





<Text style={{color:'#C0C0C0',marginTop:hp('2%'),fontFamily:'SF-Pro-Rounded-Semibold'}}>Last Name</Text>
<Input
            placeholder={'Sharma'}
           
            inputContainerStyle={{
              borderColor:'#060606',
              borderWidth: 0,

              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              top: 10,
              
            }}
            inputStyle={{color:'#C0C0C0'}}

            labelStyle={{fontSize:10}}
            containerStyle={{
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              height: hp('5%'),
              backgroundColor: '#060606',
              width: wp('80%'),
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              borderRadius: 10,
              marginTop:hp('1%'),
              borderBottomColor:'grey',
              borderBottomWidth:0.5,
              right:5

            }}
            rightIcon={
              <TouchableOpacity>
 <Icon
                name={'chevron-right'}
                size={17}
                color={'#E8EB59'}
                style={{ alignSelf: 'baseline', marginRight: 5 }}
              />
              </TouchableOpacity>
            }
          /> 










<Text style={{color:'#C0C0C0',marginTop:hp('2%'),fontFamily:'SF-Pro-Rounded-Semibold'}}>Phone Number</Text>
<Input
            placeholder={'+911234556666'}
           
            inputContainerStyle={{
              borderColor:'#060606',
              borderWidth: 0,

              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              top: 10,
              
            }}
            inputStyle={{color:'#C0C0C0'}}

            labelStyle={{fontSize:10}}
            containerStyle={{
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              height: hp('5%'),
              backgroundColor: '#060606',
              width: wp('80%'),
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              borderRadius: 10,
              marginTop:hp('1%'),
              borderBottomColor:'grey',
              borderBottomWidth:0.5,
              right:5

            }}
            rightIcon={
              <TouchableOpacity>

              </TouchableOpacity>
            }
          /> 
          










<Text style={{color:'#C0C0C0',marginTop:hp('2%'),fontFamily:'SF-Pro-Rounded-Semibold'}}>KYC status</Text>
<Input
            placeholder={'Pending'}
           
            inputContainerStyle={{
              borderColor:'#060606',
              borderWidth: 0,

              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              top: 10,
              
            }}
            inputStyle={{color:'#C0C0C0'}}

            labelStyle={{fontSize:10}}
            containerStyle={{
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              height: hp('5%'),
              backgroundColor: '#060606',
              width: wp('80%'),
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              borderRadius: 10,
              marginTop:hp('1%'),
              borderBottomColor:'grey',
              borderBottomWidth:1,
              right:5

            }}
            rightIcon={
              <TouchableOpacity>

              </TouchableOpacity>
            }
          /> 
          












<Text style={{color:'#C0C0C0',marginTop:hp('2%'),fontFamily:'SF-Pro-Rounded-Semibold'}}>Date of Birth</Text>
<Input
            placeholder={'10-11-0000'}
           
            inputContainerStyle={{
              borderColor:'#060606',
              borderWidth: 0,

              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              top: 10,
              
            }}
            inputStyle={{color:'#C0C0C0'}}

            labelStyle={{fontSize:10}}
            containerStyle={{
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              height: hp('5%'),
              backgroundColor: '#060606',
              width: wp('80%'),
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              borderRadius: 10,
              marginTop:hp('1%'),
              borderBottomColor:'grey',
              borderBottomWidth:0.5,
              right:5

            }}
            rightIcon={
              <TouchableOpacity>

              </TouchableOpacity>
            }
          /> 
          












<Text style={{color:'#C0C0C0',marginTop:hp('2%'),fontFamily:'SF-Pro-Rounded-Semibold'}}>Bio</Text>
<Input
            placeholder={'kkk'}
           
            inputContainerStyle={{
              borderColor:'#060606',
              borderWidth: 0,

              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              top: 10,
              
            }}
            inputStyle={{color:'#C0C0C0'}}

            labelStyle={{fontSize:10}}
            containerStyle={{
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              height: hp('5%'),
              backgroundColor: '#060606',
              width: wp('80%'),
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              borderRadius: 10,
              marginTop:hp('1%'),
              borderBottomColor:'grey',
              borderBottomWidth:0.5,
              right:5

            }}
            rightIcon={
              <TouchableOpacity>

              </TouchableOpacity>
            }
          /> 
          















<Text style={{color:'#C0C0C0',marginTop:hp('2%'),fontFamily:'SF-Pro-Rounded-Semibold'}}>Email</Text>
<Input
            placeholder={'dummy@gmail.com'}
           
            inputContainerStyle={{
              borderColor:'#060606',
              borderWidth: 0,

              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              top: 10,
              
            }}
            inputStyle={{color:'#C0C0C0'}}

            labelStyle={{fontSize:10}}
            containerStyle={{
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              height: hp('5%'),
              backgroundColor: '#060606',
              width: wp('80%'),
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              borderRadius: 10,
              marginTop:hp('1%'),
              borderBottomColor:'grey',
              borderBottomWidth:0.5,
              right:5
            }}
            rightIcon={
              <TouchableOpacity>

              </TouchableOpacity>
            }
          /> 
          
</View>

</ScrollView>
                 </View>
 
             </View>
         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default EditProfile;
 