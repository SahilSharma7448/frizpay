/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TextInput,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/FontAwesome';
import {Calendar} from "react-native-calendars"
 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
 import CalendarPicker from 'react-native-calendar-picker';
import Modal from "react-native-modal";

 let customDatesStyles = [];
  customDatesStyles.push({
    // Random colors
    style: {backgroundColor: 'red'},
    textStyle: {color: 'white'}, // sets the font color
    containerStyle: [], // extra styling for day container
    allowDisabled: true, // allow custom style to apply to disabled dates
  });
 const AddTask = ({ navigation }) => {
    const [family,setFamily]=useState(false)
    const [seletedDate, setSelected] = useState('')
 const [visible,setVisible]=useState(false)
 const [value,setValue]=useState('No rewards')
 const changeValue=(data)=>{
     setValue(data)
     setVisible(false)
 }
     return (
         <SafeAreaView style={{ flex: 1 }}>
             <View style={{ flex: 1, backgroundColor: '#060606', }}>
                 {/* <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}> */}
                 {/* //paddingBottom:450 */}
 
 
                 <View style={{ flex: 1, backgroundColor: '#060606', justifyContent: 'flex-start', flexDirection: 'column', alignItems: 'center', alignContent: 'space-between' }}>
 
                     <View style={{
                        
                     }}>
 
                         <TouchableHighlight
                             style={{ width: '10%', height: '100%', backgroundColor: 'transparent', justifyContent: 'center', marginLeft: -120, marginRight: 0, position: 'absolute' }}
 
                             onPress={() => {
                                 // navigation.navigate("LoggedInHomePage")
                                 // console.log(navigation.getParent());
                                 // navigation.toggleDrawer();
                                 // navigationContext.navigate('Notifications');
                                 // navigationContext.navigate('HomeTabPage',
                                 //     {
                                 //         screen: 'Home',
                                 //         // initial: false,
                                 //         params: {
                                 //             screen: 'LoggedInHomePage'
                                 //         }
                                 //     }
                                 // );
 
                                 // navigation.goBack();
 
                                 // navigation.popToTop();
                                 // navigation.goBack(navigation.getState().routes[0].key);
                                 // navigation.navigate('SettingsScreen2');
 
                             }}
                             underlayColor='grey'>
 
                             <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{}} />
 
 
                         </TouchableHighlight>
 
                         <View style={{ width: 113, height: 30, }} >
                             <Image style={{ flex: 1, width: undefined, height: undefined, backgroundColor: 'transparent' }}
                                 source={require('./assets/Fri-zpay-banner.png')}
                                 resizeMode='center'
                             />
                         </View>
                         {/* <Image source={require('./assets/Fri-zpay-banner.png')}  width={113} height={30} style={{ marginLeft: 0, marginRight: 0 }} resizeMode='stretch' /> */}
                         {/* <Image source={require('./assets/top-bell-icon.png')} width={0} height={0} /> */}
 
                     </View>
                     {/* <ScrollView style={{ flex: 1 , height:'10%', width:'100%' }} contentContainerStyle={{ flexGrow: 1, height:'100%', width:'100%', alignContent:'center', flex:1}}> */}
 


<View style={{borderRadius:10,borderWidth:1,borderColor:'#E8EB59',backgroundColor:'#313131',width:wp('90%'),marginTop:hp('5%')}}>


<ScrollView>
<TextInput style={{  color: 'grey', alignSelf: 'center', width: wp('80%'), right: 5,fontSize:20, }} placeholder={'Name of the task'} placeholderTextColor={'grey'}></TextInput>
      <View style={{ borderBottomColor: 'grey', borderBottomWidth: 1, width: wp('80%'), alignSelf: 'center', }}></View>
      <View style={{ flexDirection: 'row', marginTop: hp('5%'),  borderRadius: 8, width: wp('45%'), alignSelf: 'flex-start', alignItems: 'center', justifyContent: 'center',marginLeft:wp('5%'),backgroundColor:'#484848' }}>

<Text style={{ fontSize: 15, color: 'grey', textAlign: 'left', padding: hp('1.8%'),width:wp('30%')}}>{value}</Text>
<TouchableOpacity onPress={()=>setVisible(true)}>
        <Ionicons name={'caret-down'} size={20} color={'grey'} style={{marginLeft:wp('3%')}}></Ionicons>
        </TouchableOpacity>
      </View>










      <Calendar
          theme={{
            backgroundColor: 'red',
            calendarBackground: '#484848',
            textSectionTitleColor: 'red',
            textSectionTitleDisabledColor: 'pink',
            selectedDayBackgroundColor: 'black',
            selectedDayTextColor: 'white',
            todayTextColor: 'yellow',
            dayTextColor: 'white',
            textDisabledColor: 'white',
            dotColor: '#00adf5',
            selectedDotColor: 'green',
            arrowColor: 'white',
            disabledArrowColor: '#d9e1e8',
            monthTextColor: 'white',
            indicatorColor: 'blue',

            textDayFontWeight: '300',
            textMonthFontWeight: 'bold',
            textDayHeaderFontWeight: '300',
            textDayFontSize: 16,
            textMonthFontSize: 16,
            textDayHeaderFontSize: 16
          }}
          markedDates={seletedDate}
          onDayPress={(day) => {
              setSelected({ [day.dateString]: { selected: true, backgroundColor: 'black' } })
          }}
          hideDayNames={true}
          style={{ width: wp('80%'), borderRadius: 10, alignSelf: 'center',marginTop:hp('5%') }}
        />
<Text style={{width:wp('80%'),alignSelf:'center',marginTop:hp('2%'),color:'white',fontSize:17,fontFamily:'SFPRODISPLAYBOLD'}}>Assign to</Text>
<View style={{width:wp('80%'),flexDirection:'row',alignSelf:'center',marginTop:hp('2%'),marginBottom:hp('1%')}}>
<View>
<Image source={require('./assets/ridhima.png')} style={{height:40,width:40,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 12, marginTop: hp('2%'),color:'#E0E0E0',fontWeight:'700',fontFamily:'SFPRODISPLAYBOLD' }}>Ridhima</Text>
          </View>
          <View style={{marginLeft:wp('10%')}}>
<Image source={require('./assets/ridhima.png')} style={{height:40,width:40,borderRadius:50}} resizeMode="contain" />
          <Text style={{ fontSize: 12, marginTop: hp('2%'),color:'#E0E0E0',fontWeight:'700',fontFamily:'SFPRODISPLAYBOLD' }}>Ridhima</Text>
          </View>
              </View>


              <TextInput style={{  color: 'grey', alignSelf: 'center', width: wp('80%'), right: 5,fontSize:15, }} placeholder={'Write a note'} placeholderTextColor={'grey'}></TextInput>
      <View style={{ borderBottomColor: 'grey', borderBottomWidth: 1, width: wp('80%'), alignSelf: 'center',marginBottom:hp('1%') }}></View>
      <View style={{ height: hp('6.8%'), width: wp('40%'), marginTop: hp('2%'),marginBottom:hp('10%'), backgroundColor: '#E8EB59', borderRadius: 10, alignItems: 'center', justifyContent: 'center',alignSelf:'center' }}>
            <Text style={{fontFamily:'SF-Pro-Rounded-Semibold', color:'black'}}>Create a Task</Text>
          </View>
              </ScrollView>
</View>



















 </View>
 
             </View>
             <View>
      <Modal isVisible={visible} backdropOpacity={0.2}>
        <View style={{ flex:1}}>
          <View style={{height:hp('18%'),width:wp('25%'),backgroundColor:'black',marginTop:hp('20%'),alignSelf:'center',borderRadius:10}}>

              <Text style={{color:'#8E8E8E',fontSize:15,marginTop:hp('2%'),textAlign:'center'}} onPress={()=>changeValue('Cash')}>Cash</Text>
              <Text style={{color:'#8E8E8E',fontSize:15,marginTop:hp('2%'),textAlign:'center'}} onPress={()=>changeValue('None')}>None</Text>
              <Text style={{color:'#8E8E8E',fontSize:15,marginTop:hp('2%'),textAlign:'center'}} onPress={()=>changeValue('Custom')}>Custom</Text>
         
          </View>
        </View>
      </Modal>
    </View>
             
         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default AddTask;
 