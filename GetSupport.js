/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useEffect, useState } from 'react';

 import {
     SafeAreaView,
     ScrollView,
     StatusBar,
     StyleSheet,
     Text,
     useColorScheme,
     View,
     TouchableOpacity,
     Image, Button, TouchableHighlight, Dimensions, FlatList, PermissionsAndroid
 } from 'react-native';
 import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto'
 import {widthPercentageToDP as wp,heightPercentageToDP as hp} from "./App/utility"
//  import {Switch} from "react-native-elements"
 import { Switch } from 'react-native-elements';
 
 const GetSupport = ({ navigation }) => {
    const [family,setFamily]=useState(false)
 
 
     return (
         <SafeAreaView style={{ flex: 1 ,backgroundColor:'#060606'}}>
          <View style={{width:wp('95%'),alignSelf:'center',marginTop:hp('5%'),flexDirection:'row'}}>

            <TouchableOpacity
 
 onPress={() => {
    

     navigation.goBack();

 }}
 underlayColor='grey'>

 <Image source={require('./assets/back-icon.png')} width={22} height={20} style={{top:hp('1.5%')}} />


</TouchableOpacity>
<Text style={{
 color: '#E0E0E0',

 // width: '85%',

 textAlign: 'left',

 fontFamily: 'SFPRODISPLAYBOLD',
// fontFamily:'displayB',
fontSize: 28
}}>Get support</Text>
</View>

<View style={{ width: wp('90%'), alignSelf: "center", justifyContent: "center", alignItems: "center", flexDirection: "row",marginTop:hp('5%') }}>
          {/* <Image source={require('./assets/offer.png')} style={{height:30,width:30}} resizeMode="contain" /> */}
          <Fontisto
            name={'email'}
            size={30}
            color={'yellow'}
            // style={{ alignSelf: 'center', alignItems: 'center', justifyContent: "center", marginRight: 20 }}
          />
          <View style={{ width: wp('60%'),  marginLeft:wp('6%'),marginTop:hp('1%') }}>
            <Text style={{color:'#C0C0C0',fontSize:15,fontFamily:'SFPRODISPLAYMEDIUM'}}>Raise a query? Email us</Text>
            <Text style={{color:'grey',fontSize:10,fontFamily:'SFPRODISPLAYMEDIUM'}}>frizdaysupport@gmail.com</Text>

          </View>
          {/* <Switch value={false} color="orange" /> */}
          <Text style={{color:'grey',fontSize:10,marginTop:hp('1%'),width:wp('13%')}}></Text>

        </View>
        <View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('81%'), alignSelf: 'flex-end',marginTop:hp('3%')}}></View>
 

        <View style={{ width: wp('90%'), alignSelf: "center", justifyContent: "center", alignItems: "center", flexDirection: "row",marginTop:hp('3%') }}>
          {/* <Image source={require('./assets/parents_logo.png')} style={{height:30,width:30}} resizeMode="contain" /> */}

          <Image source={require('./assets/whatsapp.png')} style={{height:40,width:40,marginTop:hp('2%')}} resizeMode="contain" />
         
          <View style={{ width: wp('60%'),  marginLeft:wp('6%'),marginTop:hp('1%') }}>
            <Text style={{color:'#C0C0C0',fontSize:15,fontFamily:'SFPRODISPLAYMEDIUM'}}>Reach us out on whatsapp</Text>
            <Text style={{color:'grey',fontSize:10,fontFamily:'SFPRODISPLAYMEDIUM'}}>Reach us out on whatsapp for any query</Text>

          </View>
          <Switch value={family} color="#E8EB59" onChange={()=>setFamily(!family)} />

        </View>
        <View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('81%'), alignSelf: 'flex-end',marginTop:hp('3%') }}></View>
 




        <View style={{ width: wp('90%'), alignSelf: "center", justifyContent: "center", alignItems: "center", flexDirection: "row",marginTop:hp('3%') }}>
          {/* <Image source={require('./assets/calendar.png')} style={{height:30,width:30}} resizeMode="contain" /> */}
          <Ionicons
            name={'call'}
            size={30}
            color={'yellow'}
            // style={{ alignSelf: 'center', alignItems: 'center', justifyContent: "center", marginRight: 20 }}
          />
          <View style={{ width: wp('50%'),  marginLeft:wp('6%'),marginTop:hp('1%') }}>
            <Text style={{color:'#C0C0C0',fontSize:15,fontFamily:'SFPRODISPLAYMEDIUM',bottom:hp('0.5%')}}>Call us</Text>

          </View>
          <Text style={{color:'#606060',fontSize:10,fontFamily:'SFPRODISPLAYMEDIUM'}}>+9190283879779</Text>

        </View>
        <View style={{ borderWidth: 0.5, borderColor: 'grey', width: wp('81%'), alignSelf: 'flex-end',marginTop:hp('3%') }}></View>
 







       














               
             
         </SafeAreaView>
     );
 };
 
 const styles = StyleSheet.create({
     slide: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'space-around',
         backgroundColor: 'blue',
     },
     image: {
         width: 113,
         height: 30,
         // marginVertical: 32,
 
     },
     imagelogo: {
         width: 46,
         height: 50,
 
     },
     parentlogo: {
         width: "50%",
         height: "65%",
     },
     teenagerlogo: {
         width: "40%",
         height: "75%",
     },
     text: {
         color: '#E0E0E0',
         textAlign: 'center',
         fontSize: 25,
         fontFamily: 'SF-Pro-Rounded-Semibold',
     },
     title: {
         color: '#E0E0E0',
         // color: 'white',
         textAlign: 'center',
         fontSize: 25,
         width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     membertitle: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 14,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     memberPhone: {
         color: '#E0E0E0',
         // color: 'white',
         // textAlign: 'left',
         fontSize: 10,
         // width: '90%',
         fontFamily: 'SF-Pro-Rounded-Semibold',
         marginBottom: 0
     },
     doneButton: {
         width: '40%',
         // height: 51,
         // backgroundColor: 'red',
         // justifyContent: 'center',
         // alignItems: 'center',
         // borderRadius: 15,
     },
     submitTitle: {
         textAlign: 'center',
 
         fontFamily: 'SFPRODISPLAYMEDIUM',
         fontSize: 16,
         color: '#000000',
 
 
     }
 });
 
 export default GetSupport;
 